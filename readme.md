## Cees & Co CMS

### Getting started
Clone the repository to your environment. Open your terminal and browse to the root folder 
of the project. Apply your settings in the `.env` file.
```
APP_ENV=local
APP_DEBUG=true
APP_KEY=

DB_HOST=localhost
DB_DATABASE=example
DB_USERNAME=username
DB_PASSWORD=password

CACHE_DRIVER=file
SESSION_DRIVER=file
QUEUE_DRIVER=sync

MAIL_DRIVER=mail
MAIL_HOST=mailtrap.io
MAIL_PORT=2525
MAIL_USERNAME=null
MAIL_PASSWORD=null
MAIL_ENCRYPTION=null

GOOGLE_API_KEY=
RECAPTCHA_SITE_KEY=
RECAPTCHA_SECRET_KEY=
```

Before installing the packages make sure the database exists and is set in the `.env` file. Run the following command to install the composer packages.
```
composer install
```

Import the `cms_new.sql` file that is located in the root of the project in the database. If you don't want the default website, run the following command:
```
php artisan migrate --seed
```

### Helpfull

#### Writeable/Permission folders
Default folders that need to be writeable:  
```storage```  
```bootstrap/cache```  
```public/uploads```  
  
For the `spatie/laravel-analytics` module the following folder needs to be writeable:  
```storage/app```  In some cases this won't work. Delete the ```storage/app/laravel-analytics-cache/68``` folder to fix this.

#### Clear cache
```
php artisan cache:clear
```

#### Ordering products
```php
$products = $this->repository->scopeQuery(function($query) {
    return $query->join('category_product', 'products.id', '=', 'category_product.product_id')->orderBy('category_product.sequence', 'asc');
})->paginate(5);
```