<?php

$defaultImageConfig = [
    'max_size' => 10000,
    'max_images' => 1,
    'sizes' => [
        'thumb' => [
            'width' => 64,
            'height' => 64,
        ],
        'small' => [
            'width' => 200,
            'height' => 200,
        ],
        'medium' => [
            'width' => 450,
            'height' => 450,
        ],
        'big' => [
            'width' => 1000,
            'height' => 1000,
        ]
    ]
];

$bannerImageConfig = $defaultImageConfig;
$bannerImageConfig['sizes']['banner'] = [
    'width' => 1600,
    'height' => 1000
];
$productImageConfig = $defaultImageConfig;
$productImageConfig['max_images'] ='10';
return [
    'pages' => [
        'active' => 1,
        'settings' => [
            'images' => $defaultImageConfig
        ]
    ],
    'subpages' => [
        'active' => 1,
        'button' => 1,
        'settings' => [
            'images' => $defaultImageConfig
        ]
    ],
    'blocks' => [
        'active' => 1,
        'settings' => [
            'images' => $defaultImageConfig
        ]
    ],
    'banners' => [
        'active' => 1,
        'settings' => [
            'images' => $bannerImageConfig
        ]
    ],
	'referenties' => [
        'active' => 1,
        'settings' => [
            'images' => $defaultImageConfig
        ]
    ],
    'categories' => [
        'active' => 1,
        'settings' => [
            'images' => $defaultImageConfig
        ]
    ],
    'products' => [
        'active' => 1,
        'settings' => [
            'images' => $defaultImageConfig
        ]
    ],
    'projectgroups' => [
        'active' => 1,
        'settings' => [
            'images' => $defaultImageConfig
        ]
    ],
    'projects' => [
        'active' => 1,
        'settings' => [
            'images' => $defaultImageConfig
        ]
    ],
    'news' => [
        'active' => 1,
        'settings' => [
            'images' => $defaultImageConfig
        ]
    ]
];