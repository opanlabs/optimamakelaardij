$( document ).ready(function() {
    $('.slider-wrapper-block').slick({
    	dots: true,
    	prevArrow: false,
    	nextArrow: false,
    	autoplay:2000
    });
    $('.refrence-wrapper-block').slick({
    	prevArrow: '<i class="fa fa-angle-left prev" aria-hidden="true"></i>',
    	nextArrow: '<i class="fa fa-angle-right next" aria-hidden="true"></i>',
    	autoplay:2000
    });

    $('.slider-for').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        prevArrow: '<i class="fa fa-angle-left prev" aria-hidden="true"></i>',
        nextArrow: '<i class="fa fa-angle-right next" aria-hidden="true"></i>',
        asNavFor: '.slider-nav',
    });
    $('.slider-nav').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        asNavFor: '.slider-for',
        arrows: false,
        dots: false,
        centerMode: false,
        focusOnSelect: true,
        responsive: [{
            breakpoint: 1280,
            settings: {
                slidesToShow: 2
            }
        },{
            breakpoint: 768,
            settings: {
                arrows: false,
                slidesToShow: 2
            }
        }]
    });

    $('#back-to-top').on('click', function (e) {
        e.preventDefault();
        $('html,body').animate({
            scrollTop: 0
        }, 700);
    });
});