<div class="top">
			<div class="container">
				<div class="row">
					<div class="top-left pull-left">
						Heeft u een vraag of wilt u professioneel advies? <span class="red">Neem contact met ons op:</span>		
					</div>	
					<div class="top-right pull-right">
						@foreach($topmenu as $row)
							@if($row->title=='Facebook')
								<div class="fb">
									<a href="{!! !empty($row->slug) ? $row->slug : '#' !!} {{!empty(preg_match('/^((https|http|ftp)\:\/\/)?([a-z0-9A-Z]+\.[a-z0-9A-Z]+\.[a-z0-9A-Z]+\.[a-zA-Z]{2,4}|[a-z0-9A-Z]+\.[a-z0-9A-Z]+\.[a-zA-Z]{2,4}|[a-z0-9A-Z]+\.[a-zA-Z]{2,4})$/i', $row->slug))?$row->slug:url($row->slug)}}"><i class="fa fa-facebook-square" aria-hidden="true"></i> {!! strip_tags($row->text) !!} </a>
								</div>
							@elseif($row->title=='Phone')
								<div class="phone">
									<i class="fa fa-phone-square" aria-hidden="true"></i> {!! strip_tags($row->text) !!}
								</div>
							@elseif($row->title=='Email')
								<div class="mail">
									<a href="{{$row->slug}}"><i class="fa fa-envelope" aria-hidden="true"></i> {!! strip_tags($row->text) !!} </a>
								</div>
							
							@endif
						@endforeach
					</div>	
				</div>
			</div>
		</div>