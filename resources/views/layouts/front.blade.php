<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>{{ $settings['company']->value }} - @yield('title')</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<link rel="stylesheet" href="{{ asset('/app/css/bootstrap.min.css') }}">
	<link rel="stylesheet" href="{{ asset('/app/css/slick.css') }}">
	<link rel="stylesheet" href="{{ asset('/app/css/font-awesome.min.css') }}">
	<link rel="stylesheet" href="{{ asset('/app/css/print.css') }}">
	<script src="{{ asset('/app/js/jquery-1.11.0.min.js') }}"></script>
	<script src="{{ asset('/app/js/bootstrap.min.js') }}"></script>
	<script src="{{ asset('/app/js/slick.min.js') }}"></script>
	<script src="{{ asset('/app/js/mainfront.js') }}"></script>
</head>
<body>

	<div id="wrapper">
		@include('commonfront.top')
		@include('commonfront.header')
		@yield('content')
		@include('commonfront.footer')
	</div>
    
</body>
</html>