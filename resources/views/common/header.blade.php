<div class="header">
			<div class="container">
				<div class="desktop-header">
					<div class="row">
						<div class="logos pull-left"><a href="{{url()}}"><img src="{{ asset('/app/img/logo.png') }}" alt="" class="img-responsive"></a></div>
						<div class="menus pull-right">
							<ul>
								<li><a href="{{url()}}">Home</a></li>
								<li><a href="{{url('optima')}}">Over Optima</a></li>
								<li class="dropdown">
									<a href="{{url('aanbod')}}" class="dropbtn">Aanbod <i class="fa fa-angle-down" aria-hidden="true"></i></a>
									<div class="dropdown-content">
										<ul>
										@if(count($submenuAabond)>0)
										@foreach ($submenuAabond as $key=>$row)
											<li><a href="{{!empty(preg_match('/^((https|http|ftp)\:\/\/)?([a-z0-9A-Z]+\.[a-z0-9A-Z]+\.[a-z0-9A-Z]+\.[a-zA-Z]{2,4}|[a-z0-9A-Z]+\.[a-z0-9A-Z]+\.[a-zA-Z]{2,4}|[a-z0-9A-Z]+\.[a-zA-Z]{2,4})$/i', $row->slug))?$row->slug:url($row->slug)}}">{{$row->title}}</a></li>
										
										@endforeach
										@endif
										</ul>
	    							</div>
								</li>
								<li class="dropdown">
									<a href="{{url('diensten')}}" class="dropbtn">Advies <i class="fa fa-angle-down" aria-hidden="true"></i></a>
									<div class="dropdown-content">
	    								<ul>
										@if(count($submenuAdvies)>0)
											@foreach ($submenuAdvies as $key=>$row)
												<li><a href="{{!empty(preg_match('/^((https|http|ftp)\:\/\/)?([a-z0-9A-Z]+\.[a-z0-9A-Z]+\.[a-z0-9A-Z]+\.[a-zA-Z]{2,4}|[a-z0-9A-Z]+\.[a-z0-9A-Z]+\.[a-zA-Z]{2,4}|[a-z0-9A-Z]+\.[a-zA-Z]{2,4})$/i', $row->slug))?$row->slug:url($row->slug)}}">{{$row->title}}</a></li>
										
											@endforeach
										@endif
										</ul>
	    							</div>
								</li>
								<li><a href="{{url('contact')}}">Contact</a></li>
							</ul>
						</div>
					</div>	
				</div>
				<div class="mobile-header">
					<div class="navbar-header">
					  <div class="logos pull-left"><a href="{{url()}}"><img src="{{ asset('/app/img/logo.png') }}" alt="" class="img-responsive"></a></div>
			          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
			            <span class="icon-bar"></span>
			            <span class="icon-bar"></span>
			            <span class="icon-bar"></span>
			          </button>
			        </div>
			        <div id="navbar" class="navbar-collapse collapse">
			          <ul class="nav navbar-nav">
			            <li class="active"><a href="{{url()}}">Home</a></li>
			            <li><a href="{{url('optima')}}">Over Optima</a></li>
			            <li><a href="{{url('contact')}}">Contact</a></li>
			            <li class="dropdown">
			              <a href="{{url('aanbod')}}" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Aanbod <span class="caret"></span></a>
			              <ul class="dropdown-menu">
							@if(count($submenuAabond)>0)
								@foreach ($submenuAabond as $key=>$row)
									<li><a href="{{url()}}/{{$row->slug}}">{{$row->title}}</a></li>
								@endforeach
							@endif
			              </ul>
			            </li>
						<li class="dropdown">
			              <a href="{{url('diensten')}}" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Advies <span class="caret"></span></a>
			              <ul class="dropdown-menu">
						  @if(count($submenuAdvies)>0)
			                @foreach ($submenuAdvies as $key=>$row)
								<li><a href="{{url()}}/{{$row->slug}}">{{$row->title}}</a></li>
							@endforeach
			               @endif
			              </ul>
			            </li>
			          </ul>
			        </div>
				</div>
			</div>
		</div>