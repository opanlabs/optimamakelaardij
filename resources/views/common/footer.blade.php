<div class="footer">	
			<div class="footer-one">
				@if(isset($blocks[0]))
					<div class="container">	
							{!! !empty($blocks[0]->text) ? $blocks[0]->text : "" !!} </span> <a class="btn btn-default btn-contact" href="{{!empty(preg_match('/^((https|http|ftp)\:\/\/)?([a-z0-9A-Z]+\.[a-z0-9A-Z]+\.[a-z0-9A-Z]+\.[a-zA-Z]{2,4}|[a-z0-9A-Z]+\.[a-z0-9A-Z]+\.[a-zA-Z]{2,4}|[a-z0-9A-Z]+\.[a-zA-Z]{2,4})$/i', $blocks[0]->slug))?$blocks[0]->slug:url($blocks[0]->slug)}}" role="button">{!! !empty($blocks[0]->title) ? $blocks[0]->title : "" !!}</a>
					</div>
				@endif
			</div>
			<div class="footer-two">	
				<div class="container">	
					<div class="row">
						@if(isset($blocks[1]))
						<div class="footer-two-block col-md-4">
							<div class="title">optimamakelaardij</div>
							<div class="desc">
								{!! !empty($blocks[1]->text) ? $blocks[1]->text : "" !!}
							</div>
						</div>
						
						@endif
						@if(isset($blocks[3]) || isset($blocks[4]))
						<div class="footer-two-block col-md-4">
							<div class="title">Contactgegevens</div>
							<div class="desc">
								@if(isset($blocks[3]))
									{!! !empty($blocks[3]->text) ?  $blocks[3]->text  : "" !!}
								@endif
								@if(isset($blocks[4]))
								<p><b><i class="fa fa-facebook-official" aria-hidden="true"></i></b> {!! !empty($blocks[4]->text) ? strip_tags($blocks[4]->text) : "" !!}</p>
								@endif
							</div>
						</div>
						@endif
						
						<div class="footer-two-block col-md-4">
							<div class="title">Wij zijn aangesloten bij:</div>
							<div class="desc">
								@if(isset($blocks[5]) &&!empty($blocks[5]->images()->orderBy('sequence')->limit(1)->get()->toArray()[0]['path']))
								<div class="img-block">
									<a href="{{!empty(preg_match('/^((https|http|ftp)\:\/\/)?([a-z0-9A-Z]+\.[a-z0-9A-Z]+\.[a-z0-9A-Z]+\.[a-zA-Z]{2,4}|[a-z0-9A-Z]+\.[a-z0-9A-Z]+\.[a-zA-Z]{2,4}|[a-z0-9A-Z]+\.[a-zA-Z]{2,4})$/i', $blocks[5]->slug))?$blocks[5]->slug:url($blocks[5]->slug)}}"><img src="{{url()}}/uploads/images//{{$blocks[5]->images()->orderBy('sequence')->limit(1)->get()->toArray()[0]['path']}}/original.{{$blocks[5]->images()->orderBy('sequence')->limit(1)->get()->toArray()[0]['extension']}}" alt="" class="img-responsive"></a>
								</div>
							    @endif	@if(isset($blocks[6]) &&!empty($blocks[6]->images()->orderBy('sequence')->limit(1)->get()->toArray()[0]['path']))
								<div class="img-block">
									<a href="{{!empty(preg_match('/^((https|http|ftp)\:\/\/)?([a-z0-9A-Z]+\.[a-z0-9A-Z]+\.[a-z0-9A-Z]+\.[a-zA-Z]{2,4}|[a-z0-9A-Z]+\.[a-z0-9A-Z]+\.[a-zA-Z]{2,4}|[a-z0-9A-Z]+\.[a-zA-Z]{2,4})$/i', $blocks[6]->slug))?$blocks[6]->slug:url($blocks[6]->slug)}}"><img src="{{url()}}/uploads/images//{{$blocks[6]->images()->orderBy('sequence')->limit(1)->get()->toArray()[0]['path']}}/original.{{$blocks[6]->images()->orderBy('sequence')->limit(1)->get()->toArray()[0]['extension']}}" alt="" class="img-responsive"></a>
								</div>
								@endif
								@if(isset($blocks[7]) &&!empty($blocks[7]->images()->orderBy('sequence')->limit(1)->get()->toArray()[0]['path']))
								<div class="img-block">
									<a href="{{!empty(preg_match('/^((https|http|ftp)\:\/\/)?([a-z0-9A-Z]+\.[a-z0-9A-Z]+\.[a-z0-9A-Z]+\.[a-zA-Z]{2,4}|[a-z0-9A-Z]+\.[a-z0-9A-Z]+\.[a-zA-Z]{2,4}|[a-z0-9A-Z]+\.[a-zA-Z]{2,4})$/i', $blocks[7]->slug))?$blocks[7]->slug:url($blocks[7]->slug)}}"><img src="{{url()}}/uploads/images//{{$blocks[7]->images()->orderBy('sequence')->limit(1)->get()->toArray()[0]['path']}}/original.{{$blocks[7]->images()->orderBy('sequence')->limit(1)->get()->toArray()[0]['extension']}}" alt="" class="img-responsive"></a>
								</div>
								@endif
								@if(isset($blocks[8]) &&!empty($blocks[8]->images()->orderBy('sequence')->limit(1)->get()->toArray()[0]['path']))
								<div class="img-block">
									<a href="{{!empty(preg_match('/^((https|http|ftp)\:\/\/)?([a-z0-9A-Z]+\.[a-z0-9A-Z]+\.[a-z0-9A-Z]+\.[a-zA-Z]{2,4}|[a-z0-9A-Z]+\.[a-z0-9A-Z]+\.[a-zA-Z]{2,4}|[a-z0-9A-Z]+\.[a-zA-Z]{2,4})$/i', $blocks[8]->slug))?$blocks[8]->slug:url($blocks[8]->slug)}}"><img src="{{url()}}/uploads/images//{{$blocks[8]->images()->orderBy('sequence')->limit(1)->get()->toArray()[0]['path']}}/original.{{$blocks[8]->images()->orderBy('sequence')->limit(1)->get()->toArray()[0]['extension']}}" alt="" class="img-responsive"></a>
								</div>
								@endif
								@if(isset($blocks[9]) &&!empty($blocks[9]->images()->orderBy('sequence')->limit(1)->get()->toArray()[0]['path']))
								<div class="img-block">
									<a href="{{!empty(preg_match('/^((https|http|ftp)\:\/\/)?([a-z0-9A-Z]+\.[a-z0-9A-Z]+\.[a-z0-9A-Z]+\.[a-zA-Z]{2,4}|[a-z0-9A-Z]+\.[a-z0-9A-Z]+\.[a-zA-Z]{2,4}|[a-z0-9A-Z]+\.[a-zA-Z]{2,4})$/i', $blocks[9]->slug))?$blocks[9]->slug:url($blocks[9]->slug)}}"><img src="{{url()}}/uploads/images//{{$blocks[9]->images()->orderBy('sequence')->limit(1)->get()->toArray()[0]['path']}}/original.{{$blocks[9]->images()->orderBy('sequence')->limit(1)->get()->toArray()[0]['extension']}}" alt="" class="img-responsive"></a>
								</div>
								@endif
							</div>
						</div>	
					</div>
					<div class="rowx">
						<div class="footer-bottom">
							<div class="footer-bottom-left pull-left">
								© 2016 - <a href="#">Optima Makelaardij</a> - Disclaimer - Algemene Voorwaarden
							</div>
							<div class="footer-bottom-right pull-right">
								Website door: <a href="#">Cees & Co</a> ism <a href="#">Medialex</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>