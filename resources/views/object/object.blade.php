@extends('layouts.front')

@section('title', 'objecct')

@section('content')

   <div class="breadcumb-wrapper2">
			<div class="breadcumb-banner">
				<div class="img"><img src="{{ asset('app/img/slider-object1.jpg') }}" alt="" ></div>
				<div class="breadcumb-block2">
					<div class="container">
						<div class="row">
							<div class="breadcumb-left pull-left">
								<div class="title">{{$product->name}} </div>
								<div class="price">€ {{$product->price}}</div>
							</div>
							<div class="breadcumb-right pull-right">
								<div class="breadcumb"><a href="{{url('home')}}">Home</a> <span>></span><a href="{{url('aanbod')}}">Aanbod </a> <span>></span> <a href="{{url('object/show')}}/{{$product->slug}}">{{$product->name}} </a><span>></span><span>Aanbod</span></div>
							</div>
						</div>
					</div>				
				</div>
			</div>
		</div>
		<div class="menus2-wrapper">
			<div class="container">
				<div class="row">
					<div class="menus col-md-9">
						<ul>
							<li><a href="#" class="active">Kenmerken</a></li>
							<li><a href="#">Omschrijving</a></li>
							<li><a href="#">Foto’s</a></li>
							<li><a href="#">360°</a></li>
							<li><a href="#">Video</a></li>
							<li><a href="#">Kaart</a></li>
							<li><a href="#">Plattegrond</a></li>
						</ul>
					</div>
					<div class="back col-md-3"><a class="btn btn-default " href="#" role="button">Terug naar het aanbod</a></div>
				</div>
			</div>
		</div>
		<div class="detail-object-wrapper">
			<div class="container">
				<div class="row">
					<div class="content-main col-md-9">
						<div class="column-detail">
							<div class="topx">
								<div class="column-left pull-left">
									<div class="logos">
										<i class="fa fa-key" aria-hidden="true"></i>
									</div>
								</div>
								<div class="column-right pull-left">
									<div class="title">Overdracht</div>
									<div class="table">
										<table>
										  <tbody>
										    <tr>
										      <td class="labels">Vraagprijs:</td>
										      <td>€ 227.500 k.k</td>
										    </tr>
										    <tr>
										      <td class="labels">Status:</td>
										      <td>Beschikbaar</td>
										    </tr>
										    <tr>
										      <td class="labels">Aanvaarding:</td>
										      <td>In overleg</td>
										    </tr>
										  </tbody>
										</table>
									</div>
								</div>
							</div>
							<div class="topx">
								<div class="column-left pull-left">
									<div class="logos">
										<i class="fa fa-home" aria-hidden="true"></i>
									</div>
								</div>
								<div class="column-right pull-left">
									<div class="title">Bouw</div>
									<div class="table">
										<table>
										  <tbody>
										    <tr>
										      <td class="labels">Soort woonhuis:</td>
										      <td>Eengezinswoning</td>
										    </tr>
										    <tr>
										      <td class="labels">Type woonhuis:</td>
										      <td>Hoekwoning</td>
										    </tr>
										    <tr>
										      <td class="labels">Bouwvorm:</td>
										      <td>Bestaande bouw</td>
										    </tr>
										    <tr>
										      <td class="labels">Bouwjaar:</td>
										      <td>2007</td>
										    </tr>
										  </tbody>
										</table>
									</div>
								</div>
							</div>
							<div class="topx">
								<div class="column-left pull-left">
									<div class="logos">
										<i class="fa fa-arrows-alt" aria-hidden="true"></i>
									</div>
								</div>
								<div class="column-right pull-left">
									<div class="title">Oppervlakte</div>
									<div class="table">
										<table>
										  <tbody>
										    <tr>
										      <td class="labels">Woonruimte:</td>
										      <td>110 m<sup>2</sup></td>
										    </tr>
										    <tr>
										      <td class="labels">Perceeloppervlakte:</td>
										      <td>193 m<sup>2</sup></td>
										    </tr>
										    <tr>
										      <td class="labels">Inhoud:</td>
										      <td>400 m<sup>2</sup></td>
										    </tr>
										  </tbody>
										</table>
									</div>
								</div>
							</div>
							<div class="topx">
								<div class="column-left pull-left">
									<div class="logos">
										<i class="fa fa-bed" aria-hidden="true"></i>
									</div>
								</div>
								<div class="column-right pull-left">
									<div class="title">Kamers</div>
									<div class="table">
										<table>
										  <tbody>
										    <tr>
										      <td class="labels">Aantal kamers:</td>
										      <td>5</td>
										    </tr>
										    <tr>
										      <td class="labels">Slaapkamers:</td>
										      <td>4</td>
										    </tr>
										    <tr>
										      <td class="labels">Badkamers:</td>
										      <td>1</td>
										    </tr>
										  </tbody>
										</table>
									</div>
								</div>
							</div>
							<div class="topx">
								<div class="column-left pull-left">
									<div class="logos">
										<i class="fa fa-leaf" aria-hidden="true"></i>
									</div>
								</div>
								<div class="column-right pull-left">
									<div class="title">Tuin</div>
									<div class="table">
										<table>
										  <tbody>
										    <tr>
										      <td class="labels">Oppervlakte voortuin:</td>
										      <td>12 m<sup>2</sup></td>
										    </tr>
										    <tr>
										      <td class="labels">Ligging voortuin:</td>
										      <td>Zuid-oost</td>
										    </tr>
										    <tr>
										      <td class="labels">Oppervlakte achtertuin:</td>
										      <td>50 m<sup>2</sup></td>
										    </tr>
										    <tr>
										      <td class="labels">Oppervlakte achtertuin:</td>
										      <td>50 m<sup>2</sup></td>
										    </tr>
										  </tbody>
										</table>
									</div>
								</div>
							</div>
							<div class="topx">
								<div class="column-left pull-left">
									<div class="logos">
										<i class="fa fa-lightbulb-o" aria-hidden="true"></i>
									</div>
								</div>
								<div class="column-right pull-left">
									<div class="title">Energie</div>
									<div class="table">
										<table>
										  <tbody>
										    <tr>
										      <td class="labels">Energielabel:</td>
										      <td>F</td>
										    </tr>
										    <tr>
										      <td class="labels">Isolatie:</td>
										      <td>Dubbel glas</td>
										    </tr>
										    <tr>
										      <td class="labels">Verwarming:</td>
										      <td>CV Ketel, open haard</td>
										    </tr>
										    <tr>
										      <td class="labels">Warm water:</td>
										      <td>CV Ketel</td>
										    </tr>
										  </tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="content-sidebar col-md-3">
						<div class="section-sidebar">
							<div class="title">Heeft u vragen over dit object?</div>
							<div class="desc">
								<p>Lorem ipsum dolor sit amet,
								consectetur adipiscing elit sed
								do eiusmod tempor incidid...</p>
								<div class="bold">Bel ons!</div>
								<div class="tlp">+31 (0)229 50 66 90</div>
								<div class="comment"><a href="#">Reageren <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a></div>
							</div>
						</div>
						<div class="section-sidebar">
							<div class="title">Uw makelaar</div>
							<div class="img"><img src="{{ asset('app/img/img-orang.png') }}" alt="" class="img-responsive"></div>
							<div class="desc">
								<div class="bold">Contact Rik:</div>
								<div class="email"><span>E:</span> <a href="mailto:rick@optimamakelaardij.nl">rick@optimamakelaardij.nl</a></div>
								<div class="tlp"><span>T:</span> +31 (0)229 50 66 90</div></div>
						</div>
						<div class="section-sidebar">
							<div class="title">Brochure</div>
							<div class="brochure"><a href="#" class="btn btn-default ">Downloaden</a></div>
						</div>
					</div>
					</div>
				</div>
			</div>
		</div>
		<div class="slider-bottom-wrapper">
			<div class="container">
				<div class="slider-for ">
					@foreach ($product->images as $row)
					<div class="slider-large-block">
						<div class="img"><img src="{{ asset('uploads/images') }}/{{$row->path}}/big.jpg" alt="" style="width:100%;" class="img-responsive"></div>
						<div class="caption">Foto</div>
					</div>
					@endforeach
				</div>
				<div class="slider-nav ">
					@foreach ($product->images as $row)
						<div class="img"><img src="{{ asset('uploads/images') }}/{{$row->path}}/original.jpg" alt="" class="img-responsive"></div>
					@endforeach
				</div>
			</div>
		</div>
@endsection