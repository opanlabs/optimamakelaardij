@extends('layouts.app')

@section('title', 'Pagina niet gevonden')

@section('content')
	<div class="breadcumb-wrapper">
			<div class="container">
				<div class="breadcumb-block">
					<div class="row">
						<div class="breadcumb-left pull-left">
							<div class="title"></div>
						</div>
						<div class="breadcumb-right pull-right">
							<div class="breadcumb"><a href="{{url()}}">Home</a> <span>></span> <span>404</span></div>
						</div>
					</div>
				</div>				
			</div>
	</div>
	<div class="contact-wrapper">
			<div class="container">
				<div class="row">
					<div class="contact-left">
						<div class="title">
							@if ($msg = $settings['message_404']->value)
								{{ $msg }}
							@else
								Helaas! De pagina die u zoekt kan niet worden gevonden.
							@endif
						</div>
					</div>	
					<div class="contact-right">
						
					</div>	
					<div class="clear"></div>
				</div>						
			</div>
			
	</div>


    
@endsection