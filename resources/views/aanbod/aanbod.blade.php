@extends('layouts.front')
@section('content')
    <div class="breadcumb-wrapper">
			<div class="container">
				<div class="breadcumb-block">
					<div class="row">
						<div class="breadcumb-left pull-left">
							<div class="title">Aanbod</div>
						</div>
						<div class="breadcumb-right pull-right">
							<div class="breadcumb"><a href="{{url()}}">Home</a> <span>></span> <span>Aanbod</span></div>
						</div>
					</div>
				</div>				
			</div>
		</div>
		<div class="search-advance-wrapper">
			<div class="container">
				<div class="form-search-advance">
					<form action="">
						<div class="row">
							<div class="search-block col-md-2">
								<select class="form-control">
									<option value="">Zoek op kaart</option>
									<option value="">test 1</option>
									<option value="">test 2</option>
								</select>		
							</div>
							<div class="search-block col-md-2">
								<input class="form-control" type="text" id="" placeholder="Plaats, buurt, straat">		
							</div>
							<div class="search-block col-md-2">
								<select class="form-control">
									<option value="">Type Woning</option>
									<option value="">test 1</option>
									<option value="">test 2</option>
								</select>		
							</div>
							<div class="search-block col-md-2">
								<select class="form-control">
									<option value="">Alle prijzen</option>
									<option value="">test 1</option>
									<option value="">test 2</option>
								</select>		
							</div>
							<div class="search-block col-md-2">
								<a class="btn btn-default btn-meet" href="#" role="button">Meer filters <i class="fa fa-sliders" aria-hidden="true"></i></a>	
							</div>
							<div class="search-block col-md-2">
								<a class="btn btn-default btn-green" href="#" role="button">Bekijk aanbod</a>		
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<div class="latest-offer-wrapper">
			<div class="container">
						<?php $i=0; ?>
						@foreach ($product as $key=>$row)
							<?php $i++; ?>
							@if($i==1)
								<div class="column-offer-wrapper">
									<div class="row">
							@elseif($i==4)
									</div>
								</div>
								<div class="column-offer-wrapper">
									<div class="row">
							@endif
									<div class="column-offer-block col-md-4">
										<div class="img">
											<a href="{{url('object/show')}}/{{$row->slug}}"><img src="{{ asset('uploads/images') }}/{{$product[$key]->images()->orderBy('sequence')->limit(1)->get()->toArray()[0]['path']}}/medium.jpg" alt="" class="img-responsive"></a>
											<div class="sale">{{$product[$key]->getAttrValue('status')}}</div>
											<div class="price">
												€ {{$row->price}}<span>|</span>{{$product[$key]->getAttrValue('slaapkamers')}} slaapkamers
											</div>
										</div>
										<div class="desc-wrapper">
											<div class="title"><a href="{{url('object/show')}}/{{$row->slug}}">{{$row->name}}</a></div>
											<div class="desc">
												{{strip_tags($row->intro)}}
											</div>
											<div class="more"><a class="btn btn-default " href="{{url('object/show')}}/{{$row->slug}}" role="button">Lees meer</a></div>	
										</div>
									</div>
									
							
							@if($i==count($product))
									</div>
								</div>
							@endif
						@endforeach
						
					
				<div class="arrow-to-top"><a href="#" id="back-to-top"><i class="fa fa-angle-up" aria-hidden="true"></i></a></div>
			</div>
		</div>
@endsection