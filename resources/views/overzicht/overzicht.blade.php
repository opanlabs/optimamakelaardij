@extends('layouts.app')

@section('title','overzicht')

@section('content')
    <div class="row">
        <div class="col-md-8">
            <article itemscope itemprop itemtype="http://schema.org/Article">
                <header>
                    <h1 class="title" itemprop="name">{{ $page->meta_title }}</h1>
                    <hr />
                </header>
                <section itemprop="articleBody">
                    {!! $page->text !!}
                </section>
            </article>
        </div>
        <div class="col-md-4">
            <aside>
                @if ($img = $page->images()->first())
                    <figure>
                        <img width="100%" src="{{ asset('uploads/images/' . $img->path . '/medium.' . $img->extension) }}" alt="{{ $img->description }}" />
                        <figcaption>{!! $img->description !!}</figcaption>
                    </figure>
                @endif
            </aside>
        </div>
    </div>
@endsection