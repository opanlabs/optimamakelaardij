@extends('layouts.app')

@section('title', $page->meta_title)

@section('content')
    <div class="row">
        <div class="col-md-6">
            <article itemscope itemprop itemtype="http://schema.org/Article">
                <header>
                    <h1 class="title" itemprop="name">{{ $page->meta_title }}</h1>
                    <hr />
                </header>
                <section itemprop="articleBody">
                    {!! $page->text !!}
                </section>
            </article>

            @include('common.message')

            {!! Form::open(['url' => 'contact']) !!}
            <div class="form-group">
                {!! Form::label('name', 'Naam') !!}
                {!! Form::text('name', null, ['class' => 'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('email', 'E-mailadres') !!}
                {!! Form::email('email', null, ['class' => 'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('subject', 'Onderwerp') !!}
                {!! Form::text('subject', null, ['class' => 'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('message', 'Uw bericht') !!}
                {!! Form::textarea('message', null, ['class' => 'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Recaptcha::render() !!}
            </div>
            <div class="form-group">
                {!! Form::submit('Verzenden') !!}
            </div>
            {!! Form::open() !!}
        </div>
        <div class="col-md-6">
            <aside>

            </aside>
        </div>
    </div>
@endsection