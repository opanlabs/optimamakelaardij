@extends('layouts.front')

@section('title', $page->meta_title)

@section('content')
	<div class="breadcumb-wrapper">
			<div class="container">
				<div class="breadcumb-block">
					<div class="row">
						<div class="breadcumb-left pull-left">
							<div class="title">{{ $page->title }}</div>
						</div>
						<div class="breadcumb-right pull-right">
							<div class="breadcumb"><a href="{{url()}}">Home</a> <span>></span> <span>{{ $page->title }}</span></div>
						</div>
					</div>
				</div>				
			</div>
	</div>
	<div class="contact-wrapper">
			<div class="container">
				<div class="row">
					<div class="contact-left">
						 <article itemscope itemprop itemtype="http://schema.org/Article">
							<header>
								<h1 class="title" itemprop="name">{{ $page->title }}</h1>
								<hr />
							</header>
							<section itemprop="articleBody">
								{!! $page->text !!}
							</section>
						</article>
					</div>	
					<div class="contact-right">
							 <aside>
								@if ($img = $page->images()->first())
									<figure>
										<img width="100%" src="{{ asset('uploads/images/' . $img->path . '/medium.' . $img->extension) }}" alt="{{ $img->description }}" />
										<figcaption>{!! $img->description !!}</figcaption>
									</figure>
								@endif
							</aside>
					</div>	
					<div class="clear"></div>
				</div>						
			</div>
			
	</div>
   
@endsection