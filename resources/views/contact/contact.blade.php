@extends('layouts.front')

@section('title', 'contact')

@section('content')
   <div class="breadcumb-wrapper">
			<div class="container">
				<div class="breadcumb-block">
					<div class="row">
						<div class="breadcumb-left pull-left">
							<div class="title">Contact</div>
						</div>
						<div class="breadcumb-right pull-right">
							<div class="breadcumb"><a href="{{url()}}">Home</a> <span>></span> <span>Contact</span></div>
						</div>
					</div>
				</div>				
			</div>
		</div>
		<div class="contact-wrapper">
			<div class="container">
				<div class="row">
					<div class="contact-left">
					
						<div class="title">{{isset($subPages[0])?$subPages[0]->title:''}}</div>
						<div class="shortdesc">{{isset($subPages[0])?strip_tags($subPages[0]->text):''}}</div>
						<div class="form-contact">
							 <div id="info-status"> @include('common.message')</div>
							<form action="{{ url('contact/send') }}" class="contact-form" id="contact-form" method="post">
							{{ csrf_field() }}
								<input type="text" class="form-control name"  name="name" placeholder="Uw naam*" required>
								<input type="text" class="form-control phones" name="phone" placeholder="Uw telefoonnummer*" required>
								<input type="email" class="form-control email"  name="email" placeholder="Uw emailadres*" required>
								<input type="text" class="form-control onderwerp"    name="onderwerp" placeholder="Onderwerp*" required>
								<textarea class="form-control" rows="3"  name="bericht" placeholder="Uw bericht"></textarea>
								 <button type="submit" value="Verzenden" class="btn btn-default btn-submit btn-green">Verzenden</button>
								
							</form>
						</div>
					</div>	
					<div class="contact-right">
							<div class="bg-right-contact"><img src="{{ asset('app/img/bg-contact.png') }}" alt=""></div>
							<div class="column-content-contact">
								@if(isset($subPages[1]) && $subPages[2] && $subPages[3] && $subPages[4])
								<div class="title">{{$subPages[1]->title}}</div>
								<div class="desc">
									{!! $subPages[1]->text !!}
									<span>{{$subPages[2]->title}}</span> 
									{!!$subPages[2]->text!!}
									<p>
										<a href="{{!empty(preg_match('/^((https|http|ftp)\:\/\/)?([a-z0-9A-Z]+\.[a-z0-9A-Z]+\.[a-z0-9A-Z]+\.[a-zA-Z]{2,4}|[a-z0-9A-Z]+\.[a-z0-9A-Z]+\.[a-zA-Z]{2,4}|[a-z0-9A-Z]+\.[a-zA-Z]{2,4})$/i', $subPages[3]->slug))?$subPages[3]->slug:url($subPages[3]->slug)}}" target="_blank"><i class="fa fa-facebook-square" aria-hidden="true"></i> {{strip_tags($subPages[3]->text)}}</a>
									</p>
									<p>
										<a href="{{!empty(preg_match('/^((https|http|ftp)\:\/\/)?([a-z0-9A-Z]+\.[a-z0-9A-Z]+\.[a-z0-9A-Z]+\.[a-zA-Z]{2,4}|[a-z0-9A-Z]+\.[a-z0-9A-Z]+\.[a-zA-Z]{2,4}|[a-z0-9A-Z]+\.[a-zA-Z]{2,4})$/i', $subPages[4]->slug))?$subPages[4]->slug:url($subPages[4]->slug)}}" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i> {{strip_tags($subPages[4]->text)}}</a>
									</p>
								</div>
								@endif
							</div>
					</div>	
					<div class="clear"></div>
				</div>						
			</div>
			<div class="maps-wrapper">
				<div class="overlay" onClick="style.pointerEvents='none'"></div>
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2419.774735554941!2d5.1048638158128465!3d52.66404627984178!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c8a8c714f971f9%3A0xa548c56e30e4cff3!2sOosterblokker+2%2C+1696+BH+Oosterblokker%2C+Netherlands!5e0!3m2!1sen!2sid!4v1488909031099" width="100%" height="500" frameborder="0" style="border:0" allowfullscreen></iframe>
			</div>
		</div>
@endsection