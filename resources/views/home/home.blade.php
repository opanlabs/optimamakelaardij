@extends('layouts.front')
@section('content')
  <div class="slider-wrapper">
			<div class="slider-wrapper-block slider">
			@if(isset($banner)>0)
			@foreach ($banner as $key=>$row)
				<div class="slider-block">
					<div class="img"><img src="{{url()}}/uploads/images//{{$banner[$key]->images()->orderBy('sequence')->limit(1)->get()->toArray()[0]['path']}}/banner.jpg" alt="" class="img-responsive"></div>
					<div class="container">
						<div class="caption-wrapper">
							<div class="caption">
								<div class="title">{{$row->title}}</div>
								<div class="desc">
									{{strip_tags($row->description)}}
								</div>
							</div>					
						</div>	
					</div>
				</div>
			@endforeach
			@endif
			</div>			
		</div>
		<div class="search-advance-wrapper">
			<div class="container">
				<div class="form-search-advance">
					<form action="">
						<div class="row">
							<div class="search-block col-md-2">
								<select class="form-control">
									<option value="">Zoek op kaart</option>
									<option value="">test 1</option>
									<option value="">test 2</option>
								</select>		
							</div>
							<div class="search-block col-md-2">
								<input class="form-control" type="text" id="" placeholder="Plaats, buurt, straat">		
							</div>
							<div class="search-block col-md-2">
								<select class="form-control">
									<option value="">Type Woning</option>
									<option value="">test 1</option>
									<option value="">test 2</option>
								</select>		
							</div>
							<div class="search-block col-md-2">
								<select class="form-control">
									<option value="">Alle prijzen</option>
									<option value="">test 1</option>
									<option value="">test 2</option>
								</select>		
							</div>
							<div class="search-block col-md-2">
								<a class="btn btn-default btn-meet" href="#" role="button">Meer filters <i class="fa fa-sliders" aria-hidden="true"></i></a>	
							</div>
							<div class="search-block col-md-2">
								<a class="btn btn-default btn-green" href="#" role="button">Bekijk aanbod</a>		
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<div class="latest-offer-wrapper">
			<div class="container">
				<div class="titlex">Ons nieuwste aanbod</div>
				<div class="column-offer-wrapper">
					<div class="row">
					@if(count($products )>0)
					@foreach ($products as $key=>$row)
					
					
						<div class="column-offer-block col-md-4">
							<div class="img">
								<a href="{{url('object/show')}}/{{$row->slug}}"><img src="{{ asset('uploads/images') }}/{{$products[$key]->images()->orderBy('sequence')->limit(1)->get()->toArray()[0]['path']}}/medium.jpg" alt="" class="img-responsive"></a>
								<div class="sale">{{$products[$key]->getAttrValue('status')}}</div>
								<div class="price">
									€ {{$row->price}}<span>|</span>{{$products[$key]->getAttrValue('slaapkamers')}}  slaapkamers
								</div>
							</div>
							<div class="desc-wrapper">
								<div class="title"><a href="{{url('object/show')}}/{{$row->slug}}">{{$row->name}}</a></div>
								<div class="desc">
									{{strip_tags($row->intro)}}
								</div>
								<div class="more"><a class="btn btn-default " href="{{url('object/show')}}/{{$row->slug}}" role="button">Lees meer</a></div>	
							</div>
						</div>
						@endforeach
						@endif
					</div>
					@if(isset($blocks[2]))
					<div class="see-more"><a class="btn btn-default btn-see-more" href="{{!empty(preg_match('/^((https|http|ftp)\:\/\/)?([a-z0-9A-Z]+\.[a-z0-9A-Z]+\.[a-z0-9A-Z]+\.[a-zA-Z]{2,4}|[a-z0-9A-Z]+\.[a-z0-9A-Z]+\.[a-zA-Z]{2,4}|[a-z0-9A-Z]+\.[a-zA-Z]{2,4})$/i', $blocks[2]->slug))?$blocks[2]->slug:url($blocks[2]->slug)}}" role="button">Bekijk ons gehele aanbod <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a></div>
					@endif
				</div>
			</div>
		</div>
		<div class="refrence-wrapper">
			<div class="container">
				<div class="titlex">Referenties</div>
				<div class="refrence-wrapper-block slider">	
				@if(count($referenties)>0)
				@foreach ($referenties as $key=>$row)
					<div class="refrence-block">
						<div class="img"><img src="{{ asset('uploads/images') }}/{{$row->path}}/medium.jpg" alt="" class="img-responsive"></div>
						<div class="desc-wrapper">
							<div class="title">{{$row->title}}</div>
							<div class="desc">
								{!! $row->description !!}
							</div>
							<div class="star">
								@for($i=1;$i <= $row->star; $i++)
									<i class="fa fa-star" aria-hidden="true"></i>
								@endfor
							</div>
						</div>
						<div class="clear"></div>
					</div>
				@endforeach
				@endif
				</div>
			</div>
		</div>
		<div class="assessment-wrapper">
			@if(isset($funda[0]))
			<div class="container">
				<div class="column">
					<div class="titlex">{{$funda[0]->title}}</div>
					<div class="funda">
						<div class="point">{!! $funda[0]->text !!}</div>
					</div>
				</div>				
			</div>
			@endif
		</div>
		<div class="wecanhelp-wrapper">	
			<div class="container">	
				<div class="titlex">Waarmee kunnen wij u helpen?</div>
				<div class="column-help">	
					<div class="row">	
					@if(count($subPages)>0)
						@foreach ($subPages as $row)
							<div class="column-help-block col-md-4">	
								<div class="img"><i class="fa {{$row->meta_title}}" aria-hidden="true"></i></div>
								<div class="title">{{$row->title}}</div>
								<div class="desc">	
									{!! $row->text !!}

									<a href="{{!empty(preg_match('/^((https|http|ftp)\:\/\/)?([a-z0-9A-Z]+\.[a-z0-9A-Z]+\.[a-z0-9A-Z]+\.[a-zA-Z]{2,4}|[a-z0-9A-Z]+\.[a-z0-9A-Z]+\.[a-zA-Z]{2,4}|[a-z0-9A-Z]+\.[a-zA-Z]{2,4})$/i', $row->slug))?$row->slug:url($row->slug)}}">LEES MEER</a>
								</div>
							</div>
						@endforeach
						@endif
					</div>
				</div>
			</div>			
		</div>
@endsection