@extends('layouts.front')
@section('content')
    <div class="breadcumb-wrapper">
			<div class="container">
				<div class="breadcumb-block">
					<div class="row">
						<div class="breadcumb-left pull-left">
							<div class="title">Diensten</div>
						</div>
						<div class="breadcumb-right pull-right">
							<div class="breadcumb"><a href="{{url()}}">Home</a> <span>></span> <span>Diensten</span></div>
						</div>
					</div>
				</div>				
			</div>
		</div>
		<div class="content-wrapper">
			<div class="container">
				<div class="recomended-wrapper">
					<div class="titlex">Waarin kan Optima Makelaardij u adviseren?</div>
					@foreach($subPages as $key => $subPage)
						<div class="column-recomended-wrapper">
							<div class="recomended-block">
								<div class="title">{{$subPage->title}}</div>
								<div class="img"><?php echo ($subPage->meta_title=='handshake')?'<img src="'.asset('app/img/handshake.png').'" alt="" class="img-responsive">' :'<i class="fa '.$subPage->meta_title.'" aria-hidden="true"></i>'; ?></div>
								<div class="desc">{!! $subPage->text !!}</div>
							</div>
						</div>
					@endforeach
					
				</div>
			</div>
		</div>
@endsection