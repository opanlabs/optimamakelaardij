/*
SQLyog Ultimate v11.11 (64 bit)
MySQL - 5.6.24 : Database - optima
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`optima` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `optima`;

/*Table structure for table `activity_log` */

DROP TABLE IF EXISTS `activity_log`;

CREATE TABLE `activity_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `text` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ip_address` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=343 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `activity_log` */

insert  into `activity_log`(`id`,`user_id`,`text`,`ip_address`,`created_at`,`updated_at`) values (1,1,'Banner(1) \'Welkom bij Optima Makelaardij!\' was created','::1','2017-03-12 18:34:44','2017-03-12 18:34:44'),(2,1,'Image \'slider1.jpg\' uploaded with a size of: 177294','::1','2017-03-12 18:39:17','2017-03-12 18:39:17'),(3,1,'Banner(2) \'Welkom bij Optima Makelaardij!2\' was created','::1','2017-03-12 18:40:37','2017-03-12 18:40:37'),(4,1,'Image \'slider1.jpg\' uploaded with a size of: 177294','::1','2017-03-12 18:40:58','2017-03-12 18:40:58'),(5,1,'Block(1) \'test\' was created','::1','2017-03-16 11:04:50','2017-03-16 11:04:50'),(6,1,'Block(1) \'test\' was updated','::1','2017-03-16 11:04:50','2017-03-16 11:04:50'),(7,1,'Block(1) \'Volg ons!l3\' was updated','::1','2017-03-16 11:27:23','2017-03-16 11:27:23'),(8,1,'Block(2) \'0229 - 50 66 90\' was created','::1','2017-03-16 11:27:51','2017-03-16 11:27:51'),(9,1,'Block(2) \'0229 - 50 66 90\' was updated','::1','2017-03-16 11:27:52','2017-03-16 11:27:52'),(10,1,'Block(2) \'0229 - 50 66 90\' was updated','::1','2017-03-16 11:28:17','2017-03-16 11:28:17'),(11,1,'Block(3) \'info@optimamakelaardij.nl\' was created','::1','2017-03-16 11:29:27','2017-03-16 11:29:27'),(12,1,'Block(3) \'info@optimamakelaardij.nl\' was updated','::1','2017-03-16 11:29:28','2017-03-16 11:29:28'),(13,1,'Block(3) \'info@optimamakelaardij.nl\' was updated','::1','2017-03-16 11:30:07','2017-03-16 11:30:07'),(14,1,'Block(1) \'Facebook\' was updated','::1','2017-03-16 17:14:47','2017-03-16 17:14:47'),(15,1,'Block(2) \'Phone\' was updated','::1','2017-03-16 17:18:45','2017-03-16 17:18:45'),(16,1,'Block(3) \'Email\' was updated','::1','2017-03-16 17:19:36','2017-03-16 17:19:36'),(17,1,'Block(2) \'Phone\' was updated','::1','2017-03-16 17:20:07','2017-03-16 17:20:07'),(18,1,'Category(1) \'TE KOOP\' was created','::1','2017-03-19 17:45:25','2017-03-19 17:45:25'),(19,1,'Category(1) \'TE KOOP\' was updated','::1','2017-03-19 17:45:25','2017-03-19 17:45:25'),(20,1,'Product(1) \'test\' was created','::1','2017-03-19 17:51:43','2017-03-19 17:51:43'),(21,1,'Product(1) \'test\' was updated','::1','2017-03-19 17:51:44','2017-03-19 17:51:44'),(22,1,'Image \'thumb1.jpg\' uploaded with a size of: 27780','::1','2017-03-19 17:53:19','2017-03-19 17:53:19'),(23,1,'User(1) \'Guus\' was updated','::1','2017-03-20 18:53:05','2017-03-20 18:53:05'),(24,1,'Product(1) \'\' was deleted','::1','2017-03-20 19:51:51','2017-03-20 19:51:51'),(25,1,'Category(1) \'\' was deleted','::1','2017-03-20 20:28:10','2017-03-20 20:28:10'),(26,1,'User(1) \'Guus\' was updated','::1','2017-03-20 20:31:08','2017-03-20 20:31:08'),(27,1,'User(1) \'Guus\' was updated','::1','2017-03-20 20:33:32','2017-03-20 20:33:32'),(28,1,'Category(2) \'House\' was created','::1','2017-03-20 20:42:06','2017-03-20 20:42:06'),(29,1,'Category(2) \'House\' was updated','::1','2017-03-20 20:42:07','2017-03-20 20:42:07'),(30,1,'Category(3) \'Apartement\' was created','::1','2017-03-20 20:42:35','2017-03-20 20:42:35'),(31,1,'Category(3) \'Apartement\' was updated','::1','2017-03-20 20:42:36','2017-03-20 20:42:36'),(32,1,'Product(2) \'Veendijk 3, Zwaag\' was created','::1','2017-03-20 20:46:21','2017-03-20 20:46:21'),(33,1,'Product(2) \'Veendijk 3, Zwaag\' was updated','::1','2017-03-20 20:46:21','2017-03-20 20:46:21'),(34,1,'Product(2) \'Veendijk 3, Zwaag\' was updated','::1','2017-03-20 20:46:46','2017-03-20 20:46:46'),(35,1,'Product(2) \'Veendijk 3, Zwaag\' was updated','::1','2017-03-20 21:34:36','2017-03-20 21:34:36'),(36,1,'Product(2) \'Veendijk 3, Zwaag\' was updated','::1','2017-03-20 21:38:40','2017-03-20 21:38:40'),(37,1,'Product(2) \'Veendijk 3, Zwaag\' was updated','::1','2017-03-20 21:39:14','2017-03-20 21:39:14'),(38,1,'Product(2) \'Veendijk 3, Zwaag\' was updated','::1','2017-03-20 21:39:56','2017-03-20 21:39:56'),(39,1,'Product(2) \'Veendijk 3, Zwaag\' was updated','::1','2017-03-20 21:40:15','2017-03-20 21:40:15'),(40,1,'Product(2) \'Veendijk 3, Zwaag\' was updated','::1','2017-03-20 21:41:07','2017-03-20 21:41:07'),(41,1,'Product(2) \'Veendijk 3, Zwaag\' was updated','::1','2017-03-20 21:41:42','2017-03-20 21:41:42'),(42,1,'Product(2) \'Veendijk 3, Zwaag\' was updated','::1','2017-03-20 21:41:58','2017-03-20 21:41:58'),(43,1,'Product(2) \'Veendijk 3, Zwaag\' was updated','::1','2017-03-21 04:32:03','2017-03-21 04:32:03'),(44,1,'Product(2) \'Veendijk 3, Zwaag\' was updated','::1','2017-03-21 04:33:32','2017-03-21 04:33:32'),(45,1,'Product(2) \'Veendijk 3, Zwaag\' was updated','::1','2017-03-21 04:34:23','2017-03-21 04:34:23'),(46,1,'Product(2) \'Veendijk 3, Zwaag\' was updated','::1','2017-03-21 04:34:43','2017-03-21 04:34:43'),(47,1,'Product(2) \'Veendijk 3, Zwaag\' was updated','::1','2017-03-21 04:35:07','2017-03-21 04:35:07'),(48,1,'Product(2) \'Veendijk 3, Zwaag\' was updated','::1','2017-03-21 04:36:23','2017-03-21 04:36:23'),(49,1,'Product(2) \'Veendijk 3, Zwaag\' was updated','::1','2017-03-21 04:38:07','2017-03-21 04:38:07'),(50,1,'Product(2) \'Veendijk 3, Zwaag\' was updated','::1','2017-03-21 04:38:38','2017-03-21 04:38:38'),(51,1,'Product(2) \'Veendijk 3, Zwaag\' was updated','::1','2017-03-21 04:42:34','2017-03-21 04:42:34'),(52,1,'Product(2) \'Veendijk 3, Zwaag\' was updated','::1','2017-03-21 04:45:09','2017-03-21 04:45:09'),(53,1,'Product(2) \'Veendijk 3, Zwaag\' was updated','::1','2017-03-21 04:45:35','2017-03-21 04:45:35'),(54,1,'Product(2) \'Veendijk 3, Zwaag\' was updated','::1','2017-03-21 04:46:26','2017-03-21 04:46:26'),(55,1,'Product(2) \'Veendijk 3, Zwaag\' was updated','::1','2017-03-21 04:46:41','2017-03-21 04:46:41'),(56,1,'Product(2) \'Veendijk 3, Zwaag\' was updated','::1','2017-03-21 04:47:23','2017-03-21 04:47:23'),(57,1,'Product(2) \'Veendijk 3, Zwaag\' was updated','::1','2017-03-21 04:47:49','2017-03-21 04:47:49'),(58,1,'Product(2) \'Veendijk 3, Zwaag\' was updated','::1','2017-03-21 04:52:11','2017-03-21 04:52:11'),(59,1,'Product(2) \'Veendijk 3, Zwaag\' was updated','::1','2017-03-21 04:52:40','2017-03-21 04:52:40'),(60,1,'Product(3) \'wahyu\' was created','::1','2017-03-21 05:05:16','2017-03-21 05:05:16'),(61,1,'Product(3) \'wahyu\' was updated','::1','2017-03-21 05:05:16','2017-03-21 05:05:16'),(62,1,'Product(3) \'wahyu\' was updated','::1','2017-03-21 05:05:59','2017-03-21 05:05:59'),(63,1,'Product(3) \'wahyu\' was updated','::1','2017-03-21 05:06:56','2017-03-21 05:06:56'),(64,1,'Product(3) \'wahyu\' was updated','::1','2017-03-21 05:07:26','2017-03-21 05:07:26'),(65,1,'Product(3) \'wahyu\' was updated','::1','2017-03-21 05:07:42','2017-03-21 05:07:42'),(66,1,'Product(3) \'wahyu\' was updated','::1','2017-03-21 05:08:23','2017-03-21 05:08:23'),(67,1,'Product(3) \'wahyu\' was updated','::1','2017-03-21 05:08:34','2017-03-21 05:08:34'),(68,1,'Product(3) \'wahyu\' was updated','::1','2017-03-21 05:08:53','2017-03-21 05:08:53'),(69,1,'Product(3) \'wahyu\' was updated','::1','2017-03-21 05:13:09','2017-03-21 05:13:09'),(70,1,'Product(3) \'wahyu\' was updated','::1','2017-03-21 05:13:31','2017-03-21 05:13:31'),(71,1,'Product(3) \'wahyu\' was updated','::1','2017-03-21 05:13:54','2017-03-21 05:13:54'),(72,1,'Product(3) \'wahyu\' was updated','::1','2017-03-21 05:15:38','2017-03-21 05:15:38'),(73,1,'Product(3) \'wahyu\' was updated','::1','2017-03-21 05:15:58','2017-03-21 05:15:58'),(74,1,'Product(4) \'coba\' was created','::1','2017-03-21 05:23:58','2017-03-21 05:23:58'),(75,1,'Product(4) \'coba\' was updated','::1','2017-03-21 05:23:58','2017-03-21 05:23:58'),(76,1,'Product(5) \'coba\' was created','::1','2017-03-21 05:25:00','2017-03-21 05:25:00'),(77,1,'Product(5) \'coba\' was updated','::1','2017-03-21 05:25:00','2017-03-21 05:25:00'),(78,1,'Product(6) \'coba\' was created','::1','2017-03-21 05:25:46','2017-03-21 05:25:46'),(79,1,'Product(6) \'coba\' was updated','::1','2017-03-21 05:25:47','2017-03-21 05:25:47'),(80,1,'Product(7) \'coba\' was created','::1','2017-03-21 05:26:04','2017-03-21 05:26:04'),(81,1,'Product(7) \'coba\' was updated','::1','2017-03-21 05:26:04','2017-03-21 05:26:04'),(82,1,'Product(2) \'Veendijk 3, Zwaag\' was updated','::1','2017-03-21 07:06:46','2017-03-21 07:06:46'),(83,1,'Product(3) \'\' was deleted','::1','2017-03-21 07:10:21','2017-03-21 07:10:21'),(84,1,'Product(4) \'\' was deleted','::1','2017-03-21 07:10:26','2017-03-21 07:10:26'),(85,1,'Product(5) \'\' was deleted','::1','2017-03-21 07:10:32','2017-03-21 07:10:32'),(86,1,'Product(6) \'\' was deleted','::1','2017-03-21 07:10:37','2017-03-21 07:10:37'),(87,1,'Product(7) \'\' was deleted','::1','2017-03-21 07:10:42','2017-03-21 07:10:42'),(88,1,'Product(9) \'cobah\' was created','::1','2017-03-21 07:11:15','2017-03-21 07:11:15'),(89,1,'Product(9) \'cobah\' was updated','::1','2017-03-21 07:11:15','2017-03-21 07:11:15'),(90,1,'Product(9) \'cobah\' was updated','::1','2017-03-21 07:13:25','2017-03-21 07:13:25'),(91,1,'Product(9) \'cobah\' was updated','::1','2017-03-21 07:17:15','2017-03-21 07:17:15'),(92,1,'Product(2) \'Veendijk 3, Zwaag\' was updated','::1','2017-03-21 07:17:38','2017-03-21 07:17:38'),(93,1,'Product(2) \'Veendijk 3, Zwaag\' was updated','::1','2017-03-21 07:18:08','2017-03-21 07:18:08'),(94,1,'Product(9) \'\' was deleted','::1','2017-03-21 22:20:38','2017-03-21 22:20:38'),(95,1,'Product(2) \'Veendijk 3, Zwaag\' was updated','::1','2017-03-21 22:46:59','2017-03-21 22:46:59'),(96,1,'Product(2) \'Veendijk 3, Zwaag\' was updated','::1','2017-03-21 22:57:25','2017-03-21 22:57:25'),(97,1,'Image \'thumb1.jpg\' uploaded with a size of: 27780','::1','2017-03-21 23:03:15','2017-03-21 23:03:15'),(98,1,'Product(2) \'Veendijk 3, Zwaag\' was updated','::1','2017-03-21 23:33:26','2017-03-21 23:33:26'),(99,1,'Product(2) \'Veendijk 3, Zwaag\' was updated','::1','2017-03-22 00:30:10','2017-03-22 00:30:10'),(100,1,'Product(2) \'Veendijk 3, Zwaag\' was updated','::1','2017-03-22 00:31:04','2017-03-22 00:31:04'),(101,1,'Product(2) \'Veendijk 3, Zwaag\' was updated','::1','2017-03-22 00:39:14','2017-03-22 00:39:14'),(102,1,'Product(10) \'fdggfdg\' was created','::1','2017-03-22 00:42:08','2017-03-22 00:42:08'),(103,1,'Product(11) \'fdggfdg\' was created','::1','2017-03-22 00:43:15','2017-03-22 00:43:15'),(104,1,'Product(11) \'fdggfdg\' was updated','::1','2017-03-22 00:43:15','2017-03-22 00:43:15'),(105,1,'Product(11) \'fdggfdg\' was updated','::1','2017-03-22 00:43:33','2017-03-22 00:43:33'),(106,1,'Banner(1) \'coba\' was created','::1','2017-03-22 21:28:43','2017-03-22 21:28:43'),(107,1,'User(1) \'Guus\' was updated','::1','2017-03-22 21:59:15','2017-03-22 21:59:15'),(108,1,'Image \'thumb-refrence.jpg\' uploaded with a size of: 44333','::1','2017-03-22 22:05:38','2017-03-22 22:05:38'),(109,1,'Product(11) \'\' was deleted','::1','2017-03-23 01:37:17','2017-03-23 01:37:17'),(110,1,'Product(12) \'Veendijk 3, Zwaag\' was created','::1','2017-03-23 01:38:29','2017-03-23 01:38:29'),(111,1,'Product(12) \'Veendijk 3, Zwaag\' was updated','::1','2017-03-23 01:38:29','2017-03-23 01:38:29'),(112,1,'Product(12) \'Veendijk 3, Zwaag\' was updated','::1','2017-03-23 01:38:40','2017-03-23 01:38:40'),(113,1,'Image \'thumb2.jpg\' uploaded with a size of: 30207','::1','2017-03-23 01:39:22','2017-03-23 01:39:22'),(114,1,'Product(12) \'Veendijk 3, Zwaag\' was updated','::1','2017-03-23 01:45:26','2017-03-23 01:45:26'),(115,1,'Banner(1) \'coba\' was updated','::1','2017-03-23 06:13:09','2017-03-23 06:13:09'),(116,1,'Banner(1) \'coba\' was updated','::1','2017-03-23 06:18:50','2017-03-23 06:18:50'),(117,1,'Product(2) \'Veendijk 3, Zwaag\' was updated','::1','2017-03-23 07:13:45','2017-03-23 07:13:45'),(118,1,'Product(2) \'Veendijk 3, Zwaag\' was updated','::1','2017-03-23 07:14:15','2017-03-23 07:14:15'),(119,1,'Product(13) \'test\' was created','::1','2017-03-23 07:15:44','2017-03-23 07:15:44'),(120,1,'Product(13) \'test\' was updated','::1','2017-03-23 07:15:44','2017-03-23 07:15:44'),(121,1,'Product(14) \'wowo\' was created','::1','2017-03-23 07:19:59','2017-03-23 07:19:59'),(122,1,'Product(14) \'wowo\' was updated','::1','2017-03-23 07:20:00','2017-03-23 07:20:00'),(123,1,'Product(15) \'wowo\' was created','::1','2017-03-23 07:21:04','2017-03-23 07:21:04'),(124,1,'Product(15) \'wowo\' was updated','::1','2017-03-23 07:21:05','2017-03-23 07:21:05'),(125,1,'Product(16) \'wowo1\' was created','::1','2017-03-23 07:22:50','2017-03-23 07:22:50'),(126,1,'Product(16) \'wowo1\' was updated','::1','2017-03-23 07:22:51','2017-03-23 07:22:51'),(127,1,'Product(17) \'wowo1\' was created','::1','2017-03-23 07:39:36','2017-03-23 07:39:36'),(128,1,'Product(17) \'wowo1\' was updated','::1','2017-03-23 07:39:36','2017-03-23 07:39:36'),(129,1,'Product(13) \'\' was deleted','::1','2017-03-23 07:40:03','2017-03-23 07:40:03'),(130,1,'Product(14) \'\' was deleted','::1','2017-03-23 07:40:10','2017-03-23 07:40:10'),(131,1,'Product(15) \'\' was deleted','::1','2017-03-23 07:40:26','2017-03-23 07:40:26'),(132,1,'Product(16) \'\' was deleted','::1','2017-03-23 07:40:33','2017-03-23 07:40:33'),(133,1,'Product(17) \'\' was deleted','::1','2017-03-23 07:40:53','2017-03-23 07:40:53'),(134,1,'Product(18) \'topan\' was created','::1','2017-03-23 07:41:45','2017-03-23 07:41:45'),(135,1,'Product(18) \'topan\' was updated','::1','2017-03-23 07:41:46','2017-03-23 07:41:46'),(136,1,'Product(18) \'topan\' was updated','::1','2017-03-23 07:41:59','2017-03-23 07:41:59'),(137,1,'Product(18) \'\' was deleted','::1','2017-03-23 07:42:37','2017-03-23 07:42:37'),(138,1,'Page(1) \'tester\' was created','::1','2017-03-23 09:56:34','2017-03-23 09:56:34'),(139,1,'Page(1) \'tester\' was updated','::1','2017-03-23 09:56:35','2017-03-23 09:56:35'),(140,1,'Page(1) \'tester\' was updated','::1','2017-03-23 09:56:45','2017-03-23 09:56:45'),(141,1,'Image \'slider-object2.jpg\' uploaded with a size of: 265105','::1','2017-03-23 10:10:22','2017-03-23 10:10:22'),(142,1,'Page(2) \'yoi\' was created','::1','2017-03-23 10:11:16','2017-03-23 10:11:16'),(143,1,'Page(2) \'yoi\' was updated','::1','2017-03-23 10:11:17','2017-03-23 10:11:17'),(144,1,'Page(2) \'yoi\' was updated','::1','2017-03-23 10:11:17','2017-03-23 10:11:17'),(145,1,'Page(2) \'\' was deleted','::1','2017-03-23 10:11:38','2017-03-23 10:11:38'),(146,1,'Page(1) \'\' was deleted','::1','2017-03-23 10:12:02','2017-03-23 10:12:02'),(147,1,'Product(19) \'tetetete\' was created','::1','2017-03-23 10:24:10','2017-03-23 10:24:10'),(148,1,'Product(19) \'tetetete\' was updated','::1','2017-03-23 10:24:10','2017-03-23 10:24:10'),(149,1,'Product(12) \'Veendijk 3, Zwaag\' was updated','::1','2017-03-23 10:51:40','2017-03-23 10:51:40'),(150,1,'Product(2) \'Veendijk 3, Zwaag\' was updated','::1','2017-03-23 10:52:08','2017-03-23 10:52:08'),(151,1,'Category(3) \'\' was deleted','::1','2017-03-23 16:15:56','2017-03-23 16:15:56'),(152,1,'Category(2) \'Objecten\' was updated','::1','2017-03-23 16:16:33','2017-03-23 16:16:33'),(153,1,'Product(12) \'\' was deleted','::1','2017-03-23 16:16:50','2017-03-23 16:16:50'),(154,1,'Product(19) \'\' was deleted','::1','2017-03-23 16:16:58','2017-03-23 16:16:58'),(155,1,'Product(20) \'Veendijk 3, Zwaag\' was created','::1','2017-03-23 16:18:24','2017-03-23 16:18:24'),(156,1,'Product(20) \'Veendijk 3, Zwaag\' was updated','::1','2017-03-23 16:18:24','2017-03-23 16:18:24'),(157,1,'Product(21) \'Veendijk 3, Zwaag2\' was created','::1','2017-03-23 16:19:29','2017-03-23 16:19:29'),(158,1,'Product(21) \'Veendijk 3, Zwaag2\' was updated','::1','2017-03-23 16:19:29','2017-03-23 16:19:29'),(159,1,'Product(20) \'Veendijk 3, Zwaag\' was updated','::1','2017-03-23 16:19:38','2017-03-23 16:19:38'),(160,1,'Product(21) \'Veendijk 3, Zwaag2\' was updated','::1','2017-03-23 16:19:39','2017-03-23 16:19:39'),(161,1,'Product(20) \'Veendijk 3, Zwaag\' was updated','::1','2017-03-23 16:20:19','2017-03-23 16:20:19'),(162,1,'Product(21) \'Veendijk 3, Zwaag2\' was updated','::1','2017-03-23 16:20:41','2017-03-23 16:20:41'),(163,1,'Image \'thumb1.jpg\' uploaded with a size of: 27780','::1','2017-03-23 16:33:57','2017-03-23 16:33:57'),(164,1,'Image \'thumb2.jpg\' uploaded with a size of: 30207','::1','2017-03-23 16:34:55','2017-03-23 16:34:55'),(165,1,'User(1) \'Guus\' was updated','::1','2017-03-23 17:13:51','2017-03-23 17:13:51'),(166,1,'Image \'thumb2.jpg\' uploaded with a size of: 30207','::1','2017-03-23 18:38:20','2017-03-23 18:38:20'),(167,1,'Image \'slider-object-bottom.jpg\' uploaded with a size of: 308618','::1','2017-03-23 18:38:20','2017-03-23 18:38:20'),(168,1,'Page(1) \'coba nih\' was created','::1','2017-03-28 09:57:16','2017-03-28 09:57:16'),(169,1,'Page(1) \'coba nih\' was updated','::1','2017-03-28 09:57:17','2017-03-28 09:57:17'),(170,1,'Image \'bg-warmee.png\' uploaded with a size of: 3182','::1','2017-03-28 10:16:46','2017-03-28 10:16:46'),(171,1,'User(1) \'Guus\' was updated','::1','2017-03-28 10:34:09','2017-03-28 10:34:09'),(172,1,'Page(1) \'coba nih\' was updated','::1','2017-03-28 11:05:43','2017-03-28 11:05:43'),(173,1,'Page(2) \'coba1\' was created','::1','2017-03-28 11:17:54','2017-03-28 11:17:54'),(174,1,'Page(2) \'coba1\' was updated','::1','2017-03-28 11:17:54','2017-03-28 11:17:54'),(175,1,'Page(2) \'coba1\' was updated','::1','2017-03-28 11:17:55','2017-03-28 11:17:55'),(176,1,'Page(2) \'coba1\' was updated','::1','2017-03-28 11:18:03','2017-03-28 11:18:03'),(177,1,'Block(1) \'Facebook\' was updated','::1','2017-03-28 17:40:14','2017-03-28 17:40:14'),(178,1,'Block(3) \'Email\' was updated','::1','2017-03-28 17:41:45','2017-03-28 17:41:45'),(179,1,'User(1) \'Guus\' was updated','::1','2017-03-28 17:44:30','2017-03-28 17:44:30'),(180,1,'Block(4) \'Funda\' was created','::1','2017-03-28 17:48:22','2017-03-28 17:48:22'),(181,1,'Block(4) \'Funda\' was updated','::1','2017-03-28 17:48:23','2017-03-28 17:48:23'),(182,1,'Block(4) \'Funda\' was updated','::1','2017-03-28 17:48:34','2017-03-28 17:48:34'),(183,1,'Page(1) \'\' was deleted','::1','2017-03-28 17:49:33','2017-03-28 17:49:33'),(184,1,'Page(3) \'Home\' was created','::1','2017-03-28 17:50:23','2017-03-28 17:50:23'),(185,1,'Page(3) \'Home\' was updated','::1','2017-03-28 17:50:23','2017-03-28 17:50:23'),(186,1,'Page(4) \'Referenties\' was created','::1','2017-03-28 17:51:06','2017-03-28 17:51:06'),(187,1,'Page(4) \'Referenties\' was updated','::1','2017-03-28 17:51:06','2017-03-28 17:51:06'),(188,1,'Page(4) \'Referenties\' was updated','::1','2017-03-28 17:51:07','2017-03-28 17:51:07'),(189,1,'Page(4) \'\' was deleted','::1','2017-03-28 17:56:21','2017-03-28 17:56:21'),(190,1,'Page(5) \'VERKOOP\' was created','::1','2017-03-28 17:56:57','2017-03-28 17:56:57'),(191,1,'Page(5) \'VERKOOP\' was updated','::1','2017-03-28 17:56:58','2017-03-28 17:56:58'),(192,1,'Page(5) \'VERKOOP\' was updated','::1','2017-03-28 17:56:58','2017-03-28 17:56:58'),(193,1,'Page(6) \'AANKOOP\' was created','::1','2017-03-28 17:57:53','2017-03-28 17:57:53'),(194,1,'Page(6) \'AANKOOP\' was updated','::1','2017-03-28 17:57:53','2017-03-28 17:57:53'),(195,1,'Page(6) \'AANKOOP\' was updated','::1','2017-03-28 17:57:53','2017-03-28 17:57:53'),(196,1,'Page(7) \'TAXATIES\' was created','::1','2017-03-28 17:58:29','2017-03-28 17:58:29'),(197,1,'Page(7) \'TAXATIES\' was updated','::1','2017-03-28 17:58:29','2017-03-28 17:58:29'),(198,1,'Page(7) \'TAXATIES\' was updated','::1','2017-03-28 17:58:29','2017-03-28 17:58:29'),(199,1,'Page(5) \'VERKOOP\' was updated','::1','2017-03-28 17:58:42','2017-03-28 17:58:42'),(200,1,'Page(6) \'AANKOOP\' was updated','::1','2017-03-28 17:58:43','2017-03-28 17:58:43'),(201,1,'Page(7) \'TAXATIES\' was updated','::1','2017-03-28 17:58:43','2017-03-28 17:58:43'),(202,1,'Page(8) \'Over Optima\' was created','::1','2017-03-28 18:00:03','2017-03-28 18:00:03'),(203,1,'Page(8) \'Over Optima\' was updated','::1','2017-03-28 18:00:03','2017-03-28 18:00:03'),(204,1,'Page(9) \'Goede prijs/kwaliteit verhouding\' was created','::1','2017-03-28 18:03:15','2017-03-28 18:03:15'),(205,1,'Page(9) \'Goede prijs/kwaliteit verhouding\' was updated','::1','2017-03-28 18:03:15','2017-03-28 18:03:15'),(206,1,'Page(9) \'Goede prijs/kwaliteit verhouding\' was updated','::1','2017-03-28 18:03:16','2017-03-28 18:03:16'),(207,1,'Page(10) \'Goede positionering in de woningmarkt\' was created','::1','2017-03-28 18:03:54','2017-03-28 18:03:54'),(208,1,'Page(10) \'Goede positionering in de woningmarkt\' was updated','::1','2017-03-28 18:03:54','2017-03-28 18:03:54'),(209,1,'Page(10) \'Goede positionering in de woningmarkt\' was updated','::1','2017-03-28 18:03:55','2017-03-28 18:03:55'),(210,1,'Page(11) \'Persoonlijke aandacht\' was created','::1','2017-03-28 18:04:32','2017-03-28 18:04:32'),(211,1,'Page(11) \'Persoonlijke aandacht\' was updated','::1','2017-03-28 18:04:32','2017-03-28 18:04:32'),(212,1,'Page(11) \'Persoonlijke aandacht\' was updated','::1','2017-03-28 18:04:33','2017-03-28 18:04:33'),(213,1,'Page(12) \'Dynamisch en flexibel\' was created','::1','2017-03-28 18:05:07','2017-03-28 18:05:07'),(214,1,'Page(12) \'Dynamisch en flexibel\' was updated','::1','2017-03-28 18:05:07','2017-03-28 18:05:07'),(215,1,'Page(12) \'Dynamisch en flexibel\' was updated','::1','2017-03-28 18:05:07','2017-03-28 18:05:07'),(216,1,'Page(13) \'Aanbod\' was created','::1','2017-03-28 18:05:56','2017-03-28 18:05:56'),(217,1,'Page(13) \'Aanbod\' was updated','::1','2017-03-28 18:05:56','2017-03-28 18:05:56'),(218,1,'Page(14) \'Diensten\' was created','::1','2017-03-28 18:06:48','2017-03-28 18:06:48'),(219,1,'Page(14) \'Diensten\' was updated','::1','2017-03-28 18:06:48','2017-03-28 18:06:48'),(220,1,'Page(15) \'Taxaties\' was created','::1','2017-03-28 18:07:31','2017-03-28 18:07:31'),(221,1,'Page(15) \'Taxaties\' was updated','::1','2017-03-28 18:07:32','2017-03-28 18:07:32'),(222,1,'Page(15) \'Taxaties\' was updated','::1','2017-03-28 18:07:32','2017-03-28 18:07:32'),(223,1,'Page(16) \'Aankoopbemiddeling\' was created','::1','2017-03-28 18:08:12','2017-03-28 18:08:12'),(224,1,'Page(16) \'Aankoopbemiddeling\' was updated','::1','2017-03-28 18:08:12','2017-03-28 18:08:12'),(225,1,'Page(16) \'Aankoopbemiddeling\' was updated','::1','2017-03-28 18:08:12','2017-03-28 18:08:12'),(226,1,'Page(17) \'Verkoop van uw woning\' was created','::1','2017-03-28 18:08:48','2017-03-28 18:08:48'),(227,1,'Page(17) \'Verkoop van uw woning\' was updated','::1','2017-03-28 18:08:49','2017-03-28 18:08:49'),(228,1,'Page(17) \'Verkoop van uw woning\' was updated','::1','2017-03-28 18:08:49','2017-03-28 18:08:49'),(229,1,'Page(18) \'Gratis waardebepaling\' was created','::1','2017-03-28 18:09:24','2017-03-28 18:09:24'),(230,1,'Page(18) \'Gratis waardebepaling\' was updated','::1','2017-03-28 18:09:24','2017-03-28 18:09:24'),(231,1,'Page(18) \'Gratis waardebepaling\' was updated','::1','2017-03-28 18:09:25','2017-03-28 18:09:25'),(232,1,'Page(19) \'contact\' was created','::1','2017-03-28 18:10:04','2017-03-28 18:10:04'),(233,1,'Page(19) \'contact\' was updated','::1','2017-03-28 18:10:04','2017-03-28 18:10:04'),(234,1,'Page(19) \'Contact\' was updated','::1','2017-03-28 18:10:40','2017-03-28 18:10:40'),(235,1,'Block(5) \'Contact ons!\' was created','::1','2017-03-28 18:11:37','2017-03-28 18:11:37'),(236,1,'Block(5) \'Contact ons!\' was updated','::1','2017-03-28 18:11:38','2017-03-28 18:11:38'),(237,1,'Page(20) \'Contactformulier\' was created','::1','2017-03-28 18:13:41','2017-03-28 18:13:41'),(238,1,'Page(20) \'Contactformulier\' was updated','::1','2017-03-28 18:13:41','2017-03-28 18:13:41'),(239,1,'Page(20) \'Contactformulier\' was updated','::1','2017-03-28 18:13:41','2017-03-28 18:13:41'),(240,1,'Page(21) \'Contactgegevens\' was created','::1','2017-03-28 18:14:27','2017-03-28 18:14:27'),(241,1,'Page(21) \'Contactgegevens\' was updated','::1','2017-03-28 18:14:28','2017-03-28 18:14:28'),(242,1,'Page(21) \'Contactgegevens\' was updated','::1','2017-03-28 18:14:28','2017-03-28 18:14:28'),(243,1,'Page(22) \'Optima Makelaardij\' was created','::1','2017-03-28 18:15:21','2017-03-28 18:15:21'),(244,1,'Page(22) \'Optima Makelaardij\' was updated','::1','2017-03-28 18:15:21','2017-03-28 18:15:21'),(245,1,'Page(22) \'Optima Makelaardij\' was updated','::1','2017-03-28 18:15:22','2017-03-28 18:15:22'),(246,1,'Page(22) \'Optima Makelaardij\' was updated','::1','2017-03-28 18:18:34','2017-03-28 18:18:34'),(247,1,'Page(23) \'Facebook\' was created','::1','2017-03-28 18:19:16','2017-03-28 18:19:16'),(248,1,'Page(23) \'Facebook\' was updated','::1','2017-03-28 18:19:16','2017-03-28 18:19:16'),(249,1,'Page(23) \'\' was deleted','::1','2017-03-28 18:19:52','2017-03-28 18:19:52'),(250,1,'Page(24) \'Facebook\' was created','::1','2017-03-28 18:20:22','2017-03-28 18:20:22'),(251,1,'Page(24) \'Facebook\' was updated','::1','2017-03-28 18:20:22','2017-03-28 18:20:22'),(252,1,'Page(24) \'Facebook\' was updated','::1','2017-03-28 18:20:23','2017-03-28 18:20:23'),(253,1,'Page(25) \'Instagram\' was created','::1','2017-03-28 18:21:09','2017-03-28 18:21:09'),(254,1,'Page(25) \'Instagram\' was updated','::1','2017-03-28 18:21:09','2017-03-28 18:21:09'),(255,1,'Page(25) \'Instagram\' was updated','::1','2017-03-28 18:21:10','2017-03-28 18:21:10'),(256,1,'Page(9) \'Goede prijs/kwaliteit verhouding\' was updated','::1','2017-03-28 18:26:56','2017-03-28 18:26:56'),(257,1,'Page(10) \'Goede positionering in de woningmarkt\' was updated','::1','2017-03-28 18:26:57','2017-03-28 18:26:57'),(258,1,'Page(11) \'Persoonlijke aandacht\' was updated','::1','2017-03-28 18:26:58','2017-03-28 18:26:58'),(259,1,'Page(12) \'Dynamisch en flexibel\' was updated','::1','2017-03-28 18:26:59','2017-03-28 18:26:59'),(260,1,'Page(9) \'Goede prijs/kwaliteit verhouding\' was updated','::1','2017-03-28 18:48:45','2017-03-28 18:48:45'),(261,1,'Page(9) \'Goede prijs/kwaliteit verhouding\' was updated','::1','2017-03-28 18:52:29','2017-03-28 18:52:29'),(262,1,'Page(10) \'Goede positionering in de woningmarkt\' was updated','::1','2017-03-28 18:54:17','2017-03-28 18:54:17'),(263,1,'Page(11) \'Persoonlijke aandacht\' was updated','::1','2017-03-28 18:55:04','2017-03-28 18:55:04'),(264,1,'Page(12) \'Dynamisch en flexibel\' was updated','::1','2017-03-28 18:55:42','2017-03-28 18:55:42'),(265,1,'Page(15) \'Taxaties\' was updated','::1','2017-03-28 19:16:08','2017-03-28 19:16:08'),(266,1,'Page(15) \'Taxaties\' was updated','::1','2017-03-28 19:16:27','2017-03-28 19:16:27'),(267,1,'Page(16) \'Aankoopbemiddeling\' was updated','::1','2017-03-28 19:16:28','2017-03-28 19:16:28'),(268,1,'Page(17) \'Verkoop van uw woning\' was updated','::1','2017-03-28 19:16:29','2017-03-28 19:16:29'),(269,1,'Page(18) \'Gratis waardebepaling\' was updated','::1','2017-03-28 19:16:30','2017-03-28 19:16:30'),(270,1,'Page(16) \'Aankoopbemiddeling\' was updated','::1','2017-03-28 19:22:21','2017-03-28 19:22:21'),(271,1,'Page(17) \'Verkoop van uw woning\' was updated','::1','2017-03-28 19:22:59','2017-03-28 19:22:59'),(272,1,'Page(18) \'Gratis waardebepaling\' was updated','::1','2017-03-28 19:23:31','2017-03-28 19:23:31'),(273,1,'Page(26) \'Referenties\' was created','::1','2017-03-28 19:37:44','2017-03-28 19:37:44'),(274,1,'Page(26) \'Referenties\' was updated','::1','2017-03-28 19:37:44','2017-03-28 19:37:44'),(275,1,'Page(26) \'Referenties\' was updated','::1','2017-03-28 19:37:44','2017-03-28 19:37:44'),(276,1,'Page(27) \'Botter 102, Hoorn\' was created','::1','2017-03-28 19:39:33','2017-03-28 19:39:33'),(277,1,'Page(27) \'Botter 102, Hoorn\' was updated','::1','2017-03-28 19:39:33','2017-03-28 19:39:33'),(278,1,'Page(27) \'Botter 102, Hoorn\' was updated','::1','2017-03-28 19:39:33','2017-03-28 19:39:33'),(279,1,'Page(27) \'Botter 102, Hoorn\' was updated','::1','2017-03-28 19:39:52','2017-03-28 19:39:52'),(280,1,'Image \'thumb-refrence.jpg\' uploaded with a size of: 44333','::1','2017-03-28 19:41:29','2017-03-28 19:41:29'),(281,1,'Page(28) \'Gemiddelde beoordeling op\' was created','::1','2017-03-28 19:52:42','2017-03-28 19:52:42'),(282,1,'Page(28) \'Gemiddelde beoordeling op\' was updated','::1','2017-03-28 19:52:43','2017-03-28 19:52:43'),(283,1,'Page(28) \'Gemiddelde beoordeling op\' was updated','::1','2017-03-28 19:52:43','2017-03-28 19:52:43'),(284,1,'Page(5) \'VERKOOP\' was updated','::1','2017-03-28 20:00:08','2017-03-28 20:00:08'),(285,1,'Page(6) \'AANKOOP\' was updated','::1','2017-03-28 20:00:37','2017-03-28 20:00:37'),(286,1,'Page(7) \'TAXATIES\' was updated','::1','2017-03-28 20:01:09','2017-03-28 20:01:09'),(287,1,'Block(2) \'Phone\' was updated','::1','2017-03-28 20:16:08','2017-03-28 20:16:08'),(288,1,'Block(3) \'Email\' was updated','::1','2017-03-28 20:16:39','2017-03-28 20:16:39'),(289,1,'Page(29) \'submenu\' was created','::1','2017-03-28 20:32:43','2017-03-28 20:32:43'),(290,1,'Page(29) \'submenu\' was updated','::1','2017-03-28 20:32:44','2017-03-28 20:32:44'),(291,1,'Page(29) \'submenu\' was updated','::1','2017-03-28 20:32:44','2017-03-28 20:32:44'),(292,1,'Page(29) \'\' was deleted','::1','2017-03-28 20:33:51','2017-03-28 20:33:51'),(293,1,'Block(6) \'Aanbod Link 1\' was created','::1','2017-03-28 20:34:50','2017-03-28 20:34:50'),(294,1,'Block(6) \'Aanbod Link 1\' was updated','::1','2017-03-28 20:34:50','2017-03-28 20:34:50'),(295,1,'Block(7) \'Aanbod Link 2\' was created','::1','2017-03-28 20:35:12','2017-03-28 20:35:12'),(296,1,'Block(7) \'Aanbod Link 2\' was updated','::1','2017-03-28 20:35:12','2017-03-28 20:35:12'),(297,1,'Block(8) \'Aanbod Link 3\' was created','::1','2017-03-28 20:35:33','2017-03-28 20:35:33'),(298,1,'Block(8) \'Aanbod Link 3\' was updated','::1','2017-03-28 20:35:33','2017-03-28 20:35:33'),(299,1,'Block(9) \'Advies Link 1\' was created','::1','2017-03-28 20:36:13','2017-03-28 20:36:13'),(300,1,'Block(9) \'Advies Link 1\' was updated','::1','2017-03-28 20:36:13','2017-03-28 20:36:13'),(301,1,'Block(10) \'Advies Link 2\' was created','::1','2017-03-28 20:36:34','2017-03-28 20:36:34'),(302,1,'Block(10) \'Advies Link 2\' was updated','::1','2017-03-28 20:36:34','2017-03-28 20:36:34'),(303,1,'Block(11) \'Advies Link 3\' was created','::1','2017-03-28 20:36:54','2017-03-28 20:36:54'),(304,1,'Block(11) \'Advies Link 3\' was updated','::1','2017-03-28 20:36:55','2017-03-28 20:36:55'),(305,1,'Block(12) \'footer optimamakelaardij\' was created','::1','2017-03-28 21:37:28','2017-03-28 21:37:28'),(306,1,'Block(12) \'footer optimamakelaardij\' was updated','::1','2017-03-28 21:37:28','2017-03-28 21:37:28'),(307,1,'Block(12) \'footer optimamakelaardij\' was updated','::1','2017-03-28 21:37:48','2017-03-28 21:37:48'),(308,1,'Block(13) \'Bekijk ons gehele aanbod \' was created','::1','2017-03-28 21:40:50','2017-03-28 21:40:50'),(309,1,'Block(13) \'Bekijk ons gehele aanbod \' was updated','::1','2017-03-28 21:40:51','2017-03-28 21:40:51'),(310,1,'Page(30) \'Object\' was created','::1','2017-03-28 21:46:50','2017-03-28 21:46:50'),(311,1,'Page(30) \'Object\' was updated','::1','2017-03-28 21:46:51','2017-03-28 21:46:51'),(312,1,'Page(30) \'\' was deleted','::1','2017-03-28 21:52:44','2017-03-28 21:52:44'),(313,1,'Block(12) \'footer optimamakelaardij\' was updated','::1','2017-03-28 22:39:11','2017-03-28 22:39:11'),(314,1,'Block(5) \'Contact ons!\' was updated','::1','2017-03-28 22:40:27','2017-03-28 22:40:27'),(315,1,'Block(13) \'Bekijk ons gehele aanbod \' was updated','::1','2017-03-28 22:41:36','2017-03-28 22:41:36'),(316,1,'User(1) \'Guus\' was updated','::1','2017-03-28 22:57:40','2017-03-28 22:57:40'),(317,1,'Block(14) \'Contactgegevens footer\' was created','::1','2017-03-29 23:28:32','2017-03-29 23:28:32'),(318,1,'Block(14) \'Contactgegevens footer\' was updated','::1','2017-03-29 23:28:32','2017-03-29 23:28:32'),(319,1,'Block(15) \'Contactgegevens Facebook\' was created','::1','2017-03-29 23:29:20','2017-03-29 23:29:20'),(320,1,'Block(15) \'Contactgegevens Facebook\' was updated','::1','2017-03-29 23:29:20','2017-03-29 23:29:20'),(321,1,'Block(16) \'Logo Footer 1\' was created','::1','2017-03-29 23:31:21','2017-03-29 23:31:21'),(322,1,'Block(16) \'Logo Footer 1\' was updated','::1','2017-03-29 23:31:22','2017-03-29 23:31:22'),(323,1,'Image \'logo-funda.png\' uploaded with a size of: 6433','::1','2017-03-29 23:32:34','2017-03-29 23:32:34'),(324,1,'Block(16) \'Logo Footer 1\' was updated','::1','2017-03-29 23:33:05','2017-03-29 23:33:05'),(325,1,'Block(16) \'Logo Footer 1\' was updated','::1','2017-03-29 23:33:41','2017-03-29 23:33:41'),(326,1,'Block(17) \'Logo Footer 2\' was created','::1','2017-03-29 23:34:26','2017-03-29 23:34:26'),(327,1,'Block(17) \'Logo Footer 2\' was updated','::1','2017-03-29 23:34:26','2017-03-29 23:34:26'),(328,1,'Block(18) \'Logo Footer 3\' was created','::1','2017-03-29 23:35:00','2017-03-29 23:35:00'),(329,1,'Block(18) \'Logo Footer 3\' was updated','::1','2017-03-29 23:35:00','2017-03-29 23:35:00'),(330,1,'Block(19) \'Logo Footer 4\' was created','::1','2017-03-29 23:35:19','2017-03-29 23:35:19'),(331,1,'Block(19) \'Logo Footer 4\' was updated','::1','2017-03-29 23:35:19','2017-03-29 23:35:19'),(332,1,'Block(20) \'Logo Footer 5\' was created','::1','2017-03-29 23:35:39','2017-03-29 23:35:39'),(333,1,'Block(20) \'Logo Footer 5\' was updated','::1','2017-03-29 23:35:40','2017-03-29 23:35:40'),(334,1,'Image \'logo-nww.png\' uploaded with a size of: 6274','::1','2017-03-29 23:36:29','2017-03-29 23:36:29'),(335,1,'Block(17) \'Logo Footer 2\' was updated','::1','2017-03-29 23:36:36','2017-03-29 23:36:36'),(336,1,'Image \'logo-r.png\' uploaded with a size of: 4990','::1','2017-03-29 23:37:08','2017-03-29 23:37:08'),(337,1,'Block(18) \'Logo Footer 3\' was updated','::1','2017-03-29 23:37:10','2017-03-29 23:37:10'),(338,1,'Image \'logo-vastgood.jpg\' uploaded with a size of: 3227','::1','2017-03-29 23:38:55','2017-03-29 23:38:55'),(339,1,'Block(20) \'Logo Footer 5\' was updated','::1','2017-03-29 23:39:07','2017-03-29 23:39:07'),(340,1,'Block(5) \'Contact ons!\' was updated','::1','2017-03-30 00:08:14','2017-03-30 00:08:14'),(341,1,'Page(5) \'VERKOOP\' was updated','::1','2017-03-30 00:19:44','2017-03-30 00:19:44'),(342,1,'Page(5) \'VERKOOP\' was updated','::1','2017-03-30 00:20:25','2017-03-30 00:20:25');

/*Table structure for table `attribute_attribute_set` */

DROP TABLE IF EXISTS `attribute_attribute_set`;

CREATE TABLE `attribute_attribute_set` (
  `attribute_set_id` int(10) unsigned NOT NULL,
  `attribute_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`attribute_set_id`,`attribute_id`),
  KEY `attribute_attribute_set_attribute_set_id_index` (`attribute_set_id`),
  KEY `attribute_attribute_set_attribute_id_index` (`attribute_id`),
  CONSTRAINT `attribute_attribute_set_attribute_id_foreign` FOREIGN KEY (`attribute_id`) REFERENCES `attributes` (`id`) ON DELETE CASCADE,
  CONSTRAINT `attribute_attribute_set_attribute_set_id_foreign` FOREIGN KEY (`attribute_set_id`) REFERENCES `attribute_sets` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `attribute_attribute_set` */

insert  into `attribute_attribute_set`(`attribute_set_id`,`attribute_id`) values (3,7),(3,8),(3,9),(3,10),(3,11);

/*Table structure for table `attribute_product` */

DROP TABLE IF EXISTS `attribute_product`;

CREATE TABLE `attribute_product` (
  `attribute_id` int(10) unsigned NOT NULL,
  `product_id` int(10) unsigned NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`attribute_id`,`product_id`),
  KEY `attribute_product_attribute_id_index` (`attribute_id`),
  KEY `attribute_product_product_id_index` (`product_id`),
  CONSTRAINT `attribute_product_attribute_id_foreign` FOREIGN KEY (`attribute_id`) REFERENCES `attributes` (`id`) ON DELETE CASCADE,
  CONSTRAINT `attribute_product_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `attribute_product` */

insert  into `attribute_product`(`attribute_id`,`product_id`,`value`) values (7,20,'TE KOOP'),(7,21,'TE KOOP'),(8,20,'3'),(8,21,'3'),(9,20,'4'),(9,21,'3'),(10,20,'3'),(10,21,'3'),(11,20,'3'),(11,21,'3');

/*Table structure for table `attribute_sets` */

DROP TABLE IF EXISTS `attribute_sets`;

CREATE TABLE `attribute_sets` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sequence` int(11) NOT NULL,
  `active` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `attribute_sets_active_index` (`active`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `attribute_sets` */

insert  into `attribute_sets`(`id`,`name`,`description`,`sequence`,`active`,`created_at`,`updated_at`) values (3,'object','object',1,1,'2017-03-20 20:29:53','2017-03-23 16:10:39');

/*Table structure for table `attributes` */

DROP TABLE IF EXISTS `attributes`;

CREATE TABLE `attributes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sequence` int(11) NOT NULL,
  `active` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `attributes_name_index` (`name`),
  KEY `attributes_active_index` (`active`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `attributes` */

insert  into `attributes`(`id`,`type`,`name`,`code`,`label`,`sequence`,`active`,`created_at`,`updated_at`) values (7,'textfield','status','status','Status',1,0,'2017-03-23 16:12:38','2017-03-23 16:12:38'),(8,'textfield','aanvaarding','aanvaarding','Aanvaarding',2,0,'2017-03-23 16:13:02','2017-03-23 16:13:02'),(9,'textfield','slaapkamers','slaapkamers','Slaapkamers',3,0,'2017-03-23 16:13:35','2017-03-23 16:13:35'),(10,'textfield','Soort woonhuis','soort_woonhuis','Soort woonhuis',4,0,'2017-03-23 16:14:29','2017-03-23 16:14:29'),(11,'textfield','Type woonhuis','type_woonhuis','Type woonhuis',5,0,'2017-03-23 16:15:10','2017-03-23 16:15:10');

/*Table structure for table `banners` */

DROP TABLE IF EXISTS `banners`;

CREATE TABLE `banners` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `sequence` int(11) NOT NULL,
  `active` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `banners_title_index` (`title`),
  KEY `banners_active_index` (`active`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `banners` */

insert  into `banners`(`id`,`title`,`description`,`sequence`,`active`,`created_at`,`updated_at`) values (1,'Welkom bij Optima Makelaardij!','<p>Optima Makelaardij is een jong en dynamisch makelaarskantoor wat zich richt op de bemiddeling bij aan- en verkoop van onroerend goed. &nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;</p>\r\n',1,0,'2017-03-12 18:34:44','2017-03-12 18:34:44'),(2,'Welkom bij Optima Makelaardij!2','<p>Optima Makelaardij is een jong en dynamisch makelaarskantoor wat zich richt op de bemiddeling bij aan- en verkoop van onroerend goed</p>\r\n',2,0,'2017-03-12 18:40:36','2017-03-12 18:40:36');

/*Table structure for table `block_translations` */

DROP TABLE IF EXISTS `block_translations`;

CREATE TABLE `block_translations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `block_id` int(10) unsigned NOT NULL,
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_keywords` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text` text COLLATE utf8_unicode_ci NOT NULL,
  `button_link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `button_text` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `block_translations_block_id_locale_unique` (`block_id`,`locale`),
  KEY `block_translations_locale_index` (`locale`),
  KEY `block_translations_slug_index` (`slug`),
  CONSTRAINT `block_translations_block_id_foreign` FOREIGN KEY (`block_id`) REFERENCES `blocks` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `block_translations` */

insert  into `block_translations`(`id`,`block_id`,`locale`,`title`,`slug`,`meta_title`,`meta_description`,`meta_keywords`,`text`,`button_link`,`button_text`) values (1,1,'nl','Facebook','Volg ons!l3','','','','<p>Volg ons!l3</p>\r\n',NULL,NULL),(2,1,'en','Volg ons!l3','#','','','','<p>test</p>\r\n',NULL,NULL),(3,2,'nl','Phone','0229-50-66-90','','','','<p>0229 - 50 66 90</p>\r\n',NULL,NULL),(4,2,'en','0229 - 50 66 90','0229-50-66-90-1','','','','<p>0229 - 50 66 90</p>\r\n',NULL,NULL),(5,3,'nl','Email','mailto:info@optimamakelaardij.nl','','','','<p>info@optimamakelaardij.nl</p>\r\n',NULL,NULL),(6,3,'en','info@optimamakelaardij.nl','infoatoptimamakelaardij-nl-1','','','','<p>mailto:info@optimamakelaardij.nl</p>\r\n',NULL,NULL),(7,4,'nl','Funda','funda','','','','<p>90</p>\r\n',NULL,NULL),(8,4,'en','Funda','funda-1','','','','<p>90</p>\r\n',NULL,NULL),(9,5,'nl','Contact ons!','contact','','','','Heeft u vragen over een <span>object</span> of wilt u een <span>bezichtiging plannen?</span> ',NULL,NULL),(10,5,'en','Contact ons!','contact-ons-1','','','','<p>Contact ons!</p>\r\n',NULL,NULL),(11,6,'nl','Aanbod Link 1','aanbod-link-1','','','','<p>Aanbod Link 1</p>\r\n',NULL,NULL),(12,6,'en','Aanbod Link 1','aanbod-link-1-1','','','','<p>Aanbod Link 1</p>\r\n',NULL,NULL),(13,7,'nl','Aanbod Link 2','aanbod-link-2','','','','<p>Aanbod Link 2</p>\r\n',NULL,NULL),(14,7,'en','Aanbod Link 2','aanbod-link-2-1','','','','<p>Aanbod Link 2</p>\r\n',NULL,NULL),(15,8,'nl','Aanbod Link 3','aanbod-link-3','','','','<p>Aanbod Link 3</p>\r\n',NULL,NULL),(16,8,'en','Aanbod Link 3','aanbod-link-3-1','','','','<p>Aanbod Link 3</p>\r\n',NULL,NULL),(17,9,'nl','Advies Link 1','advies-link-1','','','','<p>Advies Link 1</p>\r\n',NULL,NULL),(18,9,'en','Advies Link 1','advies-link-1-1','','','','<p>Advies Link 1</p>\r\n',NULL,NULL),(19,10,'nl','Advies Link 2','advies-link-2','','','','<p>Advies Link 2</p>\r\n',NULL,NULL),(20,10,'en','Advies Link 2','advies-link-2-1','','','','<p>Advies Link 2</p>\r\n',NULL,NULL),(21,11,'nl','Advies Link 3','advies-link-3','','','','<p>Advies Link 3</p>\r\n',NULL,NULL),(22,11,'en','Advies Link 3','advies-link-3-1','','','','<p>Advies Link 3</p>\r\n',NULL,NULL),(23,12,'nl','footer optimamakelaardij','footer-optimamakelaardij','','','','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>\r\n\r\n<p>Quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>\r\n\r\n<p><b>KVK 53753917</b><br />\r\n<b>Rabobank</b> 1638.52.596</p>\r\n',NULL,NULL),(24,12,'en','footer optimamakelaardij','footer-optimamakelaardij-1','','','','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>\r\n\r\n<p>Quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>\r\n\r\n<p><strong>KVK 53753917</strong><br />\r\n<strong>Rabobank</strong> 1638.52.596</p>\r\n',NULL,NULL),(25,13,'nl','Bekijk ons gehele aanbod ','aanbod','','','','<p>aanbod</p>\r\n',NULL,NULL),(26,13,'en','Bekijk ons gehele aanbod ','bekijk-ons-gehele-aanbod-1','','','','<p>aanbod</p>\r\n',NULL,NULL),(27,14,'nl','Contactgegevens footer','contactgegevens-footer','','','','<p>Optima Makelaardij<br />\r\npEssinglaan 4<br />\r\n1696 CA Oosterblokker</p>\r\n\r\n<p><strong>Tel:</strong> 0229 50 66 90<br />\r\n<strong>Mail:</strong> info@optimamakelaardij.nl</p>\r\n',NULL,NULL),(28,14,'en','Contactgegevens footer','contactgegevens-footer-1','','','','<p>Optima Makelaardij<br />\r\npEssinglaan 4<br />\r\n1696 CA Oosterblokker</p>\r\n\r\n<p><strong>Tel:</strong> 0229 50 66 90<br />\r\n<strong>Mail:</strong> info@optimamakelaardij.nl</p>\r\n',NULL,NULL),(29,15,'nl','Contactgegevens Facebook','contactgegevens-facebook','','','','<p>Optimamakelaardij</p>\r\n',NULL,NULL),(30,15,'en','Contactgegevens Facebook','contactgegevens-facebook-1','','','','<p>Optimamakelaardij</p>\r\n',NULL,NULL),(31,16,'nl','Logo Footer 1','http://optima.ceesenco.net','','','','<p>Logo Footer 1</p>\r\n',NULL,NULL),(32,16,'en','Logo Footer 1','logo-footer-1-1','','','','<p>Logo Footer 1</p>\r\n',NULL,NULL),(33,17,'nl','Logo Footer 2','http://optima.ceesenco.net','','','','<p>Logo Footer 2</p>\r\n',NULL,NULL),(34,17,'en','Logo Footer 2','logo-footer-2-1','','','','<p>Logo Footer 2</p>\r\n',NULL,NULL),(35,18,'nl','Logo Footer 3','http://optima.ceesenco.net','','','','<p>Logo Footer 3</p>\r\n',NULL,NULL),(36,18,'en','Logo Footer 3','logo-footer-3-1','','','','<p>Logo Footer 3</p>\r\n',NULL,NULL),(37,19,'nl','Logo Footer 4','logo-footer-4','','','','<p>Logo Footer 4</p>\r\n',NULL,NULL),(38,19,'en','Logo Footer 4','logo-footer-4-1','','','','<p>Logo Footer 4</p>\r\n',NULL,NULL),(39,20,'nl','Logo Footer 5','http://optima.ceesenco.net','','','','<p>Logo Footer 5</p>\r\n',NULL,NULL),(40,20,'en','Logo Footer 5','logo-footer-5-1','','','','<p>Logo Footer 5</p>\r\n',NULL,NULL);

/*Table structure for table `blocks` */

DROP TABLE IF EXISTS `blocks`;

CREATE TABLE `blocks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sequence` int(11) NOT NULL,
  `active` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `blocks_active_index` (`active`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `blocks` */

insert  into `blocks`(`id`,`sequence`,`active`,`created_at`,`updated_at`) values (1,1,1,'2017-03-16 11:04:50','2017-03-16 11:04:50'),(2,2,1,'2017-03-16 11:27:51','2017-03-16 11:27:51'),(3,3,1,'2017-03-16 11:29:27','2017-03-16 11:29:27'),(4,4,1,'2017-03-28 17:48:22','2017-03-28 17:48:34'),(5,5,0,'2017-03-28 18:11:37','2017-03-28 18:11:37'),(6,6,0,'2017-03-28 20:34:50','2017-03-28 20:34:50'),(7,7,0,'2017-03-28 20:35:12','2017-03-28 20:35:12'),(8,8,0,'2017-03-28 20:35:33','2017-03-28 20:35:33'),(9,9,0,'2017-03-28 20:36:13','2017-03-28 20:36:13'),(10,10,0,'2017-03-28 20:36:33','2017-03-28 20:36:33'),(11,11,0,'2017-03-28 20:36:54','2017-03-28 20:36:54'),(12,12,0,'2017-03-28 21:37:28','2017-03-28 21:37:28'),(13,13,0,'2017-03-28 21:40:50','2017-03-28 21:40:50'),(14,14,0,'2017-03-29 23:28:32','2017-03-29 23:28:32'),(15,15,0,'2017-03-29 23:29:20','2017-03-29 23:29:20'),(16,16,0,'2017-03-29 23:31:21','2017-03-29 23:31:21'),(17,17,0,'2017-03-29 23:34:26','2017-03-29 23:34:26'),(18,18,0,'2017-03-29 23:35:00','2017-03-29 23:35:00'),(19,19,0,'2017-03-29 23:35:18','2017-03-29 23:35:18'),(20,20,0,'2017-03-29 23:35:39','2017-03-29 23:35:39');

/*Table structure for table `categories` */

DROP TABLE IF EXISTS `categories`;

CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `sequence` int(11) NOT NULL,
  `active` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `categories_active_index` (`active`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `categories` */

insert  into `categories`(`id`,`parent_id`,`sequence`,`active`,`created_at`,`updated_at`) values (2,NULL,1,0,'2017-03-20 20:42:06','2017-03-20 20:42:06');

/*Table structure for table `category_product` */

DROP TABLE IF EXISTS `category_product`;

CREATE TABLE `category_product` (
  `category_id` int(10) unsigned NOT NULL,
  `product_id` int(10) unsigned NOT NULL,
  `sequence` int(11) NOT NULL,
  PRIMARY KEY (`category_id`,`product_id`),
  KEY `category_product_category_id_index` (`category_id`),
  KEY `category_product_product_id_index` (`product_id`),
  CONSTRAINT `category_product_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE,
  CONSTRAINT `category_product_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `category_product` */

insert  into `category_product`(`category_id`,`product_id`,`sequence`) values (2,20,1),(2,21,2);

/*Table structure for table `category_translations` */

DROP TABLE IF EXISTS `category_translations`;

CREATE TABLE `category_translations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(10) unsigned NOT NULL,
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_keywords` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `category_translations_category_id_locale_unique` (`category_id`,`locale`),
  KEY `category_translations_locale_index` (`locale`),
  KEY `category_translations_slug_index` (`slug`),
  CONSTRAINT `category_translations_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `category_translations` */

insert  into `category_translations`(`id`,`category_id`,`locale`,`title`,`slug`,`meta_title`,`meta_description`,`meta_keywords`,`text`) values (3,2,'nl','Objecten','Objecten','House','House','House','<p>Objecten</p>\r\n'),(4,2,'en','House','house-1','House','House','House','<p>House</p>\r\n');

/*Table structure for table `fundas` */

DROP TABLE IF EXISTS `fundas`;

CREATE TABLE `fundas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `value` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `fundas` */

insert  into `fundas`(`id`,`value`,`created_at`,`updated_at`) values (1,'100','2017-03-13 01:56:05','2017-03-12 20:06:54');

/*Table structure for table `fundass` */

DROP TABLE IF EXISTS `fundass`;

CREATE TABLE `fundass` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `fundass` */

/*Table structure for table `images` */

DROP TABLE IF EXISTS `images`;

CREATE TABLE `images` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `imageable_id` int(11) NOT NULL,
  `imageable_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `size` int(11) NOT NULL,
  `extension` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `original_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sequence` int(11) NOT NULL,
  `active` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `images_active_index` (`active`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `images` */

insert  into `images`(`id`,`imageable_id`,`imageable_type`,`path`,`size`,`extension`,`name`,`description`,`label`,`link`,`original_name`,`sequence`,`active`,`created_at`,`updated_at`) values (1,1,'App\\Models\\Banner','9d7732100f5a975a113dc65d90cd01d3e985ecf3',177294,'jpg','','','','','slider1.jpg',1,1,'2017-03-12 18:39:17','2017-03-12 18:39:17'),(2,2,'App\\Models\\Banner','e5383a9e3ae1cc50927511a241aa3b877bbbb47a',177294,'jpg','','','','','slider1.jpg',2,1,'2017-03-12 18:40:57','2017-03-12 18:40:58'),(3,2,'App\\Models\\Product','3b7f7d180cdbbeecdc8315ff5518f057686dcc5b',27780,'jpg','','','','','thumb1.jpg',3,1,'2017-03-21 23:03:15','2017-03-21 23:03:15'),(4,1,'App\\Models\\Referenties','71d0165d962d769a6fc54b6897f1526967668053',44333,'jpg','','','','','thumb-refrence.jpg',4,1,'2017-03-22 22:05:38','2017-03-22 22:05:38'),(6,20,'App\\Models\\Product','a862b6cae4fac58636b983b1d52206839ce13260',27780,'jpg','','','','','thumb1.jpg',5,1,'2017-03-23 16:33:57','2017-03-23 16:33:57'),(7,21,'App\\Models\\Product','cd8c8e77b9e44a8f702343598ffe940ec4754e74',30207,'jpg','','','','','thumb2.jpg',6,1,'2017-03-23 16:34:55','2017-03-23 16:34:55'),(8,20,'App\\Models\\Product','21e72814c1fbbbe8da62fe1393557ab732f17460',30207,'jpg','','','','','thumb2.jpg',7,1,'2017-03-23 18:38:19','2017-03-23 18:38:20'),(9,20,'App\\Models\\Product','429ed89ab9fcde95c225a6a396b18c9441df9592',308618,'jpg','','','','','slider-object-bottom.jpg',8,1,'2017-03-23 18:38:20','2017-03-23 18:38:20'),(11,27,'App\\Models\\Page','d43080daa28c535990804eb00afd4bd908bdc768',44333,'jpg','','','','','thumb-refrence.jpg',9,1,'2017-03-28 19:41:29','2017-03-28 19:41:29'),(12,16,'App\\Models\\Block','c99d030ed42fa764f37f4f60b49632ec518e2b8f',6433,'png','','','','','logo-funda.png',10,1,'2017-03-29 23:32:34','2017-03-29 23:32:34'),(13,17,'App\\Models\\Block','de28dbe4964defebab4df58fe7c4829245130d80',6274,'png','','','','','logo-nww.png',11,1,'2017-03-29 23:36:28','2017-03-29 23:36:29'),(14,18,'App\\Models\\Block','36bac98a3b1628135b5788b4d2bae01c4de209af',4990,'png','','','','','logo-r.png',12,1,'2017-03-29 23:37:08','2017-03-29 23:37:08'),(15,20,'App\\Models\\Block','f0348e806891530173f270611a411413640b158c',3227,'jpg','','','','','logo-vastgood.jpg',13,1,'2017-03-29 23:38:54','2017-03-29 23:38:55');

/*Table structure for table `language_translations` */

DROP TABLE IF EXISTS `language_translations`;

CREATE TABLE `language_translations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `language_id` int(10) unsigned NOT NULL,
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `language_translations_language_id_locale_unique` (`language_id`,`locale`),
  KEY `language_translations_locale_index` (`locale`),
  CONSTRAINT `language_translations_language_id_foreign` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `language_translations` */

insert  into `language_translations`(`id`,`language_id`,`locale`,`name`) values (1,1,'nl','Nederlands'),(2,1,'en','Dutch'),(3,1,'fr','Néerlandais'),(4,1,'de','Holländisch'),(5,1,'es','Holandés'),(6,2,'nl','Engels'),(7,2,'en','English'),(8,2,'fr','Anglais'),(9,2,'de','Englisch'),(10,2,'es','Inglés'),(11,3,'nl','Frans'),(12,3,'en','French'),(13,3,'fr','Français'),(14,3,'de','Französisch'),(15,3,'es','Francés'),(16,4,'nl','Duits'),(17,4,'en','German'),(18,4,'fr','Allemand'),(19,4,'de','Deutsch'),(20,4,'es','Alemán'),(21,5,'nl','Spaans'),(22,5,'en','Spanish'),(23,5,'fr','Espagnol'),(24,5,'de','Spanisch'),(25,5,'es','Español');

/*Table structure for table `languages` */

DROP TABLE IF EXISTS `languages`;

CREATE TABLE `languages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `flag` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sequence` int(11) NOT NULL,
  `active` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `languages_code_unique` (`code`),
  KEY `languages_active_index` (`active`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `languages` */

insert  into `languages`(`id`,`code`,`flag`,`sequence`,`active`,`created_at`,`updated_at`,`deleted_at`) values (1,'nl','nl',1,0,'2016-03-01 15:27:07','2017-03-03 12:44:44',NULL),(2,'en','gb',2,1,'2016-03-01 15:27:07','2017-03-03 12:44:38',NULL),(3,'fr','fr',3,0,'2016-03-01 15:27:07','2016-03-01 15:27:07',NULL),(4,'de','de',4,0,'2016-03-01 15:27:07','2016-03-01 15:27:07',NULL),(5,'es','es',5,0,'2016-03-01 15:27:07','2016-03-01 15:27:07',NULL);

/*Table structure for table `migrations` */

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `migrations` */

insert  into `migrations`(`migration`,`batch`) values ('2014_10_12_000000_create_users_table',1),('2014_10_12_100000_create_password_resets_table',1),('2015_12_23_082721_entrust_setup_tables',1),('2015_12_23_144256_create_modules_table',1),('2015_12_30_083328_create_languages_table',1),('2016_01_15_113228_create_sessions_table',1),('2016_01_15_115055_create_pages_table',1),('2016_01_15_143558_create_images_table',1),('2016_01_22_120310_create_blocks_table',1),('2016_01_23_095415_create_categories_table',1),('2016_01_23_113452_create_attributes_table',1),('2016_01_23_131030_create_attribute_sets_table',1),('2016_01_26_143436_create_attribute_set_attribute_pivot_table',1),('2016_01_26_160205_create_products_table',1),('2016_01_26_160756_create_category_product_pivot_table',1),('2016_01_29_121612_create_attribute_product_pivot_table',1),('2016_01_30_100023_create_news_table',1),('2016_01_30_110014_create_banners_table',1),('2016_01_30_120945_create_settings_table',1),('2016_03_04_111204_create_activity_log_table',1),('2017_03_12_193514_create_funda_table',1),('2017_03_12_194455_create_fundas_table',2),('2017_03_22_202552_create_referenties_table',3),('2017_03_22_204551_Refrentions',4);

/*Table structure for table `modules` */

DROP TABLE IF EXISTS `modules`;

CREATE TABLE `modules` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `config` text COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(4) NOT NULL,
  `sequence` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `modules_name_index` (`name`),
  KEY `modules_active_index` (`active`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `modules` */

insert  into `modules`(`id`,`name`,`display_name`,`config`,`active`,`sequence`,`created_at`,`updated_at`) values (1,'pages','Hoofdpagina\'s','{\"active\":1,\"settings\":{\"images\":{\"max_size\":10000,\"max_images\":1,\"sizes\":{\"thumb\":{\"width\":64,\"height\":64},\"small\":{\"width\":200,\"height\":200},\"medium\":{\"width\":450,\"height\":450},\"big\":{\"width\":1000,\"height\":1000}}}}}',1,1,'2016-03-01 15:27:07','2017-03-23 09:55:11'),(2,'subpages','Subpagina\'s','{\"active\":1,\"button\":1,\"settings\":{\"images\":{\"max_size\":10000,\"max_images\":1,\"sizes\":{\"thumb\":{\"width\":64,\"height\":64},\"small\":{\"width\":200,\"height\":200},\"medium\":{\"width\":450,\"height\":450},\"big\":{\"width\":1000,\"height\":1000}}}}}',1,2,'2016-03-01 15:27:07','2017-03-23 09:49:25'),(3,'blocks','Statische blokken','{\"active\":1,\"settings\":{\"images\":{\"max_size\":10000,\"max_images\":1,\"sizes\":{\"thumb\":{\"width\":64,\"height\":64},\"small\":{\"width\":200,\"height\":200},\"medium\":{\"width\":450,\"height\":450},\"big\":{\"width\":1000,\"height\":1000}}}}}',1,3,'2016-03-01 15:27:07','2017-03-16 11:04:05'),(4,'banners','Beheer banners','{\"active\":1,\"settings\":{\"images\":{\"max_size\":10000,\"max_images\":1,\"sizes\":{\"thumb\":{\"width\":64,\"height\":64},\"small\":{\"width\":200,\"height\":200},\"medium\":{\"width\":450,\"height\":450},\"big\":{\"width\":1000,\"height\":1000},\"banner\":{\"width\":1600,\"height\":1000}}}}}',1,4,'2016-03-01 15:27:07','2017-03-12 18:33:36'),(5,'categories','Product categorieen','{\"active\":0,\"settings\":{\"images\":{\"max_size\":10000,\"max_images\":1,\"sizes\":{\"thumb\":{\"width\":64,\"height\":64},\"small\":{\"width\":200,\"height\":200},\"medium\":{\"width\":450,\"height\":450},\"big\":{\"width\":1000,\"height\":1000}}}}}',1,5,'2016-03-01 15:27:07','2017-03-20 20:39:06'),(6,'products','Producten','{\"active\":1,\"settings\":{\"images\":{\"max_size\":10000,\"max_images\":10,\"sizes\":{\"thumb\":{\"width\":64,\"height\":64},\"small\":{\"width\":200,\"height\":200},\"medium\":{\"width\":450,\"height\":450},\"big\":{\"width\":1000,\"height\":1000}}}}}',1,6,'2016-03-01 15:27:07','2017-03-19 17:43:37'),(7,'projectgroups','Projectgroepen','{\"active\":1,\"settings\":{\"images\":{\"max_size\":10000,\"max_images\":1,\"sizes\":{\"thumb\":{\"width\":64,\"height\":64},\"small\":{\"width\":200,\"height\":200},\"medium\":{\"width\":450,\"height\":450},\"big\":{\"width\":1000,\"height\":1000}}}}}',0,7,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(8,'projects','Projecten','{\"active\":1,\"settings\":{\"images\":{\"max_size\":10000,\"max_images\":1,\"sizes\":{\"thumb\":{\"width\":64,\"height\":64},\"small\":{\"width\":200,\"height\":200},\"medium\":{\"width\":450,\"height\":450},\"big\":{\"width\":1000,\"height\":1000}}}}}',0,8,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(9,'news','Nieuws','{\"active\":1,\"settings\":{\"images\":{\"max_size\":10000,\"max_images\":1,\"sizes\":{\"thumb\":{\"width\":64,\"height\":64},\"small\":{\"width\":200,\"height\":200},\"medium\":{\"width\":450,\"height\":450},\"big\":{\"width\":1000,\"height\":1000}}}}}',0,9,'2016-03-01 15:27:07','2017-03-21 22:53:24'),(12,'referenties','referenties','{\"active\":1,\"settings\":{\"images\":{\"max_size\":10000,\"max_images\":1,\"sizes\":{\"thumb\":{\"width\":64,\"height\":64},\"small\":{\"width\":200,\"height\":200},\"medium\":{\"width\":450,\"height\":450},\"big\":{\"width\":1000,\"height\":1000}}}}}',1,12,'2017-03-23 04:01:05','0000-00-00 00:00:00');

/*Table structure for table `news` */

DROP TABLE IF EXISTS `news`;

CREATE TABLE `news` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sequence` int(11) NOT NULL,
  `active` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `news_active_index` (`active`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `news` */

/*Table structure for table `news_translations` */

DROP TABLE IF EXISTS `news_translations`;

CREATE TABLE `news_translations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `news_id` int(10) unsigned NOT NULL,
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `posted_on` datetime NOT NULL,
  `meta_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_keywords` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text` text COLLATE utf8_unicode_ci NOT NULL,
  `intro` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `news_translations_news_id_locale_unique` (`news_id`,`locale`),
  KEY `news_translations_locale_index` (`locale`),
  KEY `news_translations_slug_index` (`slug`),
  CONSTRAINT `news_translations_news_id_foreign` FOREIGN KEY (`news_id`) REFERENCES `news` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `news_translations` */

/*Table structure for table `page_translations` */

DROP TABLE IF EXISTS `page_translations`;

CREATE TABLE `page_translations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `page_id` int(10) unsigned NOT NULL,
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_keywords` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `page_translations_page_id_locale_unique` (`page_id`,`locale`),
  KEY `page_translations_locale_index` (`locale`),
  KEY `page_translations_slug_index` (`slug`),
  CONSTRAINT `page_translations_page_id_foreign` FOREIGN KEY (`page_id`) REFERENCES `pages` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `page_translations` */

insert  into `page_translations`(`id`,`page_id`,`locale`,`title`,`slug`,`meta_title`,`meta_description`,`meta_keywords`,`text`) values (5,3,'nl','Home','home','home','home','home','<p>home</p>\r\n'),(6,3,'en','Home','home-1','home','home','home','<p>home</p>\r\n'),(9,5,'nl','VERKOOP','verkoop','fa-map-signs','fa-map-signs','fa-map-signs','<p>Lorem ipsum dolor sit amet, cons ectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.</p>\r\n\r\n<p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>\r\n'),(10,5,'en','VERKOOP','verkoop-1','VERKOOP','VERKOOP','VERKOOP','<p>Lorem ipsum dolor sit amet, cons ectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.</p>\r\n\r\n<p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>\r\n'),(11,6,'nl','AANKOOP','aankoop','fa-key','fa-key','fa-key','<p>Lorem ipsum dolor sit amet, cons ectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.</p>\r\n\r\n<p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>\r\n'),(12,6,'en','AANKOOP','aankoop-1','AANKOOP','AANKOOP','AANKOOP','<p>Lorem ipsum dolor sit amet, cons ectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.</p>\r\n\r\n<p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>\r\n'),(13,7,'nl','TAXATIES','taxaties','fa-files-o','fa-files-o','fa-files-o','<p>Lorem ipsum dolor sit amet, cons ectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.</p>\r\n\r\n<p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>\r\n'),(14,7,'en','TAXATIES','taxaties-1','TAXATIES','TAXATIES','TAXATIES','<p>Lorem ipsum dolor sit amet, cons ectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.</p>\r\n\r\n<p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>\r\n'),(15,8,'nl','Over Optima','over-optima','Over Optima','Over Optima','Over Optima','<p>Over Optima</p>\r\n'),(16,8,'en','Over Optima','over-optima-1','Over Optima','Over Optima','Over Optima','<p>Over Optima</p>\r\n'),(17,9,'nl','Goede prijs/kwaliteit verhouding','goede-prijs-kwaliteit-verhouding','fa-eur','fa-eur','fa-eur','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>\r\n'),(18,9,'en','Goede prijs/kwaliteit verhouding','goede-prijs-kwaliteit-verhouding-1','Goede prijs/kwaliteit verhouding','Goede prijs/kwaliteit verhouding','Goede prijs/kwaliteit verhouding','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>\r\n'),(19,10,'nl','Goede positionering in de woningmarkt','goede-positionering-in-de-woningmarkt','fa-line-chart','fa-line-chart','fa-line-chart','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>\r\n'),(20,10,'en','Goede positionering in de woningmarkt','goede-positionering-in-de-woningmarkt-1','Goede positionering in de woningmarkt','Goede positionering in de woningmarkt','Goede positionering in de woningmarkt','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>\r\n'),(21,11,'nl','Persoonlijke aandacht','persoonlijke-aandacht','fa-comments','fa-comments','fa-comments','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>\r\n'),(22,11,'en','Persoonlijke aandacht','persoonlijke-aandacht-1','Persoonlijke aandacht','Persoonlijke aandacht','Persoonlijke aandacht','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>\r\n'),(23,12,'nl','Dynamisch en flexibel','dynamisch-en-flexibel','fa-exchange','fa-exchange','fa-exchange','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat</p>\r\n'),(24,12,'en','Dynamisch en flexibel','dynamisch-en-flexibel-1','Dynamisch en flexibel','Dynamisch en flexibel','Dynamisch en flexibel','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat</p>\r\n'),(25,13,'nl','Aanbod','aanbod','aanbod','aanbod','aanbod','<p>aanbod</p>\r\n'),(26,13,'en','Aanbod','aanbod-1','aanbod','aanbod','aanbod','<p>aanbod</p>\r\n'),(27,14,'nl','Diensten','diensten','diensten','diensten','diensten','<p>Diensten</p>\r\n'),(28,14,'en','Diensten','diensten-1','diensten','diensten','diensten','<p>Diensten</p>\r\n'),(29,15,'nl','Taxaties','taxaties-2','fa-files-o','fa-files-o','fa-files-o','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>\r\n'),(30,15,'en','Taxaties','taxaties-3','Taxaties','Taxaties','Taxaties','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>\r\n'),(31,16,'nl','Aankoopbemiddeling','aankoopbemiddeling','handshake','handshake','handshake','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>\r\n'),(32,16,'en','Aankoopbemiddeling','aankoopbemiddeling-1','Aankoopbemiddeling','Aankoopbemiddeling','Aankoopbemiddeling','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>\r\n'),(33,17,'nl','Verkoop van uw woning','verkoop-van-uw-woning','fa-home','fa-home','fa-home','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>\r\n'),(34,17,'en','Verkoop van uw woning','verkoop-van-uw-woning-1','Verkoop van uw woning','Verkoop van uw woning','Verkoop van uw woning','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>\r\n'),(35,18,'nl','Gratis waardebepaling','gratis-waardebepaling','fa-bar-chart','fa-bar-chart','fa-bar-chart','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>\r\n'),(36,18,'en','Gratis waardebepaling','gratis-waardebepaling-1','Gratis waardebepaling','Gratis waardebepaling','Gratis waardebepaling','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>\r\n'),(37,19,'nl','Contact','contact','contact','contact','contact','<p>contact</p>\r\n'),(38,19,'en','contact','contact-1','contact','contact','contact','<p>contact</p>\r\n'),(39,20,'nl','Contactformulier','contactformulier','Contactformulier','Contactformulier','Contactformulier','<p>Lorem ipsum dolor sit amet, cons ectetur adipiscing elit, sed tempor incididunt ut labore et dolore</p>\r\n'),(40,20,'en','Contactformulier','contactformulier-1','Contactformulier','Contactformulier','Contactformulier','<p>Lorem ipsum dolor sit amet, cons ectetur adipiscing elit, sed tempor incididunt ut labore et dolore</p>\r\n'),(41,21,'nl','Contactgegevens','contactgegevens','Contactgegevens','Contactgegevens','Contactgegevens','<p>Lorem ipsum dolor sit amet, labore et dolore cons ectetur adipiscing elit, sed do tempor incididunt ut .</p>\r\n'),(42,21,'en','Contactgegevens','contactgegevens-1','Contactgegevens','Contactgegevens','Contactgegevens','<p>Lorem ipsum dolor sit amet, labore et dolore cons ectetur adipiscing elit, sed do tempor incididunt ut .</p>\r\n'),(43,22,'nl','Optima Makelaardij','optima-makelaardij','Optima Makelaardij','Optima Makelaardij','Optima Makelaardij','<p>Oosterblokker 2A</p>\r\n\r\n<p>1696 BH Oosterblokker</p>\r\n\r\n<p>Tel: 0229 50 66 90<br />\r\nMail: info@optimamakelaardij.nl</p>\r\n\r\n<p>&nbsp;</p>\r\n'),(44,22,'en','Optima Makelaardij','optima-makelaardij-1','Optima Makelaardij','Optima Makelaardij','Optima Makelaardij','<p>Oosterblokker 2A 1696 BH Oosterblokker</p>\r\n'),(47,24,'nl','Facebook','facebook','Optimamakelaardij','Optimamakelaardij','Optimamakelaardij','<p><a href=\"http://localhost/optimamakelaardij/public/contact#\" target=\"_blank\">Optimamakelaardij</a></p>\r\n'),(48,24,'en','Facebook','facebook-1','Optimamakelaardij','Optimamakelaardij','Optimamakelaardij','<p><a href=\"http://localhost/optimamakelaardij/public/contact#\" target=\"_blank\">Optimamakelaardij</a></p>\r\n'),(49,25,'nl','Instagram','instagram','Optimamakelaardij','Optimamakelaardij','Optimamakelaardij','<p><a href=\"http://localhost/optimamakelaardij/public/contact#\" target=\"_blank\">Optimamakelaardij</a></p>\r\n'),(50,25,'en','Instagram','instagram-1','Optimamakelaardij','Optimamakelaardij','Optimamakelaardij','<p><a href=\"http://localhost/optimamakelaardij/public/contact#\" target=\"_blank\">Optimamakelaardij</a></p>\r\n'),(51,26,'nl','Referenties','referenties','Referenties','Referenties','Referenties','<p>Referenties</p>\r\n'),(52,26,'en','Referenties','referenties-1','Referenties','Referenties','Referenties','<p>Referenties</p>\r\n'),(53,27,'nl','Botter 102, Hoorn','botter-102-hoorn','3','3','3','<p>&ldquo;Rick is een zeer vakbekwame makelaar en Laura een goede fotografie. Ze nemen de tijd voor je. Ruime werktijden wat het communiceren makkelijker maakt, zeker als jezelf ook werkt overdag. Dank jullie wel voor de snelle verkoop en afhandeling.&rdquo;</p>\r\n'),(54,27,'en','Botter 102, Hoorn','botter-102-hoorn-1','3','3','3','<p>&ldquo;Rick is een zeer vakbekwame makelaar en Laura een goede fotografie. Ze nemen de tijd voor je. Ruime werktijden wat het communiceren makkelijker maakt, zeker als jezelf ook werkt overdag. Dank jullie wel voor de snelle verkoop en afhandeling.&rdquo;</p>\r\n'),(55,28,'nl','Gemiddelde beoordeling op','gemiddelde-beoordeling-op','funda','funda','funda','<p>9.5</p>\r\n'),(56,28,'en','Gemiddelde beoordeling op','gemiddelde-beoordeling-op-1','funda','funda','funda','<p>9.5</p>\r\n');

/*Table structure for table `pages` */

DROP TABLE IF EXISTS `pages`;

CREATE TABLE `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `sequence` int(11) NOT NULL,
  `active` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `pages_active_index` (`active`),
  KEY `pages_parent_id_foreign` (`parent_id`),
  CONSTRAINT `pages_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `pages` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `pages` */

insert  into `pages`(`id`,`parent_id`,`sequence`,`active`,`created_at`,`updated_at`) values (3,NULL,1,0,'2017-03-28 17:50:23','2017-03-28 17:50:23'),(5,3,1,1,'2017-03-28 17:56:57','2017-03-28 17:58:41'),(6,3,2,1,'2017-03-28 17:57:52','2017-03-28 17:58:42'),(7,3,3,1,'2017-03-28 17:58:28','2017-03-28 17:58:43'),(8,NULL,2,0,'2017-03-28 18:00:02','2017-03-28 18:00:02'),(9,8,1,1,'2017-03-28 18:03:15','2017-03-28 18:26:56'),(10,8,2,1,'2017-03-28 18:03:54','2017-03-28 18:26:57'),(11,8,3,1,'2017-03-28 18:04:32','2017-03-28 18:26:58'),(12,8,4,1,'2017-03-28 18:05:07','2017-03-28 18:26:59'),(13,NULL,3,0,'2017-03-28 18:05:55','2017-03-28 18:05:55'),(14,NULL,4,0,'2017-03-28 18:06:47','2017-03-28 18:06:47'),(15,14,1,1,'2017-03-28 18:07:31','2017-03-28 19:16:27'),(16,14,2,1,'2017-03-28 18:08:12','2017-03-28 19:16:28'),(17,14,3,1,'2017-03-28 18:08:48','2017-03-28 19:16:29'),(18,14,4,1,'2017-03-28 18:09:24','2017-03-28 19:16:30'),(19,NULL,5,0,'2017-03-28 18:10:04','2017-03-28 18:10:04'),(20,19,1,0,'2017-03-28 18:13:40','2017-03-28 18:13:41'),(21,19,2,0,'2017-03-28 18:14:27','2017-03-28 18:14:28'),(22,19,3,0,'2017-03-28 18:15:21','2017-03-28 18:15:22'),(24,19,4,0,'2017-03-28 18:20:22','2017-03-28 18:20:22'),(25,19,5,0,'2017-03-28 18:21:09','2017-03-28 18:21:10'),(26,3,4,0,'2017-03-28 19:37:43','2017-03-28 19:37:44'),(27,26,1,1,'2017-03-28 19:39:32','2017-03-28 19:39:52'),(28,3,5,0,'2017-03-28 19:52:42','2017-03-28 19:52:43');

/*Table structure for table `password_resets` */

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `password_resets` */

/*Table structure for table `permission_role` */

DROP TABLE IF EXISTS `permission_role`;

CREATE TABLE `permission_role` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `permission_role_role_id_foreign` (`role_id`),
  CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `permission_role` */

insert  into `permission_role`(`permission_id`,`role_id`) values (22,1),(24,1),(59,1),(64,2),(65,2),(66,2),(59,3);

/*Table structure for table `permissions` */

DROP TABLE IF EXISTS `permissions`;

CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sequence` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `permissions_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `permissions` */

insert  into `permissions`(`id`,`name`,`display_name`,`description`,`sequence`,`created_at`,`updated_at`) values (1,'access-permission','Rechten beheren','',0,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(2,'create-permission','Rechten toevoegen','',1,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(3,'edit-permission','Rechten bewerken','',2,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(4,'delete-permission','Rechten verwijderen','',3,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(5,'access-role','Rollen beheren','',4,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(6,'create-role','Rollen toevoegen','',5,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(7,'edit-role','Rollen bewerken','',6,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(8,'delete-role','Rollen verwijderen','',7,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(9,'access-user','Gebruikers beheren','',8,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(10,'create-user','Gebruikers toevoegen','',9,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(11,'edit-user','Gebruikers bewerken','',10,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(12,'delete-user','Gebruikers verwijderen','',11,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(13,'access-page','Pagina\'s beheren','',12,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(14,'create-page','Pagina\'s toevoegen','',13,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(15,'edit-page','Pagina\'s bewerken','',14,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(16,'delete-page','Pagina\'s verwijderen','',15,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(17,'access-subpage','Subpagina\'s beheren','',16,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(18,'create-subpage','Subpagina\'s toevoegen','',17,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(19,'edit-subpage','Subpagina\'s bewerken','',18,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(20,'delete-subpage','Subpagina\'s verwijderen','',19,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(21,'access-block','Statische blokken beheren','',20,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(22,'create-block','Statische blokken toevoegen','',21,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(23,'edit-block','Statische blokken bewerken','',22,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(24,'delete-block','Statische blokken verwijderen','',23,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(25,'access-banner','Banners beheren','',24,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(26,'create-banner','Banners toevoegen','',25,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(27,'edit-banner','Banners bewerken','',26,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(28,'delete-banner','Banners verwijderen','',27,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(29,'access-category','Product categorieen beheren','',28,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(30,'create-category','Product categorieen toevoegen','',29,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(31,'edit-category','Product categorieen bewerken','',30,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(32,'delete-category','Product categorieen verwijderen','',31,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(33,'access-product','Producten beheren','',32,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(34,'create-product','Producten toevoegen','',33,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(35,'edit-product','Producten bewerken','',34,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(36,'delete-product','Producten verwijderen','',35,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(37,'access-attribute','Attributen beheren','',36,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(38,'create-attribute','Attribuut toevoegen','',37,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(39,'edit-attribute','Attribuut bewerken','',38,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(40,'delete-attribute','Attributen verwijderen','',39,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(41,'access-attributeset','Attributen set beheren','',40,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(42,'create-attributeset','Attributen set toevoegen','',41,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(43,'edit-attributeset','Attributen set bewerken','',42,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(44,'delete-attributeset','Attributen set verwijderen','',43,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(45,'access-projectgroup','Projectgroepen beheren','',44,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(46,'create-projectgroup','Projectgroepen toevoegen','',45,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(47,'edit-projectgroup','Projectgroepen bewerken','',46,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(48,'delete-projectgroup','Projectgroepen verwijderen','',47,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(49,'access-project','Projecten beheren','',48,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(50,'create-project','Projecten toevoegen','',49,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(51,'edit-project','Projecten bewerken','',50,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(52,'delete-project','Projecten verwijderen','',51,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(53,'access-news','Nieuws beheren','',52,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(54,'create-news','Nieuws toevoegen','',53,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(55,'edit-news','Nieuws bewerken','',54,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(56,'delete-news','Nieuws verwijderen','',55,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(57,'access-setting','Instellingen beheren','',56,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(58,'edit-setting','Instellingen bewerken','',57,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(59,'reset-admin-password','Herstel admin wachtwoord ','',58,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(60,'access-topmenu','topmenu beheren',' -',0,'2017-03-12 19:19:31','2017-03-12 19:19:31'),(61,'edit-topmenu','Topmenu bewerken','-',0,'2017-03-12 19:20:15','2017-03-12 19:20:15'),(62,'access-funda','funda beheren','-',0,'2017-03-12 20:07:54','2017-03-12 20:07:54'),(63,'edit-funda','Funda bewerken','-',0,'2017-03-12 20:08:31','2017-03-12 20:08:31'),(64,'create-referenties','referenties   toevoegen',' referenties',0,'2017-03-23 06:07:23','2017-03-23 06:07:23'),(65,'edit-referenties','referenties bewerken','referenties ',0,'2017-03-23 06:08:14','2017-03-23 06:08:14'),(66,'delete-referenties ','referenties verwijderen','referenties ',0,'2017-03-23 06:09:11','2017-03-23 06:09:11');

/*Table structure for table `product_translations` */

DROP TABLE IF EXISTS `product_translations`;

CREATE TABLE `product_translations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(10) unsigned NOT NULL,
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `intro` text COLLATE utf8_unicode_ci NOT NULL,
  `text` text COLLATE utf8_unicode_ci NOT NULL,
  `meta_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_keywords` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `product_translations_product_id_locale_unique` (`product_id`,`locale`),
  KEY `product_translations_locale_index` (`locale`),
  KEY `product_translations_slug_index` (`slug`),
  CONSTRAINT `product_translations_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `product_translations` */

insert  into `product_translations`(`id`,`product_id`,`locale`,`slug`,`name`,`intro`,`text`,`meta_title`,`meta_description`,`meta_keywords`) values (36,20,'nl','veendijk-3-zwaag','Veendijk 3, Zwaag','<p>Zeer royale vrijstaande woning gebouwd in 2009 met maar liefst 230m2 aan woonoppervlak (exclusief garage). Het geheel is gelegen op een zonnige kavel met een fijne tuin op het Zuidwesten.</p>\r\n','<p>Zeer royale vrijstaande woning gebouwd in 2009 met maar liefst 230m2 aan woonoppervlak (exclusief garage). Het geheel is gelegen op een zonnige kavel met een fijne tuin op het Zuidwesten.</p>\r\n','Veendijk','Veendijk','Veendijk'),(37,20,'en','veendijk-3-zwaag-1','Veendijk 3, Zwaag','<p>Zeer royale vrijstaande woning gebouwd in 2009 met maar liefst 230m2 aan woonoppervlak (exclusief garage). Het geheel is gelegen op een zonnige kavel met een fijne tuin op het Zuidwesten.</p>\r\n','<p>Zeer royale vrijstaande woning gebouwd in 2009 met maar liefst 230m2 aan woonoppervlak (exclusief garage). Het geheel is gelegen op een zonnige kavel met een fijne tuin op het Zuidwesten.</p>\r\n','Veendijk','Veendijk','Veendijk'),(38,21,'nl','veendijk-3-zwaag2','Veendijk 3, Zwaag2','<p>Zeer royale vrijstaande woning gebouwd in 2009 met maar liefst 230m2 aan woonoppervlak (exclusief garage). Het geheel is gelegen op een zonnige kavel met een fijne tuin op het Zuidwesten.</p>\r\n','<p>Zeer royale vrijstaande woning gebouwd in 2009 met maar liefst 230m2 aan woonoppervlak (exclusief garage). Het geheel is gelegen op een zonnige kavel met een fijne tuin op het Zuidwesten.</p>\r\n','Zwaag','Zwaag','Zwaag'),(39,21,'en','veendijk-3-zwaag2-1','Veendijk 3, Zwaag2','<p>Zeer royale vrijstaande woning gebouwd in 2009 met maar liefst 230m2 aan woonoppervlak (exclusief garage). Het geheel is gelegen op een zonnige kavel met een fijne tuin op het Zuidwesten.</p>\r\n','<p>Zeer royale vrijstaande woning gebouwd in 2009 met maar liefst 230m2 aan woonoppervlak (exclusief garage). Het geheel is gelegen op een zonnige kavel met een fijne tuin op het Zuidwesten.</p>\r\n','Zwaag','Zwaag','Zwaag');

/*Table structure for table `products` */

DROP TABLE IF EXISTS `products`;

CREATE TABLE `products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `attribute_set_id` int(10) unsigned NOT NULL,
  `price` double NOT NULL,
  `active` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `products_attribute_set_id_foreign` (`attribute_set_id`),
  KEY `products_active_index` (`active`),
  CONSTRAINT `products_attribute_set_id_foreign` FOREIGN KEY (`attribute_set_id`) REFERENCES `attribute_sets` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `products` */

insert  into `products`(`id`,`attribute_set_id`,`price`,`active`,`created_at`,`updated_at`) values (20,3,540,1,'2017-03-23 16:18:24','2017-03-23 16:19:38'),(21,3,20000,1,'2017-03-23 16:19:29','2017-03-23 16:19:39');

/*Table structure for table `referenties` */

DROP TABLE IF EXISTS `referenties`;

CREATE TABLE `referenties` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `sequence` int(11) NOT NULL,
  `star` int(10) DEFAULT NULL,
  `active` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `referenties_title_index` (`title`),
  KEY `referenties_active_index` (`active`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `referenties` */

insert  into `referenties`(`id`,`title`,`description`,`sequence`,`star`,`active`,`created_at`,`updated_at`) values (1,'coba','<p>coba</p>\r\n',1,5,1,'2017-03-22 21:28:43','2017-03-23 06:18:50');

/*Table structure for table `refretions` */

DROP TABLE IF EXISTS `refretions`;

CREATE TABLE `refretions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `sequence` int(11) NOT NULL,
  `active` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `refretions_title_index` (`title`),
  KEY `refretions_active_index` (`active`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `refretions` */

/*Table structure for table `role_user` */

DROP TABLE IF EXISTS `role_user`;

CREATE TABLE `role_user` (
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `role_user_role_id_foreign` (`role_id`),
  CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `role_user` */

insert  into `role_user`(`user_id`,`role_id`) values (1,1),(2,3);

/*Table structure for table `roles` */

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sequence` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `roles` */

insert  into `roles`(`id`,`name`,`display_name`,`description`,`sequence`,`created_at`,`updated_at`) values (1,'superadmin','Superadmin','The superadmin has all the rights in the system.',1,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(2,'admin','Admin','sds',2,'2016-03-01 15:27:07','2017-03-16 17:07:19'),(3,'editor','Editor','',3,'2016-03-01 15:27:07','2016-03-01 15:27:07');

/*Table structure for table `sessions` */

DROP TABLE IF EXISTS `sessions`;

CREATE TABLE `sessions` (
  `id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payload` text COLLATE utf8_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL,
  UNIQUE KEY `sessions_id_unique` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `sessions` */

/*Table structure for table `settings` */

DROP TABLE IF EXISTS `settings`;

CREATE TABLE `settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `field` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `settings_field_index` (`field`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `settings` */

insert  into `settings`(`id`,`field`,`value`,`created_at`,`updated_at`) values (1,'company','Default Company','2016-03-01 15:27:07','2016-03-04 17:49:56'),(2,'address','Default Address 81','2016-03-01 15:27:07','2016-03-02 21:51:52'),(3,'zip_code','1000 AA','2016-03-01 15:27:07','2016-03-02 21:51:52'),(4,'city','Defaultcity','2016-03-01 15:27:07','2016-03-02 21:51:52'),(5,'email','wahyu_bunyu_jogja@yahoo.co.id','2016-03-01 15:27:07','2016-03-04 15:26:14'),(6,'phone','020-1234567','2016-03-01 15:27:07','2016-03-02 21:51:52'),(7,'mobile','0612345678','2016-03-01 15:27:07','2016-03-02 21:51:52'),(8,'coc','','2016-03-01 15:27:07','2016-03-01 15:27:07'),(9,'vat','','2016-03-01 15:27:07','2016-03-01 15:27:07'),(10,'ga_email','','2016-03-01 15:27:07','2016-03-01 15:27:07'),(11,'ga_site_id','82264673','2016-03-01 15:27:07','2016-03-01 15:28:27'),(12,'ga_tracking_id','UA-6905633-29','2016-03-01 15:27:07','2016-03-01 15:28:27'),(13,'ga_client_id','','2016-03-01 15:27:07','2016-03-01 15:27:07'),(14,'gm_lat','','2016-03-01 15:27:07','2016-03-01 15:27:07'),(15,'gm_long','','2016-03-01 15:27:07','2016-03-01 15:27:07'),(16,'facebook','','2016-03-01 15:27:07','2016-03-01 15:27:07'),(17,'twitter','','2016-03-01 15:27:07','2016-03-01 15:27:07'),(18,'google','','2016-03-01 15:27:07','2016-03-01 15:27:07'),(19,'youtube','','2016-03-01 15:27:07','2016-03-01 15:27:07'),(20,'instagram','','0000-00-00 00:00:00','0000-00-00 00:00:00'),(21,'linkedin','','0000-00-00 00:00:00','0000-00-00 00:00:00'),(22,'message_404','','0000-00-00 00:00:00','2016-03-04 16:44:44'),(23,'message_503','','0000-00-00 00:00:00','0000-00-00 00:00:00');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `surname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `company` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(4) NOT NULL,
  `sequence` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `users` */

insert  into `users`(`id`,`name`,`surname`,`company`,`email`,`password`,`active`,`sequence`,`remember_token`,`created_at`,`updated_at`,`deleted_at`) values (1,'Guus','Portegies','Cees & Co','info@ceesenco.com','$2y$10$iah1D1mT9RE6ANsN7wyWY.3BzDIPox0ce2rM36318Cgb8qSGq0.16',1,2,'5xCCQyPP5m2Hszlx2gwXLkbiMgdMr9gkFTV4jVUodHLMqQnfTnfqaxL3rwVe','2016-03-01 15:27:07','2017-03-28 22:57:40',NULL),(2,'wahyu','wahyu','dfd','wahyu_bunyu_jogja@yahoo.co.id','$2y$10$iah1D1mT9RE6ANsN7wyWY.3BzDIPox0ce2rM36318Cgb8qSGq0.16',1,3,'945aHA0iFIEmM8YQnHkAnnnPwqzlaoEdK1rJbJZ4t0kJXM8w4fppsZC7VDef','2017-03-07 10:32:30','2017-03-07 10:34:08',NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
