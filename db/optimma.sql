/*
SQLyog Ultimate v11.11 (64 bit)
MySQL - 5.6.24 : Database - optima
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`optima` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `optima`;

/*Table structure for table `activity_log` */

DROP TABLE IF EXISTS `activity_log`;

CREATE TABLE `activity_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `text` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ip_address` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `activity_log` */

insert  into `activity_log`(`id`,`user_id`,`text`,`ip_address`,`created_at`,`updated_at`) values (1,1,'Banner(1) \'Welkom bij Optima Makelaardij!\' was created','::1','2017-03-12 18:34:44','2017-03-12 18:34:44'),(2,1,'Image \'slider1.jpg\' uploaded with a size of: 177294','::1','2017-03-12 18:39:17','2017-03-12 18:39:17'),(3,1,'Banner(2) \'Welkom bij Optima Makelaardij!2\' was created','::1','2017-03-12 18:40:37','2017-03-12 18:40:37'),(4,1,'Image \'slider1.jpg\' uploaded with a size of: 177294','::1','2017-03-12 18:40:58','2017-03-12 18:40:58');

/*Table structure for table `attribute_attribute_set` */

DROP TABLE IF EXISTS `attribute_attribute_set`;

CREATE TABLE `attribute_attribute_set` (
  `attribute_set_id` int(10) unsigned NOT NULL,
  `attribute_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`attribute_set_id`,`attribute_id`),
  KEY `attribute_attribute_set_attribute_set_id_index` (`attribute_set_id`),
  KEY `attribute_attribute_set_attribute_id_index` (`attribute_id`),
  CONSTRAINT `attribute_attribute_set_attribute_id_foreign` FOREIGN KEY (`attribute_id`) REFERENCES `attributes` (`id`) ON DELETE CASCADE,
  CONSTRAINT `attribute_attribute_set_attribute_set_id_foreign` FOREIGN KEY (`attribute_set_id`) REFERENCES `attribute_sets` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `attribute_attribute_set` */

/*Table structure for table `attribute_product` */

DROP TABLE IF EXISTS `attribute_product`;

CREATE TABLE `attribute_product` (
  `attribute_id` int(10) unsigned NOT NULL,
  `product_id` int(10) unsigned NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`attribute_id`,`product_id`),
  KEY `attribute_product_attribute_id_index` (`attribute_id`),
  KEY `attribute_product_product_id_index` (`product_id`),
  CONSTRAINT `attribute_product_attribute_id_foreign` FOREIGN KEY (`attribute_id`) REFERENCES `attributes` (`id`) ON DELETE CASCADE,
  CONSTRAINT `attribute_product_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `attribute_product` */

/*Table structure for table `attribute_sets` */

DROP TABLE IF EXISTS `attribute_sets`;

CREATE TABLE `attribute_sets` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sequence` int(11) NOT NULL,
  `active` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `attribute_sets_active_index` (`active`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `attribute_sets` */

/*Table structure for table `attributes` */

DROP TABLE IF EXISTS `attributes`;

CREATE TABLE `attributes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sequence` int(11) NOT NULL,
  `active` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `attributes_name_index` (`name`),
  KEY `attributes_active_index` (`active`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `attributes` */

/*Table structure for table `banners` */

DROP TABLE IF EXISTS `banners`;

CREATE TABLE `banners` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `sequence` int(11) NOT NULL,
  `active` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `banners_title_index` (`title`),
  KEY `banners_active_index` (`active`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `banners` */

insert  into `banners`(`id`,`title`,`description`,`sequence`,`active`,`created_at`,`updated_at`) values (1,'Welkom bij Optima Makelaardij!','<p>Optima Makelaardij is een jong en dynamisch makelaarskantoor wat zich richt op de bemiddeling bij aan- en verkoop van onroerend goed. &nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;</p>\r\n',1,0,'2017-03-12 18:34:44','2017-03-12 18:34:44'),(2,'Welkom bij Optima Makelaardij!2','<p>Optima Makelaardij is een jong en dynamisch makelaarskantoor wat zich richt op de bemiddeling bij aan- en verkoop van onroerend goed</p>\r\n',2,0,'2017-03-12 18:40:36','2017-03-12 18:40:36');

/*Table structure for table `block_translations` */

DROP TABLE IF EXISTS `block_translations`;

CREATE TABLE `block_translations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `block_id` int(10) unsigned NOT NULL,
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_keywords` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text` text COLLATE utf8_unicode_ci NOT NULL,
  `button_link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `button_text` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `block_translations_block_id_locale_unique` (`block_id`,`locale`),
  KEY `block_translations_locale_index` (`locale`),
  KEY `block_translations_slug_index` (`slug`),
  CONSTRAINT `block_translations_block_id_foreign` FOREIGN KEY (`block_id`) REFERENCES `blocks` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `block_translations` */

/*Table structure for table `blocks` */

DROP TABLE IF EXISTS `blocks`;

CREATE TABLE `blocks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sequence` int(11) NOT NULL,
  `active` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `blocks_active_index` (`active`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `blocks` */

/*Table structure for table `categories` */

DROP TABLE IF EXISTS `categories`;

CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `sequence` int(11) NOT NULL,
  `active` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `categories_active_index` (`active`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `categories` */

/*Table structure for table `category_product` */

DROP TABLE IF EXISTS `category_product`;

CREATE TABLE `category_product` (
  `category_id` int(10) unsigned NOT NULL,
  `product_id` int(10) unsigned NOT NULL,
  `sequence` int(11) NOT NULL,
  PRIMARY KEY (`category_id`,`product_id`),
  KEY `category_product_category_id_index` (`category_id`),
  KEY `category_product_product_id_index` (`product_id`),
  CONSTRAINT `category_product_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE,
  CONSTRAINT `category_product_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `category_product` */

/*Table structure for table `category_translations` */

DROP TABLE IF EXISTS `category_translations`;

CREATE TABLE `category_translations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(10) unsigned NOT NULL,
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_keywords` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `category_translations_category_id_locale_unique` (`category_id`,`locale`),
  KEY `category_translations_locale_index` (`locale`),
  KEY `category_translations_slug_index` (`slug`),
  CONSTRAINT `category_translations_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `category_translations` */

/*Table structure for table `fundas` */

DROP TABLE IF EXISTS `fundas`;

CREATE TABLE `fundas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `value` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `fundas` */

insert  into `fundas`(`id`,`value`,`created_at`,`updated_at`) values (1,'100','2017-03-13 01:56:05','2017-03-12 20:06:54');

/*Table structure for table `images` */

DROP TABLE IF EXISTS `images`;

CREATE TABLE `images` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `imageable_id` int(11) NOT NULL,
  `imageable_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `size` int(11) NOT NULL,
  `extension` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `original_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sequence` int(11) NOT NULL,
  `active` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `images_active_index` (`active`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `images` */

insert  into `images`(`id`,`imageable_id`,`imageable_type`,`path`,`size`,`extension`,`name`,`description`,`label`,`link`,`original_name`,`sequence`,`active`,`created_at`,`updated_at`) values (1,1,'App\\Models\\Banner','9d7732100f5a975a113dc65d90cd01d3e985ecf3',177294,'jpg','','','','','slider1.jpg',1,1,'2017-03-12 18:39:17','2017-03-12 18:39:17'),(2,2,'App\\Models\\Banner','e5383a9e3ae1cc50927511a241aa3b877bbbb47a',177294,'jpg','','','','','slider1.jpg',2,1,'2017-03-12 18:40:57','2017-03-12 18:40:58');

/*Table structure for table `language_translations` */

DROP TABLE IF EXISTS `language_translations`;

CREATE TABLE `language_translations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `language_id` int(10) unsigned NOT NULL,
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `language_translations_language_id_locale_unique` (`language_id`,`locale`),
  KEY `language_translations_locale_index` (`locale`),
  CONSTRAINT `language_translations_language_id_foreign` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `language_translations` */

insert  into `language_translations`(`id`,`language_id`,`locale`,`name`) values (1,1,'nl','Nederlands'),(2,1,'en','Dutch'),(3,1,'fr','Néerlandais'),(4,1,'de','Holländisch'),(5,1,'es','Holandés'),(6,2,'nl','Engels'),(7,2,'en','English'),(8,2,'fr','Anglais'),(9,2,'de','Englisch'),(10,2,'es','Inglés'),(11,3,'nl','Frans'),(12,3,'en','French'),(13,3,'fr','Français'),(14,3,'de','Französisch'),(15,3,'es','Francés'),(16,4,'nl','Duits'),(17,4,'en','German'),(18,4,'fr','Allemand'),(19,4,'de','Deutsch'),(20,4,'es','Alemán'),(21,5,'nl','Spaans'),(22,5,'en','Spanish'),(23,5,'fr','Espagnol'),(24,5,'de','Spanisch'),(25,5,'es','Español');

/*Table structure for table `languages` */

DROP TABLE IF EXISTS `languages`;

CREATE TABLE `languages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `flag` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sequence` int(11) NOT NULL,
  `active` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `languages_code_unique` (`code`),
  KEY `languages_active_index` (`active`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `languages` */

insert  into `languages`(`id`,`code`,`flag`,`sequence`,`active`,`created_at`,`updated_at`,`deleted_at`) values (1,'nl','nl',1,0,'2016-03-01 15:27:07','2017-03-03 12:44:44',NULL),(2,'en','gb',2,1,'2016-03-01 15:27:07','2017-03-03 12:44:38',NULL),(3,'fr','fr',3,0,'2016-03-01 15:27:07','2016-03-01 15:27:07',NULL),(4,'de','de',4,0,'2016-03-01 15:27:07','2016-03-01 15:27:07',NULL),(5,'es','es',5,0,'2016-03-01 15:27:07','2016-03-01 15:27:07',NULL);

/*Table structure for table `migrations` */

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `migrations` */

insert  into `migrations`(`migration`,`batch`) values ('2014_10_12_000000_create_users_table',1),('2014_10_12_100000_create_password_resets_table',1),('2015_12_23_082721_entrust_setup_tables',1),('2015_12_23_144256_create_modules_table',1),('2015_12_30_083328_create_languages_table',1),('2016_01_15_113228_create_sessions_table',1),('2016_01_15_115055_create_pages_table',1),('2016_01_15_143558_create_images_table',1),('2016_01_22_120310_create_blocks_table',1),('2016_01_23_095415_create_categories_table',1),('2016_01_23_113452_create_attributes_table',1),('2016_01_23_131030_create_attribute_sets_table',1),('2016_01_26_143436_create_attribute_set_attribute_pivot_table',1),('2016_01_26_160205_create_products_table',1),('2016_01_26_160756_create_category_product_pivot_table',1),('2016_01_29_121612_create_attribute_product_pivot_table',1),('2016_01_30_100023_create_news_table',1),('2016_01_30_110014_create_banners_table',1),('2016_01_30_120945_create_settings_table',1),('2016_03_04_111204_create_activity_log_table',1),('2017_03_12_175236_create_topmenu_table',1),('2017_03_12_180304_create_topmenu_table',1),('2017_03_12_184931_create_topmenu_repositories_table',1),('2017_03_12_193514_create_funda_table',2);

/*Table structure for table `modules` */

DROP TABLE IF EXISTS `modules`;

CREATE TABLE `modules` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `config` text COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(4) NOT NULL,
  `sequence` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `modules_name_index` (`name`),
  KEY `modules_active_index` (`active`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `modules` */

insert  into `modules`(`id`,`name`,`display_name`,`config`,`active`,`sequence`,`created_at`,`updated_at`) values (1,'pages','Hoofdpagina\'s','{\"active\":1,\"settings\":{\"images\":{\"max_size\":10000,\"max_images\":1,\"sizes\":{\"thumb\":{\"width\":64,\"height\":64},\"small\":{\"width\":200,\"height\":200},\"medium\":{\"width\":450,\"height\":450},\"big\":{\"width\":1000,\"height\":1000}}}}}',0,1,'2016-03-01 15:27:07','2017-03-12 19:13:32'),(2,'subpages','Subpagina\'s','{\"active\":1,\"button\":1,\"settings\":{\"images\":{\"max_size\":10000,\"max_images\":1,\"sizes\":{\"thumb\":{\"width\":64,\"height\":64},\"small\":{\"width\":200,\"height\":200},\"medium\":{\"width\":450,\"height\":450},\"big\":{\"width\":1000,\"height\":1000}}}}}',0,2,'2016-03-01 15:27:07','2016-03-04 19:39:50'),(3,'blocks','Statische blokken','{\"active\":1,\"settings\":{\"images\":{\"max_size\":10000,\"max_images\":1,\"sizes\":{\"thumb\":{\"width\":64,\"height\":64},\"small\":{\"width\":200,\"height\":200},\"medium\":{\"width\":450,\"height\":450},\"big\":{\"width\":1000,\"height\":1000}}}}}',0,3,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(4,'banners','Beheer banners','{\"active\":1,\"settings\":{\"images\":{\"max_size\":10000,\"max_images\":1,\"sizes\":{\"thumb\":{\"width\":64,\"height\":64},\"small\":{\"width\":200,\"height\":200},\"medium\":{\"width\":450,\"height\":450},\"big\":{\"width\":1000,\"height\":1000},\"banner\":{\"width\":1600,\"height\":1000}}}}}',1,4,'2016-03-01 15:27:07','2017-03-12 18:33:36'),(5,'categories','Product categorieen','{\"active\":1,\"settings\":{\"images\":{\"max_size\":10000,\"max_images\":1,\"sizes\":{\"thumb\":{\"width\":64,\"height\":64},\"small\":{\"width\":200,\"height\":200},\"medium\":{\"width\":450,\"height\":450},\"big\":{\"width\":1000,\"height\":1000}}}}}',0,5,'2016-03-01 15:27:07','2017-03-09 23:16:56'),(6,'products','Producten','{\"active\":1,\"settings\":{\"images\":{\"max_size\":10000,\"max_images\":1,\"sizes\":{\"thumb\":{\"width\":64,\"height\":64},\"small\":{\"width\":200,\"height\":200},\"medium\":{\"width\":450,\"height\":450},\"big\":{\"width\":1000,\"height\":1000}}}}}',0,6,'2016-03-01 15:27:07','2017-03-12 18:33:28'),(7,'projectgroups','Projectgroepen','{\"active\":1,\"settings\":{\"images\":{\"max_size\":10000,\"max_images\":1,\"sizes\":{\"thumb\":{\"width\":64,\"height\":64},\"small\":{\"width\":200,\"height\":200},\"medium\":{\"width\":450,\"height\":450},\"big\":{\"width\":1000,\"height\":1000}}}}}',0,7,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(8,'projects','Projecten','{\"active\":1,\"settings\":{\"images\":{\"max_size\":10000,\"max_images\":1,\"sizes\":{\"thumb\":{\"width\":64,\"height\":64},\"small\":{\"width\":200,\"height\":200},\"medium\":{\"width\":450,\"height\":450},\"big\":{\"width\":1000,\"height\":1000}}}}}',0,8,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(9,'news','Nieuws','{\"active\":1,\"settings\":{\"images\":{\"max_size\":10000,\"max_images\":1,\"sizes\":{\"thumb\":{\"width\":64,\"height\":64},\"small\":{\"width\":200,\"height\":200},\"medium\":{\"width\":450,\"height\":450},\"big\":{\"width\":1000,\"height\":1000}}}}}',0,9,'2016-03-01 15:27:07','2017-03-12 18:41:16'),(10,'topmenus','topmenus','{\"active\":1,\"settings\":{\"images\":{\"max_size\":10000,\"max_images\":1,\"sizes\":{\"thumb\":{\"width\":64,\"height\":64},\"small\":{\"width\":200,\"height\":200},\"medium\":{\"width\":450,\"height\":450},\"big\":{\"width\":1000,\"height\":1000}}}}}',1,10,'2017-03-13 01:08:57','2017-03-12 19:22:29'),(11,'funda','funda','{\"active\":1,\"settings\":{\"images\":{\"max_size\":10000,\"max_images\":1,\"sizes\":{\"thumb\":{\"width\":64,\"height\":64},\"small\":{\"width\":200,\"height\":200},\"medium\":{\"width\":450,\"height\":450},\"big\":{\"width\":1000,\"height\":1000}}}}}',1,11,'2017-03-13 02:09:22','2017-03-12 20:09:45');

/*Table structure for table `news` */

DROP TABLE IF EXISTS `news`;

CREATE TABLE `news` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sequence` int(11) NOT NULL,
  `active` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `news_active_index` (`active`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `news` */

/*Table structure for table `news_translations` */

DROP TABLE IF EXISTS `news_translations`;

CREATE TABLE `news_translations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `news_id` int(10) unsigned NOT NULL,
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `posted_on` datetime NOT NULL,
  `meta_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_keywords` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text` text COLLATE utf8_unicode_ci NOT NULL,
  `intro` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `news_translations_news_id_locale_unique` (`news_id`,`locale`),
  KEY `news_translations_locale_index` (`locale`),
  KEY `news_translations_slug_index` (`slug`),
  CONSTRAINT `news_translations_news_id_foreign` FOREIGN KEY (`news_id`) REFERENCES `news` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `news_translations` */

/*Table structure for table `page_translations` */

DROP TABLE IF EXISTS `page_translations`;

CREATE TABLE `page_translations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `page_id` int(10) unsigned NOT NULL,
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_keywords` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `page_translations_page_id_locale_unique` (`page_id`,`locale`),
  KEY `page_translations_locale_index` (`locale`),
  KEY `page_translations_slug_index` (`slug`),
  CONSTRAINT `page_translations_page_id_foreign` FOREIGN KEY (`page_id`) REFERENCES `pages` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `page_translations` */

/*Table structure for table `pages` */

DROP TABLE IF EXISTS `pages`;

CREATE TABLE `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `sequence` int(11) NOT NULL,
  `active` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `pages_active_index` (`active`),
  KEY `pages_parent_id_foreign` (`parent_id`),
  CONSTRAINT `pages_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `pages` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `pages` */

/*Table structure for table `password_resets` */

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `password_resets` */

/*Table structure for table `permission_role` */

DROP TABLE IF EXISTS `permission_role`;

CREATE TABLE `permission_role` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `permission_role_role_id_foreign` (`role_id`),
  CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `permission_role` */

insert  into `permission_role`(`permission_id`,`role_id`) values (59,2),(59,3);

/*Table structure for table `permissions` */

DROP TABLE IF EXISTS `permissions`;

CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sequence` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `permissions_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `permissions` */

insert  into `permissions`(`id`,`name`,`display_name`,`description`,`sequence`,`created_at`,`updated_at`) values (1,'access-permission','Rechten beheren','',0,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(2,'create-permission','Rechten toevoegen','',1,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(3,'edit-permission','Rechten bewerken','',2,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(4,'delete-permission','Rechten verwijderen','',3,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(5,'access-role','Rollen beheren','',4,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(6,'create-role','Rollen toevoegen','',5,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(7,'edit-role','Rollen bewerken','',6,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(8,'delete-role','Rollen verwijderen','',7,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(9,'access-user','Gebruikers beheren','',8,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(10,'create-user','Gebruikers toevoegen','',9,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(11,'edit-user','Gebruikers bewerken','',10,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(12,'delete-user','Gebruikers verwijderen','',11,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(13,'access-page','Pagina\'s beheren','',12,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(14,'create-page','Pagina\'s toevoegen','',13,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(15,'edit-page','Pagina\'s bewerken','',14,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(16,'delete-page','Pagina\'s verwijderen','',15,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(17,'access-subpage','Subpagina\'s beheren','',16,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(18,'create-subpage','Subpagina\'s toevoegen','',17,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(19,'edit-subpage','Subpagina\'s bewerken','',18,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(20,'delete-subpage','Subpagina\'s verwijderen','',19,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(21,'access-block','Statische blokken beheren','',20,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(22,'create-block','Statische blokken toevoegen','',21,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(23,'edit-block','Statische blokken bewerken','',22,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(24,'delete-block','Statische blokken verwijderen','',23,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(25,'access-banner','Banners beheren','',24,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(26,'create-banner','Banners toevoegen','',25,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(27,'edit-banner','Banners bewerken','',26,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(28,'delete-banner','Banners verwijderen','',27,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(29,'access-category','Product categorieen beheren','',28,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(30,'create-category','Product categorieen toevoegen','',29,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(31,'edit-category','Product categorieen bewerken','',30,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(32,'delete-category','Product categorieen verwijderen','',31,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(33,'access-product','Producten beheren','',32,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(34,'create-product','Producten toevoegen','',33,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(35,'edit-product','Producten bewerken','',34,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(36,'delete-product','Producten verwijderen','',35,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(37,'access-attribute','Attributen beheren','',36,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(38,'create-attribute','Attribuut toevoegen','',37,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(39,'edit-attribute','Attribuut bewerken','',38,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(40,'delete-attribute','Attributen verwijderen','',39,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(41,'access-attributeset','Attributen set beheren','',40,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(42,'create-attributeset','Attributen set toevoegen','',41,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(43,'edit-attributeset','Attributen set bewerken','',42,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(44,'delete-attributeset','Attributen set verwijderen','',43,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(45,'access-projectgroup','Projectgroepen beheren','',44,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(46,'create-projectgroup','Projectgroepen toevoegen','',45,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(47,'edit-projectgroup','Projectgroepen bewerken','',46,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(48,'delete-projectgroup','Projectgroepen verwijderen','',47,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(49,'access-project','Projecten beheren','',48,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(50,'create-project','Projecten toevoegen','',49,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(51,'edit-project','Projecten bewerken','',50,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(52,'delete-project','Projecten verwijderen','',51,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(53,'access-news','Nieuws beheren','',52,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(54,'create-news','Nieuws toevoegen','',53,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(55,'edit-news','Nieuws bewerken','',54,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(56,'delete-news','Nieuws verwijderen','',55,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(57,'access-setting','Instellingen beheren','',56,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(58,'edit-setting','Instellingen bewerken','',57,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(59,'reset-admin-password','Herstel admin wachtwoord ','',58,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(60,'access-topmenu','topmenu beheren',' -',0,'2017-03-12 19:19:31','2017-03-12 19:19:31'),(61,'edit-topmenu','Topmenu bewerken','-',0,'2017-03-12 19:20:15','2017-03-12 19:20:15'),(62,'access-funda','funda beheren','-',0,'2017-03-12 20:07:54','2017-03-12 20:07:54'),(63,'edit-funda','Funda bewerken','-',0,'2017-03-12 20:08:31','2017-03-12 20:08:31');

/*Table structure for table `product_translations` */

DROP TABLE IF EXISTS `product_translations`;

CREATE TABLE `product_translations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(10) unsigned NOT NULL,
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `intro` text COLLATE utf8_unicode_ci NOT NULL,
  `text` text COLLATE utf8_unicode_ci NOT NULL,
  `meta_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_keywords` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `product_translations_product_id_locale_unique` (`product_id`,`locale`),
  KEY `product_translations_locale_index` (`locale`),
  KEY `product_translations_slug_index` (`slug`),
  CONSTRAINT `product_translations_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `product_translations` */

/*Table structure for table `products` */

DROP TABLE IF EXISTS `products`;

CREATE TABLE `products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `attribute_set_id` int(10) unsigned NOT NULL,
  `price` double NOT NULL,
  `active` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `products_attribute_set_id_foreign` (`attribute_set_id`),
  KEY `products_active_index` (`active`),
  CONSTRAINT `products_attribute_set_id_foreign` FOREIGN KEY (`attribute_set_id`) REFERENCES `attribute_sets` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `products` */

/*Table structure for table `role_user` */

DROP TABLE IF EXISTS `role_user`;

CREATE TABLE `role_user` (
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `role_user_role_id_foreign` (`role_id`),
  CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `role_user` */

insert  into `role_user`(`user_id`,`role_id`) values (1,1),(2,3);

/*Table structure for table `roles` */

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sequence` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `roles` */

insert  into `roles`(`id`,`name`,`display_name`,`description`,`sequence`,`created_at`,`updated_at`) values (1,'superadmin','Superadmin','The superadmin has all the rights in the system.',1,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(2,'admin','Admin','',2,'2016-03-01 15:27:07','2016-03-01 15:27:07'),(3,'editor','Editor','',3,'2016-03-01 15:27:07','2016-03-01 15:27:07');

/*Table structure for table `sessions` */

DROP TABLE IF EXISTS `sessions`;

CREATE TABLE `sessions` (
  `id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payload` text COLLATE utf8_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL,
  UNIQUE KEY `sessions_id_unique` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `sessions` */

/*Table structure for table `settings` */

DROP TABLE IF EXISTS `settings`;

CREATE TABLE `settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `field` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `settings_field_index` (`field`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `settings` */

insert  into `settings`(`id`,`field`,`value`,`created_at`,`updated_at`) values (1,'company','Default Company','2016-03-01 15:27:07','2016-03-04 17:49:56'),(2,'address','Default Address 81','2016-03-01 15:27:07','2016-03-02 21:51:52'),(3,'zip_code','1000 AA','2016-03-01 15:27:07','2016-03-02 21:51:52'),(4,'city','Defaultcity','2016-03-01 15:27:07','2016-03-02 21:51:52'),(5,'email','guus@ceesenco.com','2016-03-01 15:27:07','2016-03-04 15:26:14'),(6,'phone','020-1234567','2016-03-01 15:27:07','2016-03-02 21:51:52'),(7,'mobile','0612345678','2016-03-01 15:27:07','2016-03-02 21:51:52'),(8,'coc','','2016-03-01 15:27:07','2016-03-01 15:27:07'),(9,'vat','','2016-03-01 15:27:07','2016-03-01 15:27:07'),(10,'ga_email','','2016-03-01 15:27:07','2016-03-01 15:27:07'),(11,'ga_site_id','82264673','2016-03-01 15:27:07','2016-03-01 15:28:27'),(12,'ga_tracking_id','UA-6905633-29','2016-03-01 15:27:07','2016-03-01 15:28:27'),(13,'ga_client_id','','2016-03-01 15:27:07','2016-03-01 15:27:07'),(14,'gm_lat','','2016-03-01 15:27:07','2016-03-01 15:27:07'),(15,'gm_long','','2016-03-01 15:27:07','2016-03-01 15:27:07'),(16,'facebook','','2016-03-01 15:27:07','2016-03-01 15:27:07'),(17,'twitter','','2016-03-01 15:27:07','2016-03-01 15:27:07'),(18,'google','','2016-03-01 15:27:07','2016-03-01 15:27:07'),(19,'youtube','','2016-03-01 15:27:07','2016-03-01 15:27:07'),(20,'instagram','','0000-00-00 00:00:00','0000-00-00 00:00:00'),(21,'linkedin','','0000-00-00 00:00:00','0000-00-00 00:00:00'),(22,'message_404','','0000-00-00 00:00:00','2016-03-04 16:44:44'),(23,'message_503','','0000-00-00 00:00:00','0000-00-00 00:00:00');

/*Table structure for table `topmenu_repositories` */

DROP TABLE IF EXISTS `topmenu_repositories`;

CREATE TABLE `topmenu_repositories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `topmenu_repositories` */

/*Table structure for table `topmenus` */

DROP TABLE IF EXISTS `topmenus`;

CREATE TABLE `topmenus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `topmenus` */

insert  into `topmenus`(`id`,`type`,`name`,`link`,`created_at`,`updated_at`) values (1,'phone','0229 - 50 66 90','0229 - 50 66 90','2017-03-07 19:24:46','2017-03-08 06:57:49'),(2,'email','info@optimamakelaardij.nl','mailto:info@optimamakelaardij.nl','2017-03-10 14:26:58','0000-00-00 00:00:00'),(3,'fb',' Volg ons!','#','2017-03-13 01:05:42','0000-00-00 00:00:00');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `surname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `company` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(4) NOT NULL,
  `sequence` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `users` */

insert  into `users`(`id`,`name`,`surname`,`company`,`email`,`password`,`active`,`sequence`,`remember_token`,`created_at`,`updated_at`,`deleted_at`) values (1,'Guus','Portegies','Cees & Co','info@ceesenco.com','$2y$10$iah1D1mT9RE6ANsN7wyWY.3BzDIPox0ce2rM36318Cgb8qSGq0.16',1,2,'aAeErdNevYZCaPRdCSEDGFVEeQBcXOZrHDrOMsw1zOzk6m07kJvx6nbg7TxY','2016-03-01 15:27:07','2017-03-07 10:33:12',NULL),(2,'wahyu','wahyu','dfd','wahyu_bunyu_jogja@yahoo.co.id','$2y$10$iah1D1mT9RE6ANsN7wyWY.3BzDIPox0ce2rM36318Cgb8qSGq0.16',1,3,'945aHA0iFIEmM8YQnHkAnnnPwqzlaoEdK1rJbJZ4t0kJXM8w4fppsZC7VDef','2017-03-07 10:32:30','2017-03-07 10:34:08',NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
