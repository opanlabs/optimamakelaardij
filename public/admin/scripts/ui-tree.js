// Draw tree structue
var $tree = $('#parent-tree').tree({
    saveState: $('#parent-tree').data('name'),
    dragAndDrop: true,
    closedIcon: $('<i class="indented tree-icon fa fa-fw m-r-xs fa-caret-right"></i>'),
    openedIcon: $('<i class="indented tree-icon fa fa-caret-down fa-fw m-r-xs"></i>'),
    onCanMove: function(node) {
        if (!node.parent.parent) {
            // Cannot move root node
            return false;
        } else {
            return true;
        }
    }
}).bind(
    'tree.select',
    function(event) {
        if (event.node) {
            // node was selected
            oTable.draw();
        } else {
            // node was deselected
            oTable.draw();
        }
    }
).bind(
    'tree.init',
    function() {
        // Refresh table content
        oTable.draw();
    }
).bind(
    'tree.move',
    function(event) {
        var moved_node  = event.move_info.moved_node;
        var target_node = event.move_info.target_node;
        var position    = event.move_info.position;
        var prev_parent = event.move_info.previous_parent;

        $.ajax({
            url: $('#parent-tree').data('move'),
            type: 'POST',
            data: {
                id: moved_node.id,
                new_parent: target_node.id,
                old_parent: prev_parent.id
            }
        });
        // Refresh table content
        oTable.draw();
    }
);