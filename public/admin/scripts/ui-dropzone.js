// Start uploading
myDropzone.on("addedfile", function(file) {
    // Hookup the start button
    file.previewElement.querySelector(".start").onclick = function() { myDropzone.enqueueFile(file); };
});

// Update the total progress bar
myDropzone.on("totaluploadprogress", function(progress) {
    document.querySelector("#total-progress .progress-bar").style.width = progress + "%";
});

myDropzone.on("sending", function(file, xhr, formData) {
    // And disable the start button
    file.previewElement.querySelector(".start").setAttribute("disabled", "disabled");
    formData.append('module', $('#dropzone').data('module'));
    formData.append('imageable_type', $('#dropzone').data('type'));
    formData.append('imageable_id', $('#dropzone').data('id'));
});

// On successfull upload
myDropzone.on("success", function(file) {
    this.options.maxFiles = this.options.maxFiles - 1;
});

// Hide the total progress bar when nothing's uploading anymore
myDropzone.on("queuecomplete", function(progress) {
    this.removeAllFiles();
    document.querySelector("#total-progress .progress-bar").style.width = "0%";
    getImages($('#dropzone').data('type'), $('#dropzone').data('id'));
});

// Set up error handling messages
myDropzone.on("maxfilesexceeded", function(file) {
    $('#dropzone-error').append('Maximaal aantal afbeeldingen bereikt.');
    $('#dropzone-error').removeClass('hide');
});

// Setup the buttons for all transfers
document.querySelector("#actions .start").onclick = function() {
    myDropzone.enqueueFiles(myDropzone.getFilesWithStatus(Dropzone.ADDED));
};

document.querySelector("#actions .cancel").onclick = function() {
    myDropzone.removeAllFiles(true);
};