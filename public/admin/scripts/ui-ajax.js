/**
 * Created by guus on 14-11-2015.
 */
$(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
});

$(document.body)
    .on('click', '.ajax-delete', function(e) {
        e.preventDefault();
        var url = $(this).attr('href');

        swal({
            title: 'Weet je zeker dat je dit wilt verwijderen?',
            text: 'Je kunt het hierna niet meer herstellen',
            type: 'warning',
            showCancelButton: true,
            cancelButtonText: 'Nee',
            confirmButtonText: 'Ja',
            closeOnConfirm: true
        }, function() {
            $.ajax({
                url: url,
                type: 'POST',
                data: {
                    _method: 'DELETE'
                }
            })
            .done(function(data) {
                swal('Verwijderd!', 'De onderdeel is successvol verwijderd.', 'success');

                if (data.tree !== null) {
                    $tree.tree('loadData', data.tree);
                }

                oTable.draw();
            })
            .error(function(data) {
                swal('Oeps...', 'Er is iets fout gegaan.', 'error');
            });
        });
    })
    .on('click', '.ajax-active', function(e) {
        e.preventDefault();
        var url = $(this).attr('href');

        $.ajax({
            url: url,
            type: 'GET',
            success: function() {
                oTable.draw();
            },
            error: function(data) {
                $.growl.error({
                    message: data.responseJSON.message
                });
            }
        });
    });