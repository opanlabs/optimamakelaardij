$( document ).ready(function() {
    $('.slider-wrapper-block').slick({
    	dots: true,
    	prevArrow: false,
    	nextArrow: false,
    	autoplay:2000
    });
    $('.refrence-wrapper-block').slick({
    	prevArrow: '<i class="fa fa-angle-left prev" aria-hidden="true"></i>',
    	nextArrow: '<i class="fa fa-angle-right next" aria-hidden="true"></i>',
    	autoplay:2000
    });

    $('.slider-for').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        prevArrow: '<i class="fa fa-angle-left prev" aria-hidden="true"></i>',
        nextArrow: '<i class="fa fa-angle-right next" aria-hidden="true"></i>',
        asNavFor: '.slider-nav',
    });
    $('.slider-nav').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        asNavFor: '.slider-for',
        arrows: false,
        dots: false,
        centerMode: false,
        focusOnSelect: true,
        responsive: [{
            breakpoint: 1280,
            settings: {
                slidesToShow: 2
            }
        },{
            breakpoint: 768,
            settings: {
                arrows: false,
                slidesToShow: 2
            }
        }]
    });

    $('#back-to-top').on('click', function (e) {
        e.preventDefault();
        $('html,body').animate({
            scrollTop: 0
        }, 700);
    });
	$(".contact-form").submit(function(e) {
		var msgRequired =' can not be empty';
		var msgExtNotValid =' does not match format';
		var name = $('.name').val();
		var phone = $('.phones').val();
		var email = $('.email').val();
		var onderwerp = $('.onderwerp').val();
	
		var emailFilter = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
		var phoneFilter = /[^0-9\.]/g;
		var valid=true;
		 $('#info-status').html('');
		var msg ='';
		
		if(name=="")
		{
			msg+='The Colom name'+msgRequired+' <br>';
			
			valid=false;
		}
		if(phone=="")
		{
			msg+='The Colom phone'+msgRequired+' <br>';
			
			valid=false;
		}
		else
		{
			if(phone.match(phoneFilter))
			  {
					valid =false;
					msg+='The Colom phone'+msgExtNotValid+' <br>';
					
			  }
			
		 }
		 if(email=="")
		{
			msg+='The Colom email'+msgRequired+' <br>';
			
			valid=false;
		}
		else
		{
			
			if(!email.match(emailFilter))
			  {
					valid =false;
					alert('email2');
					msg+='The Colom email'+msgExtNotValid+' <br>';
			  }
			 
		 }
		if(onderwerp=="")
		{
			msg+='The Colom onderwerp'+msgRequired+' <br>';
			
			valid=false;
		}
		if(valid==false)
		{
			
			 $('#info-status').html(msg);
			 	return false;
		}
		
        
    });
	function createAlert(msg,type){
        var title = '';
        if(type == 'success'){
            title = 'Success';
        } else {
            title = 'Failed';
        }

        return '<div id="alert" class="alert alert-'+ type +' alert-dismissible fade in" role="alert">' +
            '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
            '<span aria-hidden="true">×</span>' +
            '</button>' +
            '<strong>'+ title +'!</strong> ' + msg +
            '</div>';
    }
});