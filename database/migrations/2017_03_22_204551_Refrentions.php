<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Refrentions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
		Schema::create('refretions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->index();
            $table->text('description');
            $table->integer('sequence');
            $table->tinyInteger('active')->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
		Schema::drop('refretions');
    }
}
