<?php

use App\Models\Language;
use App\Models\LanguageTranslation;
use Illuminate\Database\Seeder;

class LanguageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * Nederlands
         */
        $nl = Language::create([
            'code' => 'nl',
            'flag' => 'nl',
            'active' => 1,
            'sequence' => 1
        ]);

        LanguageTranslation::create([
            'language_id' => $nl->id,
            'locale' => 'nl',
            'name' => 'Nederlands'
        ]);

        LanguageTranslation::create([
            'language_id' => $nl->id,
            'locale' => 'en',
            'name' => 'Dutch'
        ]);

        LanguageTranslation::create([
            'language_id' => $nl->id,
            'locale' => 'fr',
            'name' => 'Néerlandais'
        ]);

        LanguageTranslation::create([
            'language_id' => $nl->id,
            'locale' => 'de',
            'name' => 'Holländisch'
        ]);

        LanguageTranslation::create([
            'language_id' => $nl->id,
            'locale' => 'es',
            'name' => 'Holandés'
        ]);

        /**
         * Engels
         */
        $en = Language::create([
            'code' => 'en',
            'flag' => 'gb',
            'active' => 0,
            'sequence' => 2
        ]);

        LanguageTranslation::create([
            'language_id' => $en->id,
            'locale' => 'nl',
            'name' => 'Engels'
        ]);

        LanguageTranslation::create([
            'language_id' => $en->id,
            'locale' => 'en',
            'name' => 'English'
        ]);

        LanguageTranslation::create([
            'language_id' => $en->id,
            'locale' => 'fr',
            'name' => 'Anglais'
        ]);

        LanguageTranslation::create([
            'language_id' => $en->id,
            'locale' => 'de',
            'name' => 'Englisch'
        ]);

        LanguageTranslation::create([
            'language_id' => $en->id,
            'locale' => 'es',
            'name' => 'Inglés'
        ]);

        /**
         * Frans
         */
        $fr = Language::create([
            'code' => 'fr',
            'flag' => 'fr',
            'active' => 0,
            'sequence' => 3
        ]);

        LanguageTranslation::create([
            'language_id' => $fr->id,
            'locale' => 'nl',
            'name' => 'Frans'
        ]);

        LanguageTranslation::create([
            'language_id' => $fr->id,
            'locale' => 'en',
            'name' => 'French'
        ]);

        LanguageTranslation::create([
            'language_id' => $fr->id,
            'locale' => 'fr',
            'name' => 'Français'
        ]);

        LanguageTranslation::create([
            'language_id' => $fr->id,
            'locale' => 'de',
            'name' => 'Französisch'
        ]);

        LanguageTranslation::create([
            'language_id' => $fr->id,
            'locale' => 'es',
            'name' => 'Francés'
        ]);

        /**
         * Duits
         */
        $de = Language::create([
            'code' => 'de',
            'flag' => 'de',
            'active' => 0,
            'sequence' => 4
        ]);

        LanguageTranslation::create([
            'language_id' => $de->id,
            'locale' => 'nl',
            'name' => 'Duits'
        ]);

        LanguageTranslation::create([
            'language_id' => $de->id,
            'locale' => 'en',
            'name' => 'German'
        ]);

        LanguageTranslation::create([
            'language_id' => $de->id,
            'locale' => 'fr',
            'name' => 'Allemand'
        ]);

        LanguageTranslation::create([
            'language_id' => $de->id,
            'locale' => 'de',
            'name' => 'Deutsch'
        ]);

        LanguageTranslation::create([
            'language_id' => $de->id,
            'locale' => 'es',
            'name' => 'Alemán'
        ]);

        /**
         * Spaans
         */
        $es = Language::create([
            'code' => 'es',
            'flag' => 'es',
            'active' => 0,
            'sequence' => 5
        ]);

        LanguageTranslation::create([
            'language_id' => $es->id,
            'locale' => 'nl',
            'name' => 'Spaans'
        ]);

        LanguageTranslation::create([
            'language_id' => $es->id,
            'locale' => 'en',
            'name' => 'Spanish'
        ]);

        LanguageTranslation::create([
            'language_id' => $es->id,
            'locale' => 'fr',
            'name' => 'Espagnol'
        ]);

        LanguageTranslation::create([
            'language_id' => $es->id,
            'locale' => 'de',
            'name' => 'Spanisch'
        ]);

        LanguageTranslation::create([
            'language_id' => $es->id,
            'locale' => 'es',
            'name' => 'Español'
        ]);
    }
}
