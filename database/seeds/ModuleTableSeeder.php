<?php

use App\Models\Module;
use Illuminate\Database\Seeder;

class ModuleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * PAGES
         */
        Module::create([
            'name'          => 'pages',
            'display_name'  => 'Hoofdpagina\'s',
            'config'        => Config::get('modules.pages'),
            'active'        => 0,
            'sequence'      => 1
        ]);

        Module::create([
            'name'          => 'subpages',
            'display_name'  => 'Subpagina\'s',
            'config'        => Config::get('modules.subpages'),
            'active'        => 0,
            'sequence'      => 2
        ]);

        Module::create([
            'name'          => 'blocks',
            'display_name'  => 'Statische blokken',
            'config'        => Config::get('modules.blocks'),
            'active'        => 0,
            'sequence'      => 3
        ]);

        /**
         * BANNERS
         */
        Module::create([
            'name'          => 'banners',
            'display_name'  => 'Beheer banners',
            'config'        => Config::get('modules.banners'),
            'active'        => 0,
            'sequence'      => 4
        ]);

        /**
         * PRODUCTS
         */
        Module::create([
            'name'          => 'categories',
            'display_name'  => 'Product categorieen',
            'config'        => Config::get('modules.categories'),
            'active'        => 0,
            'sequence'      => 5
        ]);

        Module::create([
            'name'          => 'products',
            'display_name'  => 'Producten',
            'config'        => Config::get('modules.products'),
            'active'        => 0,
            'sequence'      => 6
        ]);

        /**
         * PROJECTS
         */
        Module::create([
            'name'          => 'projectgroups',
            'display_name'  => 'Projectgroepen',
            'config'        => Config::get('modules.projectgroups'),
            'active'        => 0,
            'sequence'      => 7
        ]);

        Module::create([
            'name'          => 'projects',
            'display_name'  => 'Projecten',
            'config'        => Config::get('modules.projects'),
            'active'        => 0,
            'sequence'      => 8
        ]);

        Module::create([
            'name'          => 'news',
            'display_name'  => 'Nieuws',
            'config'        => Config::get('modules.news'),
            'active'        => 0,
            'sequence'      => 9
        ]);
		/**
         * referenties
         */
        Module::create([
            'name'          => 'referenties',
            'display_name'  => 'Beheer referenties',
            'config'        => Config::get('modules.referenties'),
            'active'        => 0,
            'sequence'      => 4
        ]);
    }
}
