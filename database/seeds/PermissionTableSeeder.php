<?php

use App\Models\Permission;
use Illuminate\Database\Seeder;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $count = 0;

        /**
         * Permissions
         */
        Permission::create([
            'name'          => 'access-permission',
            'display_name'  => 'Rechten beheren',
            'description'   => '',
            'sequence'      => $count++
        ]);

        Permission::create([
            'name'          => 'create-permission',
            'display_name'  => 'Rechten toevoegen',
            'description'   => '',
            'sequence'      => $count++
        ]);

        Permission::create([
            'name'          => 'edit-permission',
            'display_name'  => 'Rechten bewerken',
            'description'   => '',
            'sequence'      => $count++
        ]);

        Permission::create([
            'name'          => 'delete-permission',
            'display_name'  => 'Rechten verwijderen',
            'description'   => '',
            'sequence'      => $count++
        ]);

        /**
         * Roles
         */
        Permission::create([
            'name'          => 'access-role',
            'display_name'  => 'Rollen beheren',
            'description'   => '',
            'sequence'      => $count++
        ]);

        Permission::create([
            'name'          => 'create-role',
            'display_name'  => 'Rollen toevoegen',
            'description'   => '',
            'sequence'      => $count++
        ]);

        Permission::create([
            'name'          => 'edit-role',
            'display_name'  => 'Rollen bewerken',
            'description'   => '',
            'sequence'      => $count++
        ]);

        Permission::create([
            'name'          => 'delete-role',
            'display_name'  => 'Rollen verwijderen',
            'description'   => '',
            'sequence'      => $count++
        ]);

        /**
         * Users
         */
        Permission::create([
            'name'          => 'access-user',
            'display_name'  => 'Gebruikers beheren',
            'description'   => '',
            'sequence'      => $count++
        ]);

        Permission::create([
            'name'          => 'create-user',
            'display_name'  => 'Gebruikers toevoegen',
            'description'   => '',
            'sequence'      => $count++
        ]);

        Permission::create([
            'name'          => 'edit-user',
            'display_name'  => 'Gebruikers bewerken',
            'description'   => '',
            'sequence'      => $count++
        ]);

        Permission::create([
            'name'          => 'delete-user',
            'display_name'  => 'Gebruikers verwijderen',
            'description'   => '',
            'sequence'      => $count++
        ]);

        /**
         * Pages
         */
        Permission::create([
            'name'          => 'access-page',
            'display_name'  => 'Pagina\'s beheren',
            'description'   => '',
            'sequence'      => $count++
        ]);

        Permission::create([
            'name'          => 'create-page',
            'display_name'  => 'Pagina\'s toevoegen',
            'description'   => '',
            'sequence'      => $count++
        ]);

        Permission::create([
            'name'          => 'edit-page',
            'display_name'  => 'Pagina\'s bewerken',
            'description'   => '',
            'sequence'      => $count++
        ]);

        Permission::create([
            'name'          => 'delete-page',
            'display_name'  => 'Pagina\'s verwijderen',
            'description'   => '',
            'sequence'      => $count++
        ]);

        /**
         * Subpages
         */
        Permission::create([
            'name'          => 'access-subpage',
            'display_name'  => 'Subpagina\'s beheren',
            'description'   => '',
            'sequence'      => $count++
        ]);

        Permission::create([
            'name'          => 'create-subpage',
            'display_name'  => 'Subpagina\'s toevoegen',
            'description'   => '',
            'sequence'      => $count++
        ]);

        Permission::create([
            'name'          => 'edit-subpage',
            'display_name'  => 'Subpagina\'s bewerken',
            'description'   => '',
            'sequence'      => $count++
        ]);

        Permission::create([
            'name'          => 'delete-subpage',
            'display_name'  => 'Subpagina\'s verwijderen',
            'description'   => '',
            'sequence'      => $count++
        ]);

        /**
         * Static Blocks
         */
        Permission::create([
            'name'          => 'access-block',
            'display_name'  => 'Statische blokken beheren',
            'description'   => '',
            'sequence'      => $count++
        ]);

        Permission::create([
            'name'          => 'create-block',
            'display_name'  => 'Statische blokken toevoegen',
            'description'   => '',
            'sequence'      => $count++
        ]);

        Permission::create([
            'name'          => 'edit-block',
            'display_name'  => 'Statische blokken bewerken',
            'description'   => '',
            'sequence'      => $count++
        ]);

        Permission::create([
            'name'          => 'delete-block',
            'display_name'  => 'Statische blokken verwijderen',
            'description'   => '',
            'sequence'      => $count++
        ]);

        /**
         * Banners
         */
        Permission::create([
            'name'          => 'access-banner',
            'display_name'  => 'Banners beheren',
            'description'   => '',
            'sequence'      => $count++
        ]);

        Permission::create([
            'name'          => 'create-banner',
            'display_name'  => 'Banners toevoegen',
            'description'   => '',
            'sequence'      => $count++
        ]);

        Permission::create([
            'name'          => 'edit-banner',
            'display_name'  => 'Banners bewerken',
            'description'   => '',
            'sequence'      => $count++
        ]);

        Permission::create([
            'name'          => 'delete-banner',
            'display_name'  => 'Banners verwijderen',
            'description'   => '',
            'sequence'      => $count++
        ]);

        /**
         * Product categories
         */
        Permission::create([
            'name'          => 'access-category',
            'display_name'  => 'Product categorieen beheren',
            'description'   => '',
            'sequence'      => $count++
        ]);

        Permission::create([
            'name'          => 'create-category',
            'display_name'  => 'Product categorieen toevoegen',
            'description'   => '',
            'sequence'      => $count++
        ]);

        Permission::create([
            'name'          => 'edit-category',
            'display_name'  => 'Product categorieen bewerken',
            'description'   => '',
            'sequence'      => $count++
        ]);

        Permission::create([
            'name'          => 'delete-category',
            'display_name'  => 'Product categorieen verwijderen',
            'description'   => '',
            'sequence'      => $count++
        ]);

        /**
         * Products
         */
        Permission::create([
            'name'          => 'access-product',
            'display_name'  => 'Producten beheren',
            'description'   => '',
            'sequence'      => $count++
        ]);

        Permission::create([
            'name'          => 'create-product',
            'display_name'  => 'Producten toevoegen',
            'description'   => '',
            'sequence'      => $count++
        ]);

        Permission::create([
            'name'          => 'edit-product',
            'display_name'  => 'Producten bewerken',
            'description'   => '',
            'sequence'      => $count++
        ]);

        Permission::create([
            'name'          => 'delete-product',
            'display_name'  => 'Producten verwijderen',
            'description'   => '',
            'sequence'      => $count++
        ]);

        /**
         * Attribute
         */
        Permission::create([
            'name'          => 'access-attribute',
            'display_name'  => 'Attributen beheren',
            'description'   => '',
            'sequence'      => $count++
        ]);

        Permission::create([
            'name'          => 'create-attribute',
            'display_name'  => 'Attribuut toevoegen',
            'description'   => '',
            'sequence'      => $count++
        ]);

        Permission::create([
            'name'          => 'edit-attribute',
            'display_name'  => 'Attribuut bewerken',
            'description'   => '',
            'sequence'      => $count++
        ]);

        Permission::create([
            'name'          => 'delete-attribute',
            'display_name'  => 'Attributen verwijderen',
            'description'   => '',
            'sequence'      => $count++
        ]);

        /**
         * Attribute Set
         */
        Permission::create([
            'name'          => 'access-attributeset',
            'display_name'  => 'Attributen set beheren',
            'description'   => '',
            'sequence'      => $count++
        ]);

        Permission::create([
            'name'          => 'create-attributeset',
            'display_name'  => 'Attributen set toevoegen',
            'description'   => '',
            'sequence'      => $count++
        ]);

        Permission::create([
            'name'          => 'edit-attributeset',
            'display_name'  => 'Attributen set bewerken',
            'description'   => '',
            'sequence'      => $count++
        ]);

        Permission::create([
            'name'          => 'delete-attributeset',
            'display_name'  => 'Attributen set verwijderen',
            'description'   => '',
            'sequence'      => $count++
        ]);

        /**
         * Projectgroups
         */
        Permission::create([
            'name'          => 'access-projectgroup',
            'display_name'  => 'Projectgroepen beheren',
            'description'   => '',
            'sequence'      => $count++
        ]);

        Permission::create([
            'name'          => 'create-projectgroup',
            'display_name'  => 'Projectgroepen toevoegen',
            'description'   => '',
            'sequence'      => $count++
        ]);

        Permission::create([
            'name'          => 'edit-projectgroup',
            'display_name'  => 'Projectgroepen bewerken',
            'description'   => '',
            'sequence'      => $count++
        ]);

        Permission::create([
            'name'          => 'delete-projectgroup',
            'display_name'  => 'Projectgroepen verwijderen',
            'description'   => '',
            'sequence'      => $count++
        ]);

        /**
         * Projects
         */
        Permission::create([
            'name'          => 'access-project',
            'display_name'  => 'Projecten beheren',
            'description'   => '',
            'sequence'      => $count++
        ]);

        Permission::create([
            'name'          => 'create-project',
            'display_name'  => 'Projecten toevoegen',
            'description'   => '',
            'sequence'      => $count++
        ]);

        Permission::create([
            'name'          => 'edit-project',
            'display_name'  => 'Projecten bewerken',
            'description'   => '',
            'sequence'      => $count++
        ]);

        Permission::create([
            'name'          => 'delete-project',
            'display_name'  => 'Projecten verwijderen',
            'description'   => '',
            'sequence'      => $count++
        ]);

        /**
         * News
         */
        Permission::create([
            'name'          => 'access-news',
            'display_name'  => 'Nieuws beheren',
            'description'   => '',
            'sequence'      => $count++
        ]);

        Permission::create([
            'name'          => 'create-news',
            'display_name'  => 'Nieuws toevoegen',
            'description'   => '',
            'sequence'      => $count++
        ]);

        Permission::create([
            'name'          => 'edit-news',
            'display_name'  => 'Nieuws bewerken',
            'description'   => '',
            'sequence'      => $count++
        ]);

        Permission::create([
            'name'          => 'delete-news',
            'display_name'  => 'Nieuws verwijderen',
            'description'   => '',
            'sequence'      => $count++
        ]);

        /**
         * Settings
         */
        Permission::create([
            'name'          => 'access-setting',
            'display_name'  => 'Instellingen beheren',
            'description'   => '',
            'sequence'      => $count++
        ]);

        Permission::create([
            'name'          => 'edit-setting',
            'display_name'  => 'Instellingen bewerken',
            'description'   => '',
            'sequence'      => $count++
        ]);

        /**
         * Admin panel
         */
        Permission::create([
            'name'          => 'reset-admin-password',
            'display_name'  => 'Herstel admin wachtwoord ',
            'description'   => '',
            'sequence'      => $count++
        ]);
    }
}
