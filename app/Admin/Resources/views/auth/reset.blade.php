@extends('cms.layouts.auth')

@section('title', 'Wachtwoord vergeten')

@section('content')
    <div class="dark bg-auto w-full">
        <div class="fade-in-right-big smooth pos-rlt">
            <div class="center-block w-xxl w-auto-xs p-y-md">
                <div class="navbar">
                    <div class="pull-center">
                        @include('admin::blocks.navbar-brand')
                    </div>
                </div>
                <div class="p-a-md box-color r box-shadow-z1 text-color m-a">
                    <div class="m-b">
                        Wijzig je wachtwoord.
                        <p class="text-xs m-t">Vul in de onderstaande velden een nieuw wachtwoord in en klik op wijzigen om te bevestigen.</p>
                    </div>
                    {!! Form::open(['url' => url('cms/password/reset')]) !!}
                    <div class="md-form-group">
                        {!! Form::label('password', 'Nieuw wachtwoord') !!}
                        {!! Form::password('password', ['class' => 'md-input', 'required' => 'required']) !!}
                    </div>
                    <div class="md-form-group">
                        {!! Form::label('password_confirmation', 'Herhaal nieuw wachtwoord') !!}
                        {!! Form::password('password_confirmation', ['class' => 'md-input', 'required' => 'required']) !!}
                    </div>
                    {!! Form::hidden('token', $token) !!}
                    {!! Form::submit('Wijzigen', ['class' => 'btn primary btn-block p-x-md']) !!}
                    {!! Form::close() !!}
                </div>
                <p id="alerts-container"></p>
                <div class="p-v-lg text-center">Terug naar <a ui-sref="access.signin" href="{{ url('cms/login') }}" class="text-primary _600">Log in</a></div>
            </div>
        </div>
    </div>
@endsection