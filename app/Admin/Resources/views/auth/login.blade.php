@extends('admin::layouts.auth')

@section('title', 'Login')

@section('content')
    <div class="dark bg-auto w-full">
        <div class="fade-in-right-big smooth pos-rlt">
            <div class="center-block w-xxl w-auto-xs p-y-md">
                <div class="navbar">
                    <div class="pull-center">
                        @include('admin::blocks.navbar-brand')
                    </div>
                </div>
                <div class="p-a-md box-color r box-shadow-z1 text-color m-a">
                    <div class="m-b text-sm">
                        Log in met je cms account
                    </div>

                    @include('admin::common.errors')

                    {!! Form::open(['url' => 'cms/login', 'class' => 'form-horizontal', 'name' => 'form']) !!}
                    <div class="md-form-group">
                        {!! Form::label('email', 'E-mailadres') !!}
                        {!! Form::text('email', Input::old('email'), ['id' => 'email', 'class' => 'md-input', 'required' => 'required']) !!}
                    </div>
                    <div class="md-form-group">
                        {!! Form::label('password', 'Wachtwoord') !!}
                        {!! Form::password('password', ['id' => 'password', 'class' => 'md-input', 'required' => 'required']) !!}
                    </div>
                    <div class="m-b-md">
                        <label class="md-check">
                            {!! Form::checkbox('remember', 1) !!}<i class="primary"></i> Hou me ingelogd
                        </label>
                    </div>
                    {!! Form::submit('Log in', ['class' => 'btn primary btn-block p-x-md']) !!}
                    {!! Form::close() !!}
                </div>
                <div class="p-v-lg text-center">
                    <div class="m-b"><a href="{{ url('cms/password/email') }}" class="text-primary _600">Wachtwoord vergeten?</a></div>
                </div>
            </div>
        </div>
    </div>
@endsection