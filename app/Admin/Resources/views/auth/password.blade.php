@extends('admin::layouts.auth')

@section('title', 'Wachtwoord vergeten')

@section('content')
    <div class="dark bg-auto w-full">
        <div class="fade-in-right-big smooth pos-rlt">
            <div class="center-block w-xxl w-auto-xs p-y-md">
                <div class="navbar">
                    <div class="pull-center">
                        @include('admin::blocks.navbar-brand')
                    </div>
                </div>
                <div class="p-a-md box-color r box-shadow-z1 text-color m-a">
                    <div class="m-b">
                        Wachtwoord vergeten?
                        <p class="text-xs m-t">Vul je e-mailadres in en we sturen je de instructies om je wachtwoord te herstellen.</p>
                    </div>
                    {!! Form::open(['name' => 'reset']) !!}
                    <form name="reset">
                        <div class="md-form-group">
                            {!! Form::label('email', 'E-mailadres') !!}
                            {!! Form::text('email', Input::old('email'), ['id' => 'email', 'class' => 'md-input', 'required' => 'required']) !!}
                        </div>
                    {!! Form::submit('Versturen', ['class' => 'btn primary btn-block p-x-md']) !!}
                    {!! Form::close() !!}
                </div>
                <p id="alerts-container"></p>
                <div class="p-v-lg text-center">Terug naar <a ui-sref="access.signin" href="{{ url('cms/login') }}" class="text-primary _600">Log in</a></div>
            </div>
        </div>
    </div>
@endsection