<!-- brand -->
<a class="navbar-brand" href="{{ url('cms/dashboard') }}">
    <img src="{{ asset('/admin/assets/images/logo.png') }}" alt="Logo Cees & Co">
    <span class="hidden-folded inline">CMS</span>
</a>
<!-- / brand -->