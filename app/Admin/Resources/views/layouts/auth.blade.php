<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>@yield('title') - Beheersysteem</title>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="robots" content="noindex, nofollow" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimal-ui" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- for ios 7 style, multi-resolution icon of 152x152 -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-barstyle" content="black-translucent">
    <link rel="apple-touch-icon" href="http://localhost:8000/cms/assets/images/logo.png">
    <meta name="apple-mobile-web-app-title" content="Flatkit">
    <!-- for Chrome on Android, multi-resolution icon of 196x196 -->
    <meta name="mobile-web-app-capable" content="yes">
    <link rel="shortcut icon" sizes="196x196" href="http://localhost:8000/cms/assets/images/logo.png">

    <!-- style -->
    <link rel="stylesheet" href="{{ asset('/admin/assets/animate.css/animate.min.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/admin/assets/font-awesome/css/font-awesome.min.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/admin/assets/material-design-icons/material-design-icons.css') }}" type="text/css" />

    <link rel="stylesheet" href="{{ asset('/admin/assets/bootstrap/dist/css/bootstrap.min.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/admin/assets/styles/app.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/admin/assets/styles/font.css') }}" type="text/css" />
</head>
<body>
<div class="app" id="app">
    @yield('content')
</div>
<!-- jQuery -->
<script src="{{ asset('/admin/libs/jquery/dist/jquery.js') }}"></script>
<!-- Bootstrap -->
<script src="{{ asset('/admin/libs/tether/dist/js/tether.min.js') }}"></script>
<script src="{{ asset('/admin/libs/bootstrap/dist/js/bootstrap.js') }}"></script>
</body>
</html>