@extends('admin::layouts.admin')

@section('title', 'Nieuwe taal')

@section('content')
    <div class="padding">
        <div class="row">
            <div class="col-md-12">

                @include('admin::common.errors')

                {!! Form::open(['url' => 'cms/language']) !!}

                <div class="box">
                    <div class="box-header">
                        <h2>Vul de gegevens in</h2>
                    </div>
                    <div class="box-divider m-a-0"></div>
                    <div class="box-body">
                        <div class="form-group">
                            {!! Form::label('name', 'Taal') !!}
                            {!! Form::text('name', Input::old('name'), ['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('code', 'Code') !!}
                            {!! Form::text('code', Input::old('code'), ['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('flag', 'Vlag') !!}
                            <select name="flag" id="flags" class="input-md form-control">
                                <option selected value="nl">NL</option>
                                <option value="gb">EN</option>
                                <option value="es">ES</option>
                                <option value="fr">FR</option>
                                <option value="de">DE</option>
                            </select>
                        </div>
                    </div>
                    <footer class="dker p-a text-right">
                        <a href="{{ url('cms/language') }}" class="btn btn-fw danger">Annuleren</a>
                        {!! Form::submit('Opslaan', ['class' => 'btn btn-fw success']) !!}
                    </footer>
                </div>

                {!! Form::close() !!}

            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script>
        $(function() {
            function formatCountry (country) {
                if (!country.id) return country.text;

                var $country = $(
                    '<span><span class="flag-icon flag-icon-' + country.element.value.toLowerCase() + ' mr-5"></span> ' + country.text + '</span>'
                );
                return $country;
            }

            $("#flags").select2({
                templateResult: formatCountry
            });
        });
    </script>
@endsection