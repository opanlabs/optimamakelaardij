@extends('admin::layouts.admin')

@section('title', 'Bewerk taal')

@section('content')
    <div class="padding">
        <div class="row">
            <div class="col-md-12">

                @include('admin::common.errors')

                {!! Form::model($language, ['route' => ['cms.language.update', $language->id], 'method' => 'PUT', 'role' => 'form']) !!}

                <div class="box">
                    <div class="box-header">
                        <h2>Taal bewerken</h2>
                    </div>
                    <div class="box-divider m-a-0"></div>
                    <div class="box-body">
                        <div class="form-group">
                            {!! Form::label('name', 'Naam') !!}
                            {!! Form::text('name', null, ['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('code', 'Code') !!}
                            {!! Form::text('code', null, ['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('flag', 'Vlag') !!}
                            {!! Form::select('flag', [
                                'nl' => 'NL',
                                'gb' => 'EN',
                                'es' => 'ES',
                                'fr' => 'FR',
                                'de' => 'de'
                            ], null, ['class' => 'form-control', 'id' => 'flags']) !!}
                        </div>
                    </div>
                    <footer class="dker p-a text-right">
                        <a href="{{ url('cms/language') }}" class="btn btn-fw danger">Annuleren</a>
                        {!! Form::submit('Opslaan', ['class' => 'btn btn-fw success']) !!}
                    </footer>
                </div>

                {!! Form::close() !!}

            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script>
        $(function() {
            function formatCountry (country) {
                if (!country.id) return country.text;

                var $country = $(
                        '<span><span class="flag-icon flag-icon-' + country.element.value.toLowerCase() + ' mr-5"></span> ' + country.text + '</span>'
                );
                return $country;
            }

            $("#flags").select2({
                templateResult: formatCountry
            });
        });
    </script>
@endsection