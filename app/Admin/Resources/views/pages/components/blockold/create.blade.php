@extends('admin::layouts.admin')

@section('title', 'Nieuw statisch blok')

@section('content')
    <div class="padding">
        <div class="row">
            <div class="col-md-12">

                @include('admin::common.errors')

                {!! Form::open(['url' => 'cms/block', 'role' => 'form']) !!}

                <div class="box">
                    <div class="box-header">
                        <h2>Vul de gegevens in</h2>
                    </div>
                    <div class="box-divider m-a-0"></div>
                    <div class="box-body">
                        <div class="form-group">
                            {!! Form::label('title', 'Titel') !!}
                            {!! Form::text('title', Input::old('title'), ['class' => 'form-control']) !!}
                        </div>
                        
						 <div class="form-group">
                            {!! Form::label('slug', 'Slug') !!}
                            {!! Form::text('slug', Input::old('Slug'), ['class' => 'form-control']) !!}
                        </div>
						 <div class="form-group">
                            {!! Form::label('button_link', 'Link') !!}
                            {!! Form::text('button_link', Input::old('button_link'), ['class' => 'form-control']) !!}
                        </div>
						<div class="form-group">
                            {!! Form::label('button_text', 'Text Link') !!}
                            {!! Form::text('button_text',Input::old('button_text'), ['class' => 'form-control']) !!}
                        </div>
                    </div>
                    <footer class="dker p-a text-right">
                        <a href="{{ url('cms/block') }}" class="btn btn-fw danger">Annuleren</a>
                        {!! Form::submit('Opslaan', ['class' => 'btn btn-fw success']) !!}
                    </footer>
                </div>

                {!! Form::close() !!}

            </div>
        </div>
    </div>
@endsection