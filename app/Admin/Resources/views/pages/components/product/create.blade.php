@extends('admin::layouts.admin')

@section('title', 'Nieuw product')

@section('content')
    <div class="padding">
        <div class="row">
            <div class="col-md-12">

                @include('admin::common.errors')

                {!! Form::open(['url' => 'cms/product', 'role' => 'form']) !!}

                <div class="box">
                    <div class="box-header">
                        <h2>Vul de gegevens in</h2>
                    </div>
                    <div class="box-divider m-a-0"></div>
                    <div class="box-body">
                        <div class="form-group">
                            {!! Form::label('attribute_set_id', 'Attributen set') !!}
                            {!! Form::select('attribute_set_id', $sets, Input::old('attribute_set_id'), ['id' => 'attribute-set', 'class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('categories', 'Categorieën') !!}
                            {!! Form::select('categories[]', $categories, Input::old('categories') ? Input::old('categories') : $category, ['id' => 'categories', 'multiple' => 'multiple', 'class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('name', 'Naam') !!}
                            {!! Form::text('name', Input::old('name'), ['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('intro', 'Korte tekst') !!}
                            {!! Form::textarea('intro', Input::old('intro'), ['class' => 'ckeditor']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('text', 'Tekst') !!}
                            {!! Form::textarea('text', Input::old('text'), ['class' => 'ckeditor']) !!}
                        </div>
						
                        <div class="form-group">
                            {!! Form::label('price', 'Prijs') !!}
                            {!! Form::text('price', Input::old('price'), ['class' => 'form-control']) !!}
                        </div>
						
                        <div class="form-group">
                            {!! Form::label('meta_title', 'Meta titel') !!}
                            {!! Form::text('meta_title', Input::old('meta_title'), ['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('meta_description', 'Meta omschrijving') !!}
                            {!! Form::text('meta_description', Input::old('meta_description'), ['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('meta_keywords', 'Meta steekwoorden') !!}
                            {!! Form::text('meta_keywords', Input::old('meta_keywords'), ['class' => 'form-control']) !!}
                        </div>
                    </div>
                    <footer class="dker p-a text-right">
                        <a href="{{ url('cms/product') }}" class="btn btn-fw danger">Annuleren</a>
                        {!! Form::submit('Opslaan', ['class' => 'btn btn-fw success']) !!}
                    </footer>
                </div>

                {!! Form::close() !!}

            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script>
        $("#attribute-set").select2();
        $("#categories").select2();
    </script>
@endsection