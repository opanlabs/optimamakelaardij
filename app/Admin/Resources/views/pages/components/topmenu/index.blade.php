@extends('admin::layouts.admin')

@section('title', 'Nieuws')

@section('content')
    <div class="padding">
        <!--<div class="row m-b">
            <div class="col-sm-12 m-b-sm">
                @if (Auth::user()->hasRole('superadmin') || Auth::user()->can('create-topmenu'))
                    <a href="{{ url('cms/topmenu/create') }}" class="btn btn-info">
                        <i class="fa fa-plus"></i> Nieuw nieuwsitem
                    </a>
                @endif
            </div>
        </div> -->
        <div class="box">
            <div class="box-header">
                <h2>Top Menu</h2>
            </div>
            <div class="table-responsive">
                <table id="news-table" class="table table-striped b-t b-b" width="100%">
                    <thead>
                    <tr>
                        <th style="width:10%">Link</th>
                        <th style="width:60%">Name</th>
                        <th style="width:10%"></th>
                       
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script>
        var oTable = $('#news-table').DataTable({
            stateSave: true,
            processing: true,
            serverSide: true,
            rowReorder: {
                dataSrc: 'sequence'
            },
            ajax: {
                url: '{!! url('cms/topmenu/data') !!}',
                type: 'POST',
                data: { _token: '{!! csrf_token() !!}' }
            },
            columns: [
               
                {data: 'link', name: 'link'},
				 {data: 'name', name: 'name'},
                {data: 'edit', name: 'edit', orderable: false, searchable: false},
                
            ],
            language: {
                url: '{{ asset('/admin/localization/nl/datatable.json') }}'
            }
        });

        oTable.on('row-reorder', function (e, diff, edit) {
            var order = [];

            for ( var i=0, ien=diff.length ; i<ien ; i++ ) {
                var rowData = oTable.row( diff[i].node ).data();

                order.push([
                    rowData.id,
                    diff[i].newData
                ]);
            }

            $.ajax({
                url: '{!! url('cms/topmenu/reorder') !!}',
                type: 'POST',
                data: {
                    _token: '{!! csrf_token() !!}',
                    order: order
                }
            }).done(function(data) {
                oTable.draw(false);
            });
        });
    </script>
@endsection