@extends('admin::layouts.admin')

@section('title', 'Topmenu bewerken')

@section('content')
    <div class="padding">
        <div class="row">
            <div class="col-md-12">

                @include('admin::common.errors')

                {!! Form::model($topmenu, ['route' => ['cms.topmenu.update', $topmenu->id], 'method' => 'PUT', 'role' => 'form']) !!}

                <div class="box">
                    <div class="box-header">
                        <h2>Inhoud</h2>
                    </div>
                    <div class="box-divider m-a-0"></div>
                    <div class="box-body">
                        <div class="form-group">
                            {!! Form::label('name', 'Name') !!}
                            {!! Form::text('name', $topmenu->name, ['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('link', 'Link') !!}
                            {!! Form::text('link', $topmenu->link, ['class' => 'form-control']) !!}
                        </div>
                        
                    </div>
                    <footer class="dker p-a text-right">
                        <a href="{{ url('cms/news') }}" class="btn btn-fw danger">Annuleren</a>
                        {!! Form::submit('Opslaan', ['class' => 'btn btn-fw success']) !!}
                    </footer>
                </div>

                {!! Form::close() !!}

            </div>
        </div>
    </div>
@endsection
