@extends('admin::layouts.admin')

@section('title', 'Pagina\'s')

@section('content')
    @if ($subpagesEnabled)
        <div class="row-col">
            <div class="col-sm w-lg w-auto-xs light lt bg-auto">
                <div class="p-a">
                    <!-- Parent Tree Structure -->
                    <div id="parent-tree" data-name="page-tree" data-url="{{ url('cms/page/tree') }}" data-move="{!! url('cms/page/move-node') !!}"></div>
                </div>
            </div>
            <div class="col-sm">
                <div class="p-a pos-rlt">
    @endif

    <div class="padding">
        <div class="row m-b">
            <div class="col-sm-12 m-b-sm">
                @if (Auth::user()->hasRole('superadmin') || Auth::user()->can('create-page'))
                    <a href="{{ url('cms/page/create') }}" class="btn btn-info btn-create">
                        <i class="fa fa-plus"></i> Nieuwe pagina
                    </a>
                @endif
            </div>
        </div>
        <div class="box">
            <div class="box-header">
                <h2>Pagina's</h2>
            </div>
            <div class="table-responsive">
                <table id="pages-table" class="table table-striped b-t b-b" width="100%">
                    <thead>
                    <tr>
                        <th style="width:10%">Volgorde</th>
                        <th style="width:60%">Titel</th>
                        <th style="width:10%"></th>
                        <th style="width:10%"></th>
                        <th style="width:10%"></th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

    @if ($subpagesEnabled)
                </div>
            </div>
        </div>
    @endif
@endsection

@section('javascript')
    @if ($subpagesEnabled)
        <script src="{{ asset('/admin/libs/jqtree/js/tree.jquery.js') }}"></script>
        <script src="{{ asset('/admin/scripts/ui-tree.js') }}"></script>
        <script>
            // Create new element
            $(document.body).on('click', '.btn-create', function(e) {
                e.preventDefault();
                if (node = $tree.tree('getSelectedNode')) {
                    location.href = '{{ url('cms/page/create') }}/' + node.id;
                } else {
                    location.href = '{{ url('cms/page/create') }}';
                }
            });
        </script>
    @endif

    <script>
        var oTable = $('#pages-table').DataTable({
            stateSave: true,
            processing: true,
            serverSide: true,
            rowReorder: {
                dataSrc: 'sequence'
            },
            ajax: {
                url: '{!! url('cms/page/data') !!}',
                type: 'POST',
                @if ($subpagesEnabled)
                data: function(d){
                    var node = $tree.tree('getSelectedNode');
                    d._token = '{!! csrf_token() !!}';
                    d.id = node.id;
                },
                @else
                data: { _token: '{!! csrf_token() !!}' },
                @endif
            },
            columns: [
                {data: 'sequence', name: 'sequence'},
                {data: 'title', name: 'title'},
                {data: 'active', name: 'active', orderable: false, searchable: false},
                {data: 'edit', name: 'edit', orderable: false, searchable: false},
                {data: 'delete', name: 'delete', orderable: false, searchable: false}
            ],
            language: {
                url: '{{ asset('/admin/localization/nl/datatable.json') }}'
            }
        });

        oTable.on('row-reorder', function (e, diff, edit) {
            var order = [];

            for ( var i=0, ien=diff.length ; i<ien ; i++ ) {
                var rowData = oTable.row( diff[i].node ).data();

                order.push([
                    rowData.id,
                    diff[i].newData
                ]);
            }

            $.ajax({
                url: '{!! url('cms/page/reorder') !!}',
                type: 'POST',
                data: {
                    _token: '{!! csrf_token() !!}',
                    order: order
                }
            }).done(function(data) {
                oTable.draw(false);
            });
        });
    </script>
@endsection
