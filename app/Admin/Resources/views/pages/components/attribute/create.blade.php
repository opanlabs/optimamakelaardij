@extends('admin::layouts.admin')

@section('title', 'Nieuw attribuut')

@section('content')
    <div class="padding">
        <div class="row">
            <div class="col-md-12">

                @include('admin::common.errors')

                {!! Form::open(['url' => 'cms/attribute', 'role' => 'form']) !!}

                <div class="box">
                    <div class="box-header">
                        <h2>Vul de gegevens in</h2>
                    </div>
                    <div class="box-divider m-a-0"></div>
                    <div class="box-body">
                        <div class="form-group">
                            {!! Form::label('type', 'Type') !!}
                            {!! Form::select('type', [
                                'textfield' => 'Tekstveld',
                                'textarea' => 'Tekstvak',
                                'dropdown' => 'Dropdown'
                            ], Input::old('type'), ['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('name', 'Naam') !!}
                            {!! Form::text('name', Input::old('name'), ['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('code', 'Code') !!}
                            {!! Form::text('code', Input::old('code'), ['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('label', 'Label') !!}
                            {!! Form::text('label', Input::old('label'), ['class' => 'form-control']) !!}
                        </div>
                    </div>
                    <footer class="dker p-a text-right">
                        <a href="{{ url('cms/attribute') }}" class="btn btn-fw danger">Annuleren</a>
                        {!! Form::submit('Opslaan', ['class' => 'btn btn-fw success']) !!}
                    </footer>
                </div>

                {!! Form::close() !!}

            </div>
        </div>
    </div>
@endsection