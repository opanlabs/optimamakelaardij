@extends('admin::layouts.admin')

@section('title', 'Funda bewerken')

@section('content')
    <div class="padding">
        <div class="row">
            <div class="col-md-12">

                @include('admin::common.errors')

                {!! Form::model($funda, ['route' => ['cms.funda.update', $funda->id], 'method' => 'PUT', 'role' => 'form']) !!}

                <div class="box">
                    <div class="box-header">
                        <h2>Inhoud</h2>
                    </div>
                    <div class="box-divider m-a-0"></div>
                    <div class="box-body">
                        <div class="form-group">
                            {!! Form::label('value', 'Value') !!}
                            {!! Form::text('value', $funda->value, ['class' => 'form-control']) !!}
                        </div>
                        
                    </div>
                    <footer class="dker p-a text-right">
                        <a href="{{ url('cms/news') }}" class="btn btn-fw danger">Annuleren</a>
                        {!! Form::submit('Opslaan', ['class' => 'btn btn-fw success']) !!}
                    </footer>
                </div>

                {!! Form::close() !!}

            </div>
        </div>
    </div>
@endsection
