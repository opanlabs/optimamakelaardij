@extends('admin::layouts.admin')

@section('title', 'Nieuw Topmenu')

@section('content')
    <div class="padding">
        <div class="row">
            <div class="col-md-12">

                @include('admin::common.errors')

                {!! Form::open(['url' => 'cms/topmenu', 'role' => 'form']) !!}

                <div class="box">
                    <div class="box-header">
                        <h2>Vul de gegevens in</h2>
                    </div>
                    <div class="box-divider m-a-0"></div>
                    <div class="box-body">
                       
                        <div class="form-group">
                            {!! Form::label('name', 'Name') !!}
                            {!! Form::text('name', Input::old('name'), ['class' => 'form-control']) !!}
                        </div>
                        
                         <div class="form-group">
                            {!! Form::label('name', 'Link') !!}
                            {!! Form::text('link', Input::old('link'), ['class' => 'form-control']) !!}
                        </div>
                       
                    </div>
                    <footer class="dker p-a text-right">
                        <a href="{{ url('cms/product') }}" class="btn btn-fw danger">Annuleren</a>
                        {!! Form::submit('Opslaan', ['class' => 'btn btn-fw success']) !!}
                    </footer>
                </div>

                {!! Form::close() !!}

            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script>
        $("#attribute-set").select2();
        $("#categories").select2();
    </script>
@endsection