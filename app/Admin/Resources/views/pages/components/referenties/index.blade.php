@extends('admin::layouts.admin')

@section('title', 'referenties')

@section('content')
    <div class="padding">
        <div class="row m-b">
            <div class="col-sm-12 m-b-sm">
                @if (Auth::user()->hasRole('superadmin') || Auth::user()->can('create-referenties'))
                    <a href="{{ url('cms/referenties/create') }}" class="btn btn-info">
                        <i class="fa fa-plus"></i> Nieuwe referenties
                    </a>
                @endif
            </div>
        </div>
        <div class="box">
            <div class="box-header">
                <h2>Referenties</h2>
            </div>
            <div class="table-responsive">
                <table id="referenties-table" class="table table-striped b-t b-b" width="100%">
                    <thead>
                    <tr>
                        <th style="width:10%">Volgorde</th>
                        <th style="width:60%">Titel</th>
                        <th style="width:10%"></th>
                        <th style="width:10%"></th>
                        <th style="width:10%"></th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script>
        var oTable = $('#referenties-table').DataTable({
            stateSave: true,
            processing: true,
            serverSide: true,
            rowReorder: {
                dataSrc: 'sequence'
            },
            ajax: {
                url: '{!! url('cms/referenties/data') !!}',
                type: 'POST',
                data: { _token: '{!! csrf_token() !!}' }
            },
            columns: [
                {data: 'sequence', name: 'sequence'},
                {data: 'title', name: 'title'},
                {data: 'active', name: 'active', orderable: false, searchable: false},
                {data: 'edit', name: 'edit', orderable: false, searchable: false},
                {data: 'delete', name: 'delete', orderable: false, searchable: false}
            ],
            language: {
                url: '{{ asset('/admin/localization/nl/datatable.json') }}'
            }
        });

        oTable.on('row-reorder', function (e, diff, edit) {
            var order = [];

            for ( var i=0, ien=diff.length ; i<ien ; i++ ) {
                var rowData = oTable.row( diff[i].node ).data();

                order.push([
                    rowData.id,
                    diff[i].newData
                ]);
            }

            $.ajax({
                url: '{!! url('cms/referenties/reorder') !!}',
                type: 'POST',
                data: {
                    _token: '{!! csrf_token() !!}',
                    order: order
                }
            }).done(function(data) {
                oTable.draw(false);
            });
        });
    </script>
@endsection