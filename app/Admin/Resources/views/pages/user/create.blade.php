@extends('admin::layouts.admin')

@section('title', 'Nieuwe gebruiker')

@section('content')
    <div class="padding">
        <div class="row">
            <div class="col-md-12">

                @include('admin::common.errors')

                {!! Form::open(['url' => 'cms/user', 'role' => 'form']) !!}

                <div class="box">
                    <div class="box-header">
                        <h2>Vul de gegevens in</h2>
                    </div>
                    <div class="box-divider m-a-0"></div>
                    <div class="box-body">
                        <div class="form-group">
                            {!! Form::label('name', 'Voornaam') !!}
                            {!! Form::text('name', Input::old('name'), ['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('surname', 'Achternaam') !!}
                            {!! Form::text('surname', Input::old('surname'), ['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('email', 'E-mailadres') !!}
                            {!! Form::text('email', Input::old('email'), ['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('company', 'Bedrijf') !!}
                            {!! Form::text('company', Input::old('company'), ['class' => 'form-control']) !!}
                        </div>
                        <hr />
                        <div class="form-group">
                            {!! Form::label('password', 'Wachtwoord') !!}
                            {!! Form::password('password', ['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('password_confirmation', 'Herhaal wachtwoord') !!}
                            {!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
                        </div>
                        <hr />
                        <div class="form-group">
                            <div class="table-responsive">
                                {!! Form::label('roles', 'Rollen') !!}
                                {!! Form::select('roles[]', $roles, null, ['id' => 'multi-select', 'multiple' => 'multiple']) !!}
                            </div>
                        </div>
                    </div>
                    <footer class="dker p-a text-right">
                        <a href="{{ url('cms/user') }}" class="btn btn-fw danger">Annuleren</a>
                        {!! Form::submit('Opslaan', ['class' => 'btn btn-fw success']) !!}
                    </footer>
                </div>

                {!! Form::close() !!}

            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script src="{{ asset('/admin/libs/multiselect/jquery.multi-select.js') }}"></script>
    <script>
        $('#multi-select').multiSelect();
    </script>
@endsection