@extends('admin::layouts.admin')

@section('title', 'Configureer module')

@section('content')
    <div class="padding">
        <div class="row">
            <div class="col-md-12">

                {!! Form::model($module, ['route' => ['cms.module.update', $module->id], 'method' => 'PUT', 'role' => 'form']) !!}

                <div class="box">
                    <div class="box-header">
                        <h2>Module configuratie</h2>
                    </div>
                    <div class="box-divider m-a-0"></div>
                    <div class="box-body">
                        <div class="form-group">
                            {!! Form::label('name', 'Naam') !!}
                            {!! Form::text('name', null, ['class' => 'form-control', 'readonly' => true]) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('display_name', 'Weergavenaam') !!}
                            {!! Form::text('display_name', null, ['class' => 'form-control', 'readonly' => true]) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('config', 'Configuratie') !!}
                            {!! Form::textarea('config', json_encode($module->config), ['id' => 'codemirror', 'class' => 'form-control']) !!}
                        </div>
                    </div>
                    <footer class="dker p-a text-right">
                        <a href="{{ url('cms/module') }}" class="btn btn-fw danger">Annuleren</a>
                        {!! Form::submit('Opslaan', ['class' => 'btn btn-fw success']) !!}
                    </footer>
                </div>

                {!! Form::close() !!}

            </div>
        </div>
    </div>
@endsection