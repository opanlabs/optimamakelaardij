@extends('admin::layouts.admin')

@section('title', 'Instellingen')

@section('content')
    <div class="padding">
        <div class="row">
            <div class="col-md-12">

                {!! Form::model($settings, ['url' => 'cms/setting/update', 'method' => 'PUT']) !!}

                <div class="box">
                    <div class="box-header">
                        <h2>Algemeen</h2>
                    </div>
                    <div class="box-divider m-a-0"></div>
                    <div class="box-body">
                        <div class="form-group">
                            {!! Form::label('company', 'Bedrijfsnaam') !!}
                            {!! Form::text('company', (Input::old('company') ? Input::old('company') : $settings["company"]->value), ['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('address', 'Adres') !!}
                            {!! Form::text('address', (Input::old('address') ? Input::old('address') : $settings["address"]->value), ['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('zip_code', 'Postcode') !!}
                            {!! Form::text('zip_code', (Input::old('zip_code') ? Input::old('zip_code') : $settings["zip_code"]->value), ['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('city', 'Plaats') !!}
                            {!! Form::text('city', (Input::old('city') ? Input::old('city') : $settings["city"]->value), ['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('email', 'E-mailadres') !!}
                            {!! Form::text('email', (Input::old('email') ? Input::old('email') : $settings["email"]->value), ['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('phone', 'Telefoonnummer') !!}
                            {!! Form::text('phone', (Input::old('phone') ? Input::old('phone') : $settings["phone"]->value), ['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('mobile', 'Mobiel') !!}
                            {!! Form::text('mobile', (Input::old('mobile') ? Input::old('mobile') : $settings["mobile"]->value), ['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('coc', 'KvK-nummer') !!}
                            {!! Form::text('coc', (Input::old('coc') ? Input::old('coc') : $settings["coc"]->value), ['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('vat', 'BTW-nummer') !!}
                            {!! Form::text('vat', (Input::old('vat') ? Input::old('vat') : $settings["vat"]->value), ['class' => 'form-control']) !!}
                        </div>
                    </div>
                    <footer class="dker p-a text-right">
                        {!! Form::submit('Opslaan', ['class' => 'btn btn-fw success']) !!}
                    </footer>
                </div>

                <div class="box">
                    <div class="box-header">
                        <h2>Google Analytics</h2>
                    </div>
                    <div class="box-divider m-a-0"></div>
                    <div class="box-body">
                        <div class="form-group">
                            {!! Form::label('ga_tracking_id', 'Tracking ID') !!}
                            {!! Form::text('ga_tracking_id', (Input::old('ga_tracking_id') ? Input::old('ga_tracking_id') : $settings["ga_tracking_id"]->value), array('class' => 'form-control')) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('ga_site_id', 'View ID') !!}
                            {!! Form::text('ga_site_id', (Input::old('ga_site_id') ? Input::old('ga_site_id') : $settings["ga_site_id"]->value), array('class' => 'form-control')) !!}
                        </div>
                    </div>
                    <footer class="dker p-a text-right">
                        {!! Form::submit('Opslaan', ['class' => 'btn btn-fw success']) !!}
                    </footer>
                </div>

                <div class="box">
                    <div class="box-header">
                        <h2>Google Maps</h2>
                    </div>
                    <div class="box-divider m-a-0"></div>
                    <div class="box-body">
                        <div class="form-group">
                            {!! Form::label('gm_lat', 'Breedtegraad(lat)') !!}
                            {!! Form::text('gm_lat', (Input::old('gm_lat') ? Input::old('gm_lat') : $settings["gm_lat"]->value), array('class' => 'form-control')) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('gm_long', 'Lengtegraad(long)') !!}
                            {!! Form::text('gm_long', (Input::old('gm_long') ? Input::old('gm_long') : $settings["gm_long"]->value), array('class' => 'form-control')) !!}
                        </div>
                    </div>
                    <footer class="dker p-a text-right">
                        {!! Form::submit('Opslaan', ['class' => 'btn btn-fw success']) !!}
                    </footer>
                </div>

                <div class="box">
                    <div class="box-header">
                        <h2>Social Media</h2>
                    </div>
                    <div class="box-divider m-a-0"></div>
                    <div class="box-body">
                        <div class="form-group">
                            {!! Form::label('facebook', 'Facebook') !!}
                            {!! Form::text('facebook', (Input::old('facebook') ? Input::old('facebook') : $settings["facebook"]->value), array('class' => 'form-control')) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('twitter', 'Twitter') !!}
                            {!! Form::text('twitter', (Input::old('twitter') ? Input::old('twitter') : $settings["twitter"]->value), array('class' => 'form-control')) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('instagram', 'Instagram') !!}
                            {!! Form::text('instagram', (Input::old('instagram') ? Input::old('instagram') : $settings["instagram"]->value), array('class' => 'form-control')) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('linkedin', 'LinkedIn') !!}
                            {!! Form::text('linkedin', (Input::old('linkedin') ? Input::old('linkedin') : $settings["linkedin"]->value), array('class' => 'form-control')) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('google', 'Google+') !!}
                            {!! Form::text('google', (Input::old('google') ? Input::old('google') : $settings["google"]->value), array('class' => 'form-control')) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('youtube', 'YouTube') !!}
                            {!! Form::text('youtube', (Input::old('youtube') ? Input::old('youtube') : $settings["youtube"]->value), array('class' => 'form-control')) !!}
                        </div>
                    </div>
                    <footer class="dker p-a text-right">
                        {!! Form::submit('Opslaan', ['class' => 'btn btn-fw success']) !!}
                    </footer>
                </div>

                <div class="box">
                    <div class="box-header">
                        <h2>Foutmelding(en)</h2>
                    </div>
                    <div class="box-divider m-a-0"></div>
                    <div class="box-body">
                        <div class="form-group">
                            {!! Form::label('message_404', '404 Error') !!}
                            {!! Form::text('message_404', (Input::old('message_404') ? Input::old('message_404') : $settings["message_404"]->value), array('class' => 'form-control')) !!}
                        </div>
                    </div>
                    <footer class="dker p-a text-right">
                        {!! Form::submit('Opslaan', ['class' => 'btn btn-fw success']) !!}
                    </footer>
                </div>

                {!! Form::close() !!}

            </div>
        </div>
@endsection