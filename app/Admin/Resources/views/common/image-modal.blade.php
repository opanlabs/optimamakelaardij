{!! Form::model($image, ['route' => ['cms.image.update', $image->id], 'method' => 'PUT', 'role' => 'form', 'name' => 'edit']) !!}

<div class="modal-header">
    <h5 class="modal-title">{{ $image->original_name }}</h5>
</div>

<div class="modal-body p-lg">
    <div class="form-group">
        {!! Form::label('name', 'Naam') !!}
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('description', 'Tekst') !!}
        {!! Form::textarea('description', null, ['id' => 'modal-ckeditor', 'class' => 'modal-ckeditor']) !!}
    </div>
    @if ($image->imageable instanceof \App\Models\Banner)
        <div class="form-group">
            {!! Form::label('label', 'Label') !!}
            {!! Form::text('label', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('link', 'Link') !!}
            {!! Form::text('link', null, ['class' => 'form-control']) !!}
        </div>
    @endif
</div>
<div class="modal-footer">
    <button type="button" class="btn danger p-x-md" data-dismiss="modal">Annuleren</button>
    {!! Form::submit('Opslaan', ['class' => 'btn success p-x-md', 'data-dissmiss' => 'modal']) !!}
</div>

{!! Form::close() !!}

<script>
    CKEDITOR.replace('modal-ckeditor');
</script>
