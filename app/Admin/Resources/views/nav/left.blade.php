<ul class="nav" ui-nav>
    <li class="nav-header hidden-folded">
        <small class="text-muted">Algemeen</small>
    </li>

    <li class="{{ (Request::is('cms') || Request::is('cms/dashboard')) ? 'active' : '' }}">
        <a href="{{ url('cms/dashboard') }}">
            <span class="nav-icon">
                <i class="fa fa-dashboard fa-fw"></i>
            </span>
            <span class="nav-text">Dashboard</span>
        </a>
    </li>

    <li class="nav-header hidden-folded">
        <small class="text-muted">Website beheerer</small>
    </li>

    @if ((Auth::user()->hasRole('superadmin') or (Auth::user()->can(['access-page', 'access-block']))) and (
        Module::isActive('pages') ||
        Module::isActive('subpages') ||
        Module::isActive('blocks')
    ))
        <li class="{{ Request::is('cms/page*') || Request::is('cms/subpage*') || Request::is('cms/block*') ? 'active' : '' }}">
            <a>
            <span class="nav-caret">
                <i class="fa fa-caret-down"></i>
            </span>
            <span class="nav-icon">
                <i class="fa fa-copy fa-fw"></i>
            </span>
                <span class="nav-text">Pagina's</span>
            </a>
            <ul class="nav-sub">
                @if (Module::isActive('pages') && (Auth::user()->can('access-page') || Auth::user()->hasRole('superadmin')))
                    <li class="{{ Request::is('cms/page*') ? 'active' : '' }}">
                        <a href="{{ url('cms/page') }}"><span class="nav-text">Pagina's</span></a>
                    </li>
                @endif
                @if (Module::isActive('blocks') && (Auth::user()->can('access-block') || Auth::user()->hasRole('superadmin')))
                    <li class="{{ Request::is('cms/block*') ? 'active' : '' }}">
                        <a href="{{ url('cms/block') }}"><span class="nav-text">Statische blokken</span></a>
                    </li>
                @endif
            </ul>
        </li>
    @endif

    @if ((Auth::user()->hasRole('superadmin') or (Auth::user()->can(['access-banner']))) and (
        Module::isActive('banners')
    ))
        <li class="{{ Request::is('cms/banner*') ? 'active' : '' }}">
            <a href="{{ url('cms/banner') }}">
            <span class="nav-icon">
                <i class="fa fa-picture-o fa-fw"></i>
            </span>
                <span class="nav-text">Banners</span>
            </a>
        </li>
    @endif
	
    @if ((Auth::user()->hasRole('superadmin') or (Auth::user()->can(['access-category', 'access-product']))) and (
        Module::isActive('categories') ||
        Module::isActive('products')
    ))
        <li class="{{ Request::is('cms/category*') || Request::is('cms/product*') || Request::is('cms/attributeset*') || Request::is('cms/attribute*') ? 'active' : '' }}">
            <a>
            <span class="nav-caret">
                <i class="fa fa-caret-down"></i>
            </span>
            <span class="nav-icon">
                <i class="fa fa-shopping-cart fa-fw"></i>
            </span>
                <span class="nav-text">Producten</span>
            </a>
            <ul class="nav-sub">
                @if (Module::isActive('categories') && (Auth::user()->can('access-category') || Auth::user()->hasRole('superadmin')))
                    <li class="{{ Request::is('cms/category*') ? 'active' : '' }}">
                        <a href="{{ url('cms/category') }}"><span class="nav-text">Categorie&euml;n</span></a>
                    </li>
                @endif
                @if (Module::isActive('products') && (Auth::user()->can('access-product') || Auth::user()->hasRole('superadmin')))
                    <li class="{{ Request::is('cms/product') || Request::is('cms/product/*') ? 'active' : '' }}">
                        <a href="{{ url('cms/product') }}"><span class="nav-text">Producten</span></a>
                    </li>
                    <li class="{{ Request::is('cms/attributeset') || Request::is('cms/attributeset/*') ? 'active' : '' }}">
                        <a href="{{ url('cms/attributeset') }}"><span class="nav-text">Attributen set</span></a>
                    </li>
                        <li class="{{ Request::is('cms/attribute') || Request::is('cms/attribute/*') ? 'active' : '' }}">
                            <a href="{{ url('cms/attribute') }}"><span class="nav-text">Attributen</span></a>
                        </li>
                @endif
            </ul>
        </li>
    @endif

    @if ((Auth::user()->hasRole('superadmin') or (Auth::user()->can(['access-projectgroup', 'access-project']))) and (
        Module::isActive('projectgroups') ||
        Module::isActive('projects')
    ))
        <li class="{{ Request::is('cms/projectgroup*') || Request::is('cms/project*') ? 'active' : '' }}">
            <a>
            <span class="nav-caret">
                <i class="fa fa-caret-down"></i>
            </span>
            <span class="nav-icon">
                <i class="fa fa-suitcase fa-fw"></i>
            </span>
                <span class="nav-text">Projecten</span>
            </a>
            <ul class="nav-sub">
                @if (Module::isActive('projectgroups') && (Auth::user()->can('access-projectgroup') || Auth::user()->hasRole('superadmin')))
                    <li class="{{ Request::is('cms/projectgroup*') ? 'active' : '' }}">
                        <a href="{{ url('cms/projectgroup') }}"><span class="nav-text">Projectgroepen</span></a>
                    </li>
                @endif
                @if (Module::isActive('projects') && (Auth::user()->can('access-project') || Auth::user()->hasRole('superadmin')))
                    <li class="{{ Request::is('cms/project') || Request::is('cms/project/*') ? 'active' : '' }}">
                        <a href="{{ url('cms/project') }}"><span class="nav-text">Projecten</span></a>
                    </li>
                @endif
            </ul>
        </li>
    @endif

    @if ((Auth::user()->hasRole('superadmin') or (Auth::user()->can(['access-news']))) and (
        Module::isActive('news')
    ))
        <li class="{{ Request::is('cms/news*') ? 'active' : '' }}">
            <a href="{{ url('cms/news') }}">
            <span class="nav-icon">
                <i class="fa fa-newspaper-o fa-fw"></i>
            </span>
                <span class="nav-text">Nieuws</span>
            </a>
        </li>
    @endif
	

    @if (Auth::user()->hasRole('superadmin') or (Auth::user()->can(['access-module', 'access-language', 'access-user', 'access-permission', 'access-role'])))
        <li class="nav-header hidden-folded">
            <small class="text-muted">Configuratie</small>
        </li>
    @endif

    @if (Auth::user()->hasRole('superadmin') or Auth::user()->can('access-module'))
        <li class="{{ Request::is('cms/module*') ? 'active' : '' }}">
            <a href="{{ url('cms/module') }}">
            <span class="nav-icon">
                <i class="fa fa-cubes fa-fw"></i>
            </span>
                <span class="nav-text">Modules</span>
            </a>
        </li>
    @endif

    @if (Auth::user()->hasRole('superadmin') or Auth::user()->can('access-language'))
        <li class="{{ Request::is('cms/language*') ? 'active' : '' }}">
            <a href="{{ url('cms/language') }}">
            <span class="nav-icon">
                <i class="fa fa-flag fa-fw"></i>
            </span>
                <span class="nav-text">Talen</span>
            </a>
        </li>
    @endif

    @if (Auth::user()->hasRole('superadmin') or Auth::user()->can(['access-user', 'access-permission', 'access-role']))
        <li class="{{ Request::is('cms/user*') || Request::is('cms/permission*') || Request::is('cms/role*') ? 'active' : '' }}">
            <a>
            <span class="nav-caret">
                <i class="fa fa-caret-down"></i>
            </span>
            <span class="nav-icon">
                <i class="fa fa-users fa-fw"></i>
            </span>
                <span class="nav-text">Gebruikers</span>
            </a>
            <ul class="nav-sub">
                <li class="{{ Request::is('cms/user*') ? 'active' : '' }}">
                    <a href="{{ url('cms/user') }}"><span class="nav-text">Alle gebruikers</span></a>
                </li>
                <li class="{{ Request::is('cms/permission*') ? 'active' : '' }}">
                    <a href="{{ url('cms/permission') }}"><span class="nav-text">Rechten</span></a>
                </li>
                <li class="{{ Request::is('cms/role*') ? 'active' : '' }}">
                    <a href="{{ url('cms/role') }}"><span class="nav-text">Rollen</span></a>
                </li>
            </ul>
        </li>
        @endif

    <!--li class="nav-header hidden-folded">
        <small class="text-muted">Help</small>
    </li>

    <li class="hidden-folded" ui-sref-active="active">
        <a href="#/app/docs" ui-sref="app.docs">
            <span class="nav-text">Documenten</span>
        </a>
    </li-->
</ul>