<div class="dropdown-menu pull-right dropdown-menu-scale">
    @foreach(Language::getSupportedLocales() as $localeCode => $properties)
        <a class="dropdown-item" href="{{ url('cms/setting/language/' . $localeCode) }}">
            <span class="flag-icon flag-icon-{{ $properties['flag'] }} mr-5"></span> <span>{{ $properties['native'] }}</span>
        </a>
    @endforeach
</div>