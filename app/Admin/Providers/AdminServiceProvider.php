<?php

namespace Admin\Providers;

use Illuminate\Support\ServiceProvider;

class AdminServiceProvider extends ServiceProvider
{
    protected $name = "admin";

    /**
     * Bootstrap any admin services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerViewsPath();
        $this->registerTranslationsPath();
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Register the path for the views
     */
    public function registerViewsPath()
    {
        $this->loadViewsFrom(__DIR__.'/../Resources/views', $this->name);
    }

    /**
     * Register the path for the translations
     */
    public function registerTranslationsPath()
    {
        $this->loadTranslationsFrom(__DIR__.'/../Resources/lang', $this->name);
    }
}