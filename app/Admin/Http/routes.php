<?php

/*
 * Admin Panel Routes...
 */
Route::group(
[
    'prefix' => 'cms',
    'middleware' => ['locale']
],
function ()
{
    /*
     * Authenticated Admin Routes...
     */
    Route::group(
    [
        'middleware' => ['auth']
    ],
    function () {
        Route::get('/', 'DashboardController@index');
        Route::get('dashboard', 'DashboardController@index');

        /*
         * Module Routes...
         */
        Route::resource('module', 'ModuleController', ['except' => ['show']]);
        Route::group(
        [
            'prefix' => 'module'
        ],
        function () {
            Route::post('data', 'ModuleController@data');
            Route::post('reorder', 'ModuleController@reorder');
            Route::get('/{id}/active/{status}', 'ModuleController@status');
        });

        /*
         * User Routes...
         */
        Route::resource('user', 'UserController', ['except' => ['show']]);
        Route::group(
        [
            'prefix' => 'user'
        ],
        function () {
            Route::post('data', 'UserController@data');
            Route::post('reorder', 'UserController@reorder');
            Route::get('/{id}/active/{status}', 'UserController@status');
        });

        /*
         * Permission Routes...
         */
        Route::resource('permission', 'PermissionController', ['except' => ['show']]);
        Route::group(
        [
            'prefix' => 'permission'
        ],
        function () {
            Route::post('data', 'PermissionController@data');
            Route::post('reorder', 'PermissionController@reorder');
            Route::get('/{id}/active/{status}', 'PermissionController@status');
        });

        /*
         * Role Routes...
         */
        Route::resource('role', 'RoleController', ['except' => ['show']]);
        Route::group(
        [
            'prefix' => 'role'
        ],
        function () {
            Route::post('data', 'RoleController@data');
            Route::post('reorder', 'RoleController@reorder');
            Route::get('/{id}/active/{status}', 'RoleController@status');
        });

        /*
         * Language Routes...
         */
        Route::resource('language', 'LanguageController', ['except' => ['show']]);
        Route::group(
        [
            'prefix' => 'language'
        ],
        function () {
            Route::post('data', 'LanguageController@data');
            Route::post('reorder', 'LanguageController@reorder');
            Route::get('/{id}/active/{status}', 'LanguageController@status');
        });

        /*
         * Page Module Routes...
         */
        Route::resource('page', 'PageController', ['except' => ['show']]);
        Route::group(
        [
            'prefix' => 'page'
        ],
        function() {
            Route::get('create/{id}', 'PageController@create');
            Route::post('data', 'PageController@data');
            Route::post('reorder', 'PageController@reorder');
            Route::get('{id}/active/{status}', 'PageController@status');
            Route::get('images/{id}', 'PageController@images');
            Route::get('tree', 'PageController@tree');
        });

        /*
         * Statick Block Module Routes...
         */
        Route::resource('block', 'BlockController', ['except' => ['show']]);
        Route::group(
        [
            'prefix' => 'block'
        ],
        function()
        {
            Route::post('data', 'BlockController@data');
            Route::post('reorder', 'BlockController@reorder');
            Route::get('{id}/active/{status}', 'BlockController@status');
            Route::get('images/{id}', 'BlockController@images');
        });

        /*
         * Category Module Routes...
         */
        Route::resource('category', 'CategoryController', ['except' => ['show']]);
        Route::group(
        [
            'prefix' => 'category'
        ],
        function()
        {
            Route::get('create/{id}', 'CategoryController@create');
            Route::post('data', 'CategoryController@data');
            Route::post('reorder', 'CategoryController@reorder');
            Route::get('{id}/active/{status}', 'CategoryController@status');
            Route::get('images/{id}', 'CategoryController@images');
            Route::get('tree', 'CategoryController@tree');
        });

        /*
         * Product Attributes Routes...
         */
        Route::resource('attributeset', 'AttributeSetController', ['except' => ['show']]);
        Route::group(
        [
            'prefix' => 'attributeset'
        ],
        function()
        {
            Route::post('data', 'AttributeSetController@data');
            Route::post('reorder', 'AttributeSetController@reorder');
        });

        /*
         * Product Attributes Routes...
         */
        Route::resource('attribute', 'AttributeController', ['except' => ['show']]);
        Route::group(
        [
            'prefix' => 'attribute'
        ],
        function()
        {
            Route::post('data', 'AttributeController@data');
            Route::post('reorder', 'AttributeController@reorder');
        });

        /*
         * Product Module Routes...
         */
        Route::resource('product', 'ProductController', ['except' => ['show']]);
        Route::group(
        [
            'prefix' => 'product'
        ],
        function()
        {
            Route::get('create/{id}', 'ProductController@create');
            Route::post('data', 'ProductController@data');
            Route::post('reorder', 'ProductController@reorder');
            Route::get('{id}/active/{status}', 'ProductController@status');
            Route::get('images/{id}', 'ProductController@images');
            Route::get('tree', 'ProductController@tree');
        });

        /*
         * News Module Routes...
         */
        Route::resource('news', 'NewsController', ['except' => ['show']]);
        Route::group(
        [
            'prefix' => 'news'
        ],
        function()
        {
            Route::post('data', 'NewsController@data');
            Route::post('reorder', 'NewsController@reorder');
            Route::get('/{id}/active/{status}', 'NewsController@status');
            Route::get('images/{id}', 'NewsController@images');
        });

        /*
         * Banner Module Routes...
         */
        Route::resource('banner', 'BannerController', ['except' => ['show']]);
        Route::group(
        [
            'prefix' => 'banner'
        ],
        function()
        {
            Route::post('data', 'BannerController@data');
            Route::post('reorder', 'BannerController@reorder');
            Route::get('/{id}/active/{status}', 'BannerController@status');
            Route::get('images/{id}', 'BannerController@images');
        });
		
		 /*
         * Referenties Module Routes...
         */
        Route::resource('referenties', 'ReferentiesController', ['except' => ['show']]);
        Route::group(
        [
            'prefix' => 'referenties'
        ],
        function()
        {
            Route::post('data', 'ReferentiesController@data');
            Route::post('reorder', 'ReferentiesController@reorder');
            Route::get('/{id}/active/{status}', 'ReferentiesController@status');
            Route::get('images/{id}', 'ReferentiesController@images');
        });
		
		/*
         * Topmenu Module Routes...
         */
		/*
        Route::resource('topmenu', 'TopmenuController', ['except' => ['show']]);
        Route::group(
        [
            'prefix' => 'topmenu'
        ],
        function()
        {
            Route::post('data', 'TopmenuController@data');
            Route::post('reorder', 'TopmenuController@reorder');
            Route::get('/{id}/active/{status}', 'TopmenuController@status');
          
        });
		*/
		/*
         * Funda Module Routes...
         */
        Route::resource('funda', 'FundaController', ['except' => ['show']]);
        Route::group(
        [
            'prefix' => 'funda'
        ],
        function()
        {
            Route::post('data', 'FundaController@data');
            Route::post('reorder', 'FundaController@reorder');
            Route::get('/{id}/active/{status}', 'FundaController@status');
          
        });
        /*
         * Setting Routes...
         */
        Route::resource('setting', 'SettingController', ['except' => ['show']]);
        Route::group(
        [
            'prefix' => 'setting'
        ],
        function () {
            Route::get('language/{code}', 'SettingController@setLocale');
            Route::get('default', 'SettingController@defaultSettings');
            Route::get('profile', 'SettingController@profile');
        });

        /*
         * Image Routes...
         */
        Route::resource('image', 'ImageController', ['only' => ['edit', 'update']]);
        Route::group(
        [
            'prefix' => 'image'
        ],
        function() {
            Route::post('upload', 'ImageController@upload');
            Route::post('delete/{id}', 'ImageController@delete');
            Route::post('reorder', 'ImageController@reorder');
        });

        /*
         * API Routes...
         */
        Route::group(
        [
            'prefix' => 'api'
        ],
        function()
        {
            Route::group(
            [
                'prefix' => 'analytics'
            ],
            function()
            {
                Route::get('/', 'DashboardController@getAnalytics');
                Route::get('visitors-and-page-views', 'DashboardController@getVisitorsAndPageViews');
            });
        });
    });

    /*
     * Admin Authentication Routes...
     */
    Route::group(
    [
        'namespace' => 'Auth'
    ],
    function ()
    {
        // Authentication routes...
        Route::get('login', 'AuthController@getLogin');
        Route::post('login', 'AuthController@postLogin');
        Route::get('logout', 'AuthController@getLogout');

        // Password reset link request routes...
        Route::get('password/email', 'PasswordController@getEmail');
        Route::post('password/email', 'PasswordController@postEmail');

        // Password reset routes...
        Route::get('password/reset/{token}', 'PasswordController@getReset');
        Route::post('password/reset', 'PasswordController@postReset');
    });
});
