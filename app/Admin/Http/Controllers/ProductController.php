<?php

namespace Admin\Http\Controllers;

use App\Repositories\Contracts\AttributeRepository;
use App\Repositories\Contracts\AttributeSetRepository;
use App\Repositories\Contracts\CategoryRepository;
use App\Repositories\Contracts\ProductRepository;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Prettus\Validator\Exceptions\ValidatorException;

class ProductController extends Controller
{
    /**
     * @var ProductRepository
     */
    protected $productRepository;

    /**
     * @var CategoryRepository
     */
    protected $categoryRepository;

    /**
     * @var AttributeSetRepository
     */
    protected $attributeSetRepository;

    /**
     * @var AttributeRepository
     */
    protected $attributeRepository;

    public function __construct(ProductRepository $productRepository, CategoryRepository $categoryRepository, AttributeSetRepository $attributeSetRepository, AttributeRepository $attributeRepository)
    {
        $this->productRepository = $productRepository;
        $this->categoryRepository = $categoryRepository;
        $this->attributeSetRepository = $attributeSetRepository;
        $this->attributeRepository = $attributeRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('admin::pages.components.product.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param null $category
     * @return Response
     */
    public function create($category = null)
    {
        return view('admin::pages.components.product.create', [
            'sets' => $this->attributeSetRepository->all()->lists('name', 'id'),
            'category' => $category,
            'categories' => $this->categoryRepository->all()->lists('title', 'id')
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        try {
            $product = $this->productRepository->create(Input::all());

            foreach (Input::get('categories') as $cat) {
                $product->categories()->attach($cat, [
                    'sequence' => $this->categoryRepository->find($cat)->products()->max('sequence') + 1
                ]);
            }
			
			 foreach ($product->attributeSet->attributes as $attr) {
                if ($product->attributes->contains($attr->id)) {
                    $product->attributes()->updateExistingPivot($attr->id, [
                        'value' => Input::get('attribute_set_id')
                    ]);
                } else {
                    $product->attributes()->attach($attr->id, [
                        'value' => Input::get('attribute_set_id')
                    ]);
                }
            }
			
        } catch (ValidatorException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessageBag());
        }

        return redirect()->route('cms.product.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $model = $this->productRepository->with('categories')->find($id);

        if (!$model->translate(Session::get('locale')->code)) {
            $model = $this->productRepository->createTranslation($model, Session::get('locale')->code);
        }
		// $data = ;
		// echo"<pre>";print_r( $model->attributes);die;
        return view('admin::pages.components.product.edit',[
            'product' => $model,
			'attribute' => $this->attributeRepository->all(),
			'attributeset' =>  $this->attributeSetRepository->all()->lists('name', 'id'),
            'category' => $model->categories->lists('id')->toArray(),
            'categories' => $this->categoryRepository->all()->lists('title', 'id'),
            'imageCount' => $model->images->count()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        try {
			// echo"<pre>";print_r(Input::all());die;
            $this->productRepository->update(Input::all(), $id);
            $model = $this->productRepository->find($id);
            $model->categories()->sync(Input::get('categories'));

            foreach ($model->attributeSet->attributes as $attr) {
                if ($model->attributes->contains($attr->id)) {
                    $model->attributes()->updateExistingPivot($attr->id, [
                        'value' => Input::get('attribute.' . $attr->code)
                    ]);
                } else {
                    $model->attributes()->attach($attr->id, [
                        'value' => Input::get('attribute.' . $attr->code)
                    ]);
                }
            }
        } catch(ValidatorException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessageBag());
        }

        return redirect()->route('cms.product.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        // Load the model
        $model = $this->productRepository->find($id);

        // Delete the images
        if (count($model->images)) {
            foreach ($model->images as $image) {
                if ($image->deleteFiles())
                    $image->delete();
            }
        }

        // Delete from database
        $this->productRepository->delete($id);

        return Response::json([
            'status'    => 'success',
            'tree'      => null
        ], 200);
    }

    /**
     * Return list with resource data.
     *
     * @return mixed
     */
    public function data()
    {
        if (!$id = Input::get('id')) return Response::json(['iTotalRecords' => 0, 'iTotalDisplayRecords' => 0, 'aaData' => []], 200);

        return $this->productRepository->with('categories')->getByCategory($id)->getForDatatable(['active', 'edit', 'delete'], true, [], $id);
    }

    /**
     * Update the sequence
     */
    public function reorder()
    {
        $this->productRepository->reorder(Input::get('order'));
    }

    /**
     * Change the activity status
     *
     * @param $id
     * @param $status
     * @return \Illuminate\Http\RedirectResponse
     */
    public function status($id, $status)
    {
        $this->productRepository->activate($status, $id);
        return redirect()->route('cms.product.index');
    }

    /**
     * Receive the image list for this resource
     *
     * @param $id
     * @return mixed
     */
    public function images($id)
    {
        return $this->productRepository->find($id)->images()->orderBy('sequence')->get()->toJson();
    }

    /**
     * Get the tree structure
     *
     * @return mixed
     */
    public function tree()
    {
        return $this->categoryRepository->getForTree();
    }
}