<?php

namespace Admin\Http\Controllers;

use Admin\Facades\Module;
use App\Repositories\Contracts\PageRepository;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Prettus\Validator\Exceptions\ValidatorException;

class PageController extends Controller
{
    /**
     * @var PageRepository
     */
    protected $repository;

    public function __construct(PageRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $subpagesEnabled = Module::isActive('subpages');
        return view('admin::pages.components.page.index', compact('subpagesEnabled'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param null $parentId
     * @return Response
     */
    public function create($parentId = null)
    {
        return view('admin::pages.components.page.create', compact('parentId'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        try {
            if ($parentId = Input::get('parent_id')) {
                Input::merge(['sequence' => $this->repository->findWhere(['parent_id' => $parentId])->max('sequence') + 1]);
                $model = $this->repository->create(Input::all());
                $this->repository->find(Input::get('parent_id'))->children()->save($model);
            } else {
                Input::merge(['sequence' => $this->repository->whereNull('parent_id')->all()->max('sequence') + 1]);
                $this->repository->create(Input::all());
            }
        } catch (ValidatorException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessageBag());
        }

        return redirect()->route('cms.page.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $model = $this->repository->find($id);

        if (!$model->translate(Session::get('locale')->code)) {
            $model = $this->repository->createTranslation($model, Session::get('locale')->code);
        }
		// echo"<pre>";print_r($model->images->count());die;
        return view('admin::pages.components.page.edit', [
            'page' => $model,
            'imageCount' => $model->images->count()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        try {
            $this->repository->update(Input::all(), $id);
        } catch(ValidatorException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessageBag());
        }

        return redirect()->route('cms.page.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        // Load the model
        $model = $this->repository->find($id);

        // Delete the images
        if (count($model->images)) {
            foreach ($model->images as $image) {
                if ($image->deleteFiles())
                    $image->delete();
            }
        }

        // Delete from database
        $this->repository->delete($id);

        return Response::json([
            'status'    => 'success',
            'tree'      => null
        ], 200);
    }

    /**
     * Return list with resource data.
     *
     * @return mixed
     */
    public function data()
    {
        if ($id = Input::get('id')) {
            return $this->repository->getForDatatable(['active', 'edit', 'delete'], true, ['parent_id' => $id]);
        } else {
            return $this->repository->whereNull('parent_id')->getForDatatable(['active', 'edit', 'delete']);
        }
    }

    /**
     * Update the sequence
     */
    public function reorder()
    {
        $this->repository->reorder(Input::get('order'));
    }

    /**
     * Change the activity status
     *
     * @param $id
     * @param $status
     * @return \Illuminate\Http\RedirectResponse
     */
    public function status($id, $status)
    {
        $this->repository->activate($status, $id);
        return redirect()->route('cms.page.index');
    }

    /**
     * Receive the image list for this resource
     *
     * @param $id
     * @return mixed
     */
    public function images($id)
    {
        return $this->repository->find($id)->images()->orderBy('sequence')->get()->toJson();
    }

    /**
     * Get the tree structure
     *
     * @return mixed
     */
    public function tree()
    {
        return $this->repository->getForTree();
    }
}