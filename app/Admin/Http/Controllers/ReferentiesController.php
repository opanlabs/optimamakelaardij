<?php

namespace Admin\Http\Controllers;

use App\Repositories\Contracts\ReferentiesRepository;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Prettus\Validator\Exceptions\ValidatorException;

class ReferentiesController extends Controller
{
    /**
     * @var ReferentiesRepository
     */
    protected $repository;

    public function __construct(ReferentiesRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('admin::pages.components.referenties.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin::pages.components.referenties.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        try {
            Input::merge(['sequence' => $this->repository->all()->max('sequence') + 1,]);
            $this->repository->create(Input::all());
        } catch (ValidatorException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessageBag());
        }

        return redirect()->route('cms.referenties.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
		$model = $this->repository->find($id);
            $imageCount = $model->images->count();
        return view('admin::pages.components.referenties.edit', [
            'referenties' => $model = $this->repository->find($id),
            'imageCount' => $model->images->count()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        try {
            $this->repository->update(Input::all(), $id);
        } catch(ValidatorException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessageBag());
        }

        return redirect()->route('cms.referenties.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        // Load the model
        $model = $this->repository->find($id);

        // Delete the images
        if (count($model->images)) {
            foreach ($model->images as $image) {
                if ($image->deleteFiles())
                    $image->delete();
            }
        }

        // Delete from database
        $this->repository->delete($id);

        return Response::json([
            'status'    => 'success',
            'tree'      => null
        ], 200);
    }

    /**
     * Return list with resource data.
     *
     * @return mixed
     */
    public function data()
    {
        return $this->repository->getForDatatable(['active', 'edit', 'delete']);
    }

    /**
     * Update the sequence
     */
    public function reorder()
    {
        $this->repository->reorder(Input::get('order'));
    }

    /**
     * Change the activity status
     *
     * @param $id
     * @param $status
     * @return \Illuminate\Http\RedirectResponse
     */
    public function status($id, $status)
    {
        $this->repository->activate($status, $id);
        return redirect()->route('cms.referenties.index');
    }

    /**
     * Receive the image list for this resource
     *
     * @param $id
     * @return mixed
     */
    public function images($id)
    {
        return $this->repository->find($id)->images()->orderBy('sequence')->get()->toJson();
    }
}