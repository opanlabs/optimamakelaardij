<?php

namespace Admin\Http\Middleware;

use App\Models\Language;
use Closure;
use Illuminate\Support\Facades\Session;

class Locale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Session::has('locale')) {
            Session::put('locale', Language::whereCode(app()->getLocale())->first());
        }

        if (app()->getLocale() !== Session::get('locale')) app()->setLocale(Session::get('locale')->code);

        return $next($request);
    }
}
