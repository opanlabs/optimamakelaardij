<?php

namespace Admin\Services;

use App\Repositories\Contracts\LanguageRepository;

class Language
{
    /**
     * @var LanguageRepository
     */
    protected $repository;

    /**
     * ModuleService constructor.
     *
     * @param LanguageRepository $repository
     */
    public function __construct(LanguageRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Get the supported locales/languages
     *
     * @return mixed
     */
    public function getSupportedLocales()
    {
        $locales = [];
        $languages = $this->repository->findByField('active', 1);

        foreach ($languages as $lang) {
            $locales[$lang->code] = [
                'name' => $lang->name,
                'code' => $lang->code,
                'flag' => $lang->flag,
                'script' => 'Latn',
                'native' => $lang->translate($lang->code)->name
            ];
        }

        return $locales;
    }
}