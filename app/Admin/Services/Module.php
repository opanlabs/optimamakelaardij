<?php

namespace Admin\Services;

use App\Repositories\Contracts\ModuleRepository;

class Module
{
    /**
     * @var ModuleRepository
     */
    protected $repository;

    /**
     * ModuleService constructor.
     *
     * @param ModuleRepository $repository
     */
    public function __construct(ModuleRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Get a module configuration
     *
     * @param string $name
     * @return mixed
     */
    public function get($name = '')
    {
		// echo $name;
        $module = $this->repository->findWhere(['name' => $name], [ 'config'])->first();
        return $module->config;
    }

    /**
     * Check if a module is active
     *
     * @param string $name
     * @return mixed
     */
    public function isActive($name = '')
    {
		// echo $name.'<br>';die;
        $module = $this->repository->findByField('name', $name, ['active'])->first();
        return $module->active;
    }
}