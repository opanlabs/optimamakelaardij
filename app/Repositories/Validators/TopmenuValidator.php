<?php

namespace App\Repositories\Validators;

use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\LaravelValidator;

class TopmenuValidator extends LaravelValidator
{
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'link'             => 'required',
            'name'              => 'required'
        ],
        ValidatorInterface::RULE_UPDATE => [
            'link'             => 'required',
            'name'              => 'required'
        ]
    ];
}