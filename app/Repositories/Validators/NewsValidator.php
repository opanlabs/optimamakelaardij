<?php

namespace App\Repositories\Validators;

use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\LaravelValidator;

class NewsValidator extends LaravelValidator
{
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'title'             => 'required',
            'text'              => 'required'
        ],
        ValidatorInterface::RULE_UPDATE => [
            'title'             => 'required',
            'text'              => 'required'
        ]
    ];
}