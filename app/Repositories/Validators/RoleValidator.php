<?php

namespace App\Repositories\Validators;

use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\LaravelValidator;

class RoleValidator extends LaravelValidator
{
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'name' => 'required|unique:permissions',
            'display_name' => 'required',
            'description' => 'required',
            'permissions' => 'required'
        ],
        ValidatorInterface::RULE_UPDATE => [
            'name' => 'required|unique:permissions',
            'display_name' => 'required',
            'description' => 'required',
            'permissions' => 'required'
        ]
    ];
}