<?php

namespace App\Repositories\Validators;

use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\LaravelValidator;

class AttributeValidator extends LaravelValidator
{
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'type'             => 'required',
            'name'             => 'required',
            'code'             => 'required',
            'label'            => 'required'
        ],
        ValidatorInterface::RULE_UPDATE => [
            'name'             => 'required',
            'code'             => 'required',
            'label'            => 'required'
        ]
    ];
}