<?php

namespace App\Repositories\Validators;

use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\LaravelValidator;

class FundaValidator extends LaravelValidator
{

     protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'value'             => 'required'
           
        ],
        ValidatorInterface::RULE_UPDATE => [
            'value'             => 'required'
            
        ]
    ];
}
