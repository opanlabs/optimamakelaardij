<?php

namespace App\Repositories\Validators;

use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\LaravelValidator;

class AttributeSetValidator extends LaravelValidator
{
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'name'             => 'required',
            'description'      => 'required',
            'attributes'       => 'required'
        ],
        ValidatorInterface::RULE_UPDATE => [
            'name'             => 'required',
            'description'      => 'required',
            'attributes'       => 'required'
        ]
    ];
}