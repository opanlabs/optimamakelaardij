<?php
namespace App\Repositories\Validators;

use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\LaravelValidator;

class ReferentiesValidator extends LaravelValidator
{
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'title'             => 'required',
            'description'       => 'required'
        ],
        ValidatorInterface::RULE_UPDATE => [
            'title'             => 'required',
            'description'       => 'required'
        ]
    ];
}