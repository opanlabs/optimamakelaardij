<?php

namespace App\Repositories\Validators;

use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\LaravelValidator;

class ProductValidator extends LaravelValidator
{
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'categories'        => 'required',
            'name'              => 'required',
            'intro'             => 'required',
            'text'              => 'required'
        ],
        ValidatorInterface::RULE_UPDATE => [
            'categories'        => 'required',
            'name'              => 'required',
            'intro'             => 'required',
            'text'              => 'required'
        ]
    ];
}