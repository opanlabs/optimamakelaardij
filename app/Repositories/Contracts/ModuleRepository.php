<?php

namespace App\Repositories\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ModuleRepository
 * @package namespace App\Repositories\Contracts;
 */
interface ModuleRepository extends RepositoryInterface
{
    public function reorder($order);

    public function getForDatatable($actions = []);
}
