<?php

namespace App\Repositories\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface LanguageRepository
 * @package namespace App\Repositories\Contracts;
 */
interface LanguageRepository extends RepositoryInterface
{
    public function reorder($order);

    public function getForDatatable($actions = []);

    public function activate($status, $id);
}