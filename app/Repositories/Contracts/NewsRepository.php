<?php

namespace App\Repositories\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

interface NewsRepository extends RepositoryInterface
{
    public function reorder($order);

    public function getForDatatable($actions = []);

    public function activate($status, $id);
}