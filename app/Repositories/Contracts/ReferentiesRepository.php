<?php

namespace App\Repositories\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ReferentiesRepository
 * @package namespace App\Repositories\Contracts;
 */
interface ReferentiesRepository extends RepositoryInterface
{
    //
	public function reorder($order);

    public function getForDatatable($actions = []);

    public function activate($status, $id);
}
