<?php

namespace App\Repositories\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface PermissionRepository
 * @package namespace App\Repositories\Contracts;
 */
interface PermissionRepository extends RepositoryInterface
{
    public function reorder($order);

    public function getForDatatable($actions = []);
}