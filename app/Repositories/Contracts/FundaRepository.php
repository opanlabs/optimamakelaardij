<?php

namespace App\Repositories\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface FundaRepository
 * @package namespace App\Repositories\Contracts;
 */
interface FundaRepository extends RepositoryInterface
{
    //
	 public function reorder($order);

    public function getForDatatable($actions = []);

    public function activate($status, $id);
}
