<?php

namespace App\Repositories\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

interface AttributeRepository extends RepositoryInterface
{
    public function reorder($order);

    public function getForDatatable($actions = []);
}