<?php

namespace App\Repositories\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface RoleRepository
 * @package namespace App\Repositories\Contracts;
 */
interface RoleRepository extends RepositoryInterface
{
    public function reorder($order);

    public function getForDatatable($actions = []);

    public function getForSelect($data, $key = 'id', $orderBy = 'created_at', $sort = 'DECS');
}
