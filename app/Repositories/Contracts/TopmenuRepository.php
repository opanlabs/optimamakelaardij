<?php

namespace App\Repositories\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface TopmenuRepository
 * @package namespace App\Repositories\Contracts;
 */
interface TopmenuRepository extends RepositoryInterface
{
    //
	 public function reorder($order);

    public function getForDatatable($actions = []);

    public function activate($status, $id);

}
