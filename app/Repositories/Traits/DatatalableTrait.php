<?php

namespace App\Repositories\Traits;

use App\Models\Product;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Prettus\Repository\Contracts\CacheableInterface;
use ReflectionClass;
use Yajra\Datatables\Datatables;

trait DatatalableTrait
{
    protected $ajax;

    /**
     * Get the datatable from a collection
     *
     * @param array $actions
     * @param bool $ajax
     * @param array $where
     * @return mixed
     */
    public function getForDatatable($actions = ['active', 'edit', 'delete'], $ajax = true, $where = [], $parent = null)
    {
        $this->ajax = $ajax;

        $this->applyCriteria();
        $this->applyScope();

        if ($this instanceof CacheableInterface) {
            $this->skipCache(true);
        }

        if ($this instanceof Translatable) {
            $this->withTranslations();
        }

        // Apply where
        if (count($where >= 1)) {
            foreach ($where as $field => $value) {
                if ( is_array($value) ) {
                    list($field, $condition, $val) = $value;
                    $this->model = $this->model->where($field, $condition, $val);
                } else {
                    $this->model = $this->model->where($field,'=',$value);
                }
            }

            $results = $this->model->get();
        } else {
            $results = $this->all();
        }

        // Check if every result has a translation available
        if (in_array(\Dimsav\Translatable\Translatable::class, class_uses($this->model))) {
            if (count($results) >= 1) {
                foreach ($results as $result) {
                    if (!$result->hasTranslation($code = Session::get('locale')->code)) {
                        $this->createTranslation($result, $code);
                    }
                }
            }
        }

        //
        if (($this->model->first() instanceof Product) && ($parent != null)) {
            foreach ($results as $res) {
                foreach ($res->categories as $cat) {
                    if ($cat->id == $parent) $res->sequence = $cat->pivot->sequence;
                }
//                $category = $res->categories()->where('category_id', $parent)->first();
//                $res->sequence = $category->pivot->sequence;
            }
        }

        $this->resetModel();

        $datatable = Datatables::of($this->parserResult($results));
        $datatable->setRowId('id');
        if (in_array('active', $actions)) $this->addActiveColumn($datatable);
        if (in_array('edit', $actions)) $this->addEditColumn($datatable);
        if (in_array('edit', $actions)) $this->addDeleteColumn($datatable);

        return $datatable->make(true);
    }

    /**
     * Add enable/disable action column to the datatable
     *
     * @param $datatable
     */
    private function addActiveColumn($datatable)
    {
        $datatable->addColumn('active', function($model)
        {
            $modelShortName = $this->getModelShortName($model);
            $ajaxClass = $this->ajax == true ? 'ajax-active' : '';

            // Check if user can edit
            if (Auth::user()->hasRole('superadmin') || Auth::user()->can('edit-' . $modelShortName)) {
                if ($model->active == 1)
                    return '<a href="' . url('cms/' . $modelShortName . '/' . $model->id . '/active/0') . '" class="label success ' . $ajaxClass . '"><i class="fa fa-eye fa-fw"></i> Ingeschakeld</a>';
                else
                    return '<a href="' . url('cms/' . $modelShortName . '/' . $model->id . '/active/1') . '" class="label disabled ' . $ajaxClass . '"><i class="fa fa-eye-slash fa-fw"></i> Uitgeschakeld</a>';
            }
        });
    }

    /**
     * Add edit action column to the datatable
     *
     * @param $datatable
     */
    private function addEditColumn($datatable)
    {
        $datatable->addColumn('edit', function($model)
        {
            $modelShortName = $this->getModelShortName($model);

            // Check if user can edit
            if (Auth::user()->hasRole('superadmin') || Auth::user()->can('edit-' . $modelShortName)) {
                return '<a href="'. url('cms/' . $modelShortName . '/' . $model->id . '/edit') .'" class="label info"><i class="fa fa-pencil fa-fw"></i> Bewerk</a>';
            }
        });
    }

    /**
     * Add delete action column to the datatable
     *
     * @param $datatable
     */
    private function addDeleteColumn($datatable)
    {
        $datatable->addColumn('delete', function($model)
        {
            $modelShortName = $this->getModelShortName($model);
            $ajaxClass = 'ajax-delete';

            // Check if user can delete
            if (Auth::user()->hasRole('superadmin') || Auth::user()->can('delete-' . $modelShortName)) {
                return '<a href="'. url('cms/' . $modelShortName . '/' . $model->id) .'" class="label danger ' . $ajaxClass . '"><i class="fa fa-times fa-fw"></i> Verwijderen</a>';
            }
        });
    }

    /**
     * Get the short name for a model
     *
     * @param $model
     * @return string
     */
    private function getModelShortName($model)
    {
        $reflect = new ReflectionClass($model);
        return strtolower($reflect->getShortName());
    }
}