<?php

namespace App\Repositories\Traits;

use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Events\RepositoryEntityUpdated;

trait SortableTrait
{
    /**
     * Get the max value in field
     *
     * @param $field
     * @return $this
     */
    public function max($field)
    {
        $this->model = $this->model->max($field);
        return $this;
    }

    /**
     * Get the min value in field
     *
     * @param $field
     * @return $this
     */
    public function min($field)
    {
        $this->model = $this->model->min($field);
        return $this;
    }

    /**
     * Count the records
     *
     * @return $this
     */
    public function count()
    {
        $this->model = $this->model->count();
        return $this;
    }

    /**
     * Find where field is null
     *
     * @param $column
     * @return $this
     */
    public function whereNull($column)
    {
        $this->model = $this->model->whereNull($column);
        return $this;
    }

    /**
     * Find where field is not null
     *
     * @param $column
     * @return $this
     */
    public function whereNotNull($column)
    {
        $this->model = $this->model->whereNotNull($column);
        return $this;
    }

    /**
     * Change the sequence
     *
     * @param $order
     */
    public function reorder($order)
    {
        $nodes = [];
        if (count($order)) {
            // set new positions...
            foreach ($order as $node) {

                if ($this instanceof CacheableInterface) $this->skipCache(true);

                $model = $this->model->findOrFail($node[0]);
                $model->sequence = $node[1];
                $model->save();

                $this->resetModel();

                event(new RepositoryEntityUpdated($this, $model));
            }
        }
    }

    /**
     * Order results by column and direction
     *
     * @param $column
     * @param string $direction
     * @return $this
     */
    public function orderBy($column, $direction = 'asc')
    {
        $this->model = $this->model->orderBy($column, $direction);
        return $this;
    }
}