<?php

namespace App\Repositories\Eloquent;

use App\Repositories\Traits\Activatable;
use App\Repositories\Traits\DatatalableTrait;
use App\Repositories\Traits\SortableTrait;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\Contracts\ModuleRepository;
use App\Models\Module;
use Prettus\Repository\Traits\CacheableRepository;

/**
 * Class ModuleRepositoryEloquent
 * @package namespace App\Repositories\Eloquent;
 */
class ModuleRepositoryEloquent extends BaseRepository implements ModuleRepository, CacheableInterface
{
    use DatatalableTrait, SortableTrait, Activatable, CacheableRepository;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Module::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
