<?php

namespace App\Repositories\Eloquent;

use App\Models\Product;
use App\Repositories\Contracts\ProductRepository;
use App\Repositories\Traits\Activatable;
use App\Repositories\Traits\DatatalableTrait;
use App\Repositories\Traits\Sluggable;
use App\Repositories\Traits\SortableTrait;
use App\Repositories\Traits\Translatable;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Events\RepositoryEntityUpdated;
use Prettus\Repository\Traits\CacheableRepository;

class ProductRepositoryEloquent extends BaseRepository implements ProductRepository
{
    use DatatalableTrait, SortableTrait, Activatable, CacheableRepository, Translatable, Sluggable;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Product::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    /**
     * @return Validator
     */
    public function validator()
    {
        return "App\\Repositories\\Validators\\ProductValidator";
    }

    /**
     * Get the products by category
     *
     * @param $category
     * @return mixed
     */
    public function getByCategory($category)
    {
        $this->model = $this->model->whereHas('categories', function($query) use ($category)
        {
            $query->where('id', '=', $category);
        });

        return $this;
    }

    /**
     * Get the products by category
     *
     * @param $name
     * @param $value
     * @return mixed
     */
    public function getByAttribute($name, $value)
    {
        $this->model = $this->model->whereHas('attributes', function($query) use ($name, $value)
        {
            $query->where('name', '=', $name)->where('attribute_product.value', '=', $value);
        });

        return $this;
    }
	  /**
     * @param $attribute
     * @param $value
     * @param array $columns
     * @return mixed
     */
    public function findBy($attribute, $value, $columns = array('*')) {
        return $this->model->where($attribute, '=', $value)->first($columns);
    }
    /**
     * Change the sequence
     *
     * @param $order
     */
    public function reorder($order)
    {
        $nodes = [];

        if (count($order)) {
            // set new positions...
            foreach ($order as $node) {

                if ($this instanceof CacheableInterface) $this->skipCache(true);

                $model = $this->model->findOrFail($node[1]);
                $model->categories()->updateExistingPivot($node[0], ['sequence' => $node[2]]);

                $this->resetModel();

                event(new RepositoryEntityUpdated($this, $model));
            }
        }
    }
}