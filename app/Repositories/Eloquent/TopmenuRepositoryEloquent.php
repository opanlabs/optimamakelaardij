<?php

namespace App\Repositories\Eloquent;

use App\Models\Topmenu;
use App\Repositories\Contracts\TopmenuRepository;
use App\Repositories\Traits\Activatable;
use App\Repositories\Traits\DatatalableTrait;
use App\Repositories\Traits\Sluggable;
use App\Repositories\Traits\SortableTrait;
use App\Repositories\Traits\Translatable;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Traits\CacheableRepository;

/**
 * Class TopmenuRepositoryEloquent
 * @package namespace App\Repositories\Eloquent;
 */
class TopmenuRepositoryEloquent extends BaseRepository implements TopmenuRepository
{
	 use DatatalableTrait, SortableTrait, Activatable, CacheableRepository, Translatable, Sluggable;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Topmenu::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
	/**
     * @return Validator
     */
    public function validator()
    {
        return "App\\Repositories\\Validators\\TopmenuValidator";
    }
}
