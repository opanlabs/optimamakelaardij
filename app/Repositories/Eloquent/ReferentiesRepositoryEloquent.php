<?php

namespace App\Repositories\Eloquent;

use App\Models\Referenties;
use App\Repositories\Contracts\ReferentiesRepository;
use App\Repositories\Traits\Activatable;
use App\Repositories\Traits\DatatalableTrait;
use App\Repositories\Traits\Sluggable;
use App\Repositories\Traits\SortableTrait;
use App\Repositories\Traits\Translatable;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Traits\CacheableRepository;

/**
 * Class ReferentiesRepositoryEloquent
 * @package namespace App\Repositories\Eloquent;
 */
class ReferentiesRepositoryEloquent extends BaseRepository implements ReferentiesRepository
{
     use DatatalableTrait, SortableTrait, Activatable, CacheableRepository, Translatable, Sluggable;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Referenties::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    /**
     * @return Validator
     */
    public function validator()
    {
        return "App\\Repositories\\Validators\\ReferentiesValidator";
    }
}
