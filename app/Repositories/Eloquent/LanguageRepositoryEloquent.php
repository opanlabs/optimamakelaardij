<?php

namespace App\Repositories\Eloquent;

use App\Models\Language;
use App\Repositories\Contracts\LanguageRepository;
use App\Repositories\Traits\Activatable;
use App\Repositories\Traits\DatatalableTrait;
use App\Repositories\Traits\SortableTrait;
use App\Repositories\Traits\Translatable;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Traits\CacheableRepository;

class LanguageRepositoryEloquent extends BaseRepository implements LanguageRepository, CacheableInterface
{
    use DatatalableTrait, SortableTrait, Activatable, CacheableRepository, Translatable;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Language::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    /**
     * @return Validator
     */
    public function validator()
    {
        return "App\\Repositories\\Validators\\LanguageValidator";
    }
}