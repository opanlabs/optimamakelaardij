<?php

namespace App\Repositories\Eloquent;

use App\Repositories\Traits\Activatable;
use App\Repositories\Traits\DatatalableTrait;
use App\Repositories\Traits\SortableTrait;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\Contracts\UserRepository;
use App\Models\User;

/**
 * Class UserRepositoryEloquent
 * @package namespace App\Repositories\EloquentModels;
 */
class UserRepositoryEloquent extends BaseRepository implements UserRepository
{
    use DatatalableTrait, SortableTrait, Activatable;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return User::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    /**
     * Specify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
        return "App\\Repositories\\Validators\\UserValidator";
    }
}
