<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AttributeSet extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'description'];

    /**
     * The attributes that belong to the set.
     */
    public function attributes()
    {
        return $this->belongsToMany('App\Models\Attribute');
    }

    /**
     * Get the products for the attribute set.
     */
    public function products()
    {
        return $this->hasMany('App\Models\Product');
    }
}
