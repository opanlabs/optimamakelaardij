<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\LogsActivity;
use Spatie\Activitylog\LogsActivityInterface;
use DB;

class Referenties extends Model implements LogsActivityInterface
{
    use LogsActivity;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'description',
        'sequence',
		'star',
        'active'
    ];

    /**
     * Get all the images.
     */
    public function images()
    {
        return $this->morphMany('App\Models\Image', 'imageable');
    }
	
	/**
     * Get all the images.
     */
    public function getAll()
    {
       $data = DB::table('referenties')
				->select(
					'referenties.*',
					'images.imageable_type',
					'images.path'
				)
				->leftJoin('images', 'imageable_id', '=', 'referenties.id')
				->where('images.imageable_type','App\Models\Referenties')
				->where('referenties.active','1')
				->orderBy('referenties.id','DESC')
				->get();

		return $data;

    }

    /**
     * Get the message that needs to be logged for the given event.
     *
     * @param string $eventName
     *
     * @return string
     */
    public function getActivityDescriptionForEvent($eventName)
    {
        if ($eventName == 'created')
        {
            return "Banner({$this->id}) '{$this->title}' was created";
        }

        if ($eventName == 'updated')
        {
            return "Banner({$this->id}) '{$this->title}' was updated";
        }

        if ($eventName == 'deleted')
        {
            return "Banner({$this->id}) '{$this->title}' was deleted";
        }

        return '';
    }

}
