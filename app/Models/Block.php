<?php

namespace App\Models;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\LogsActivity;
use Spatie\Activitylog\LogsActivityInterface;
use DB;

class Block extends Model implements LogsActivityInterface
{
    use Translatable, LogsActivity;

    /**
     * The attributes that are translated
     *
     * @var array
     */
    public $translatedAttributes = ['title', 'slug', 'meta_title', 'meta_description', 'meta_keywords', 'text', 'button_link', 'button_text'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'slug',
        'meta_title',
        'meta_description',
        'meta_keywords',
        'text',
        'button_link',
        'button_text',
        'sequence',
        'active'
    ];

    /**
     * Get all the images.
     */
    public function images()
    {
        return $this->morphMany('App\Models\Image', 'imageable');
    }
	
	/**
     * Get all the Block.
     */
    public function getAll()
    {
        $data =DB::table('blocks')
				->select(
								'blocks.*',
								'block_translations.title',
								'block_translations.slug',
								'block_translations.button_link',
								'block_translations.button_text'
							)
				->join(
							'block_translations',
							'blocks.id',
							'=',
							'block_translations.block_id'
						)
				->where(
							'locale',
							'en'
						);
					$data = $data->get();
			return $data;
    }

    /**
     * Get the message that needs to be logged for the given event.
     *
     * @param string $eventName
     *
     * @return string
     */
    public function getActivityDescriptionForEvent($eventName)
    {
        if ($eventName == 'created')
        {
            return "Block({$this->id}) '{$this->title}' was created";
        }

        if ($eventName == 'updated')
        {
            return "Block({$this->id}) '{$this->title}' was updated";
        }

        if ($eventName == 'deleted')
        {
            return "Block({$this->id}) '{$this->title}' was deleted";
        }

        return '';
    }
}
