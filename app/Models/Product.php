<?php

namespace App\Models;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\LogsActivity;
use Spatie\Activitylog\LogsActivityInterface;
use DB;

class Product extends Model implements LogsActivityInterface
{
    use Translatable, LogsActivity;

    /**
     * The attributes that are translated
     *
     * @var array
     */
    public $translatedAttributes = ['name', 'slug', 'intro', 'text', 'meta_title', 'meta_description', 'meta_keywords'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['attribute_set_id', 'name', 'intro', 'text', 'meta_title', 'meta_description', 'meta_keywords', 'price', 'active'];

    /**
     * Get the attribute set that belongs to the product.
     */
    public function attributeSet()
    {
        return $this->belongsTo('App\Models\AttributeSet');
    }

    /**
     * Get all the images.
     */
    public function images()
    {
        return $this->morphMany('App\Models\Image', 'imageable');
    }

    /**
     * The categories that belong to the product.
     */
    public function categories()
    {
        return $this->belongsToMany('App\Models\Category')->withPivot('sequence');
    }

    /**
     * The attributes that belong to the product.
     */
    public function attributes()
    {
        return $this->belongsToMany('App\Models\Attribute')->withPivot('value');
    }

    /**
     * Get the related attribute value by code
     *
     * @param $name
     * @return null
     */
    public function getAttrValue($name)
    {
        if ($result = $this->attributes()->where('name', '=', $name)->first()) return $result->pivot->value;

        return null;
    }
	
	
	 /**
     * Get the related attribute value by code
     *
     * @param $name
     * @return null
     */
    public function getAttr($name)
    {
        if ($result = $this->attributeSet()->where('id', '=', 3)->first()) return $result;

        return null;
    }

    /**
     * Get the message that needs to be logged for the given event.
     *
     * @param string $eventName
     *
     * @return string
     */
    public function getActivityDescriptionForEvent($eventName)
    {
        if ($eventName == 'created')
        {
            return "Product({$this->id}) '{$this->name}' was created";
        }

        if ($eventName == 'updated')
        {
            return "Product({$this->id}) '{$this->name}' was updated";
        }

        if ($eventName == 'deleted')
        {
            return "Product({$this->id}) '{$this->name}' was deleted";
        }

        return '';
    }

    /**
     * Filter by attribute value
     *
     * @param $query
     * @param $name
     * @param $value
     * @return mixed
     */
    public function scopeByAttribute($query, $name, $value)
    {
        return $query->whereHas('attributes', function($query) use ($name, $value)
        {
            $query->where('name', '=', $name)->where('attribute_product.value', '=', $value);
        });
    }
	/**
     * Get all the Block.
     */
    public function getAll($param)
    {
        $data =DB::table('products')
				->select(
								'products.*',
								'product_translations.name',
								'product_translations.slug',
								'product_translations.intro',
								'product_translations.slug',
								'category_translations.title',
								DB::raw('(SELECT path FROM  images where imageable_id=products.id and imageable_type="App\Models\Product" ) AS path'),
								DB::raw('(SELECT attribute_sets.name FROM  attribute_sets where id=products.attribute_set_id ) AS status')

							)
					->join(
							'product_translations',
							'products.id',
							'=',
							'product_translations.product_id'
						)
					->join(
							'category_product',
							'products.id',
							'=',
							'category_product.product_id'
						)
					->join(
							'category_translations',
							'category_product.category_id',
							'=',
							'category_translations.category_id'
						)
					->where(
							'product_translations.locale',
							'en'
						);
			if(isset($param['limit']))
			{
				$data->limit($param['limit']);
			}
			
			$data->groupBy('products.id');
			$data = $data->get();
			return $data;
    }
	/**
     * Get all the Block.
     */
    public function getBySlug($slug)
    {
		// DB::enableQueryLog();
        $data =DB::table('products')
				->select(
								'products.*',
								'product_translations.name',
								'product_translations.intro',
								'product_translations.slug'
						)
					->join(
							'product_translations',
							'products.id',
							'=',
							'product_translations.product_id'
						)
					->where(
							'product_translations.slug',
							$slug
						)
					->groupBy('products.id');
			
			$data = $data->first();
			// dd(DB::getQueryLog());die;

			return $data;
    }
}
