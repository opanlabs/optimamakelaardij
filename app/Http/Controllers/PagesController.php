<?php

namespace App\Http\Controllers;

use App\Models\Setting;
use App\Models\Product;
use App\Models\Referenties;

use App\Repositories\Contracts\ProductRepository;
use App\Repositories\Contracts\BannerRepository;
use App\Repositories\Contracts\PageRepository;
use App\Repositories\Contracts\BlockRepository;
use App\Repositories\Criteria\IsActive;
use App\Repositories\Criteria\IsRoot;
use Illuminate\Http\Request;

use App\Http\Requests;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class PagesController extends Controller
{
    /**
     * @var static vars
     */
    protected $settings, $navigation, $banner;

    /**
     * @var repository vars
     */
    protected $productRepository,$blockRepository,$bannerRepository,$pageRepository;

    public function __construct(ProductRepository $productRepository, BlockRepository $blockRepository,BannerRepository $bannerRepository,PageRepository $pageRepository) {                
        
      $this->settings = Setting::select('field', 'value')->get()->keyBy('field');
	  $this->product = new Product;
	  $this->productRepository =  $productRepository;
	  $this->blockRepository =  $blockRepository;
	  $this->bannerRepository =  $bannerRepository;
	  $this->pageRepository =  $pageRepository;
	        $this->referenties = new Referenties;

	   $this->blocks           = $this->blockRepository->findWhereIn('id',[5,12,13,14,15,16,17,18,19,20] )->where('active',1);
      $this->submenuAabond           = $this->blockRepository->findWhereIn('id',[6,7,8])->where('active',1);
	  $this->submenuAdvies           = $this->blockRepository->findWhereIn('id',[9,10,11])->where('active',1);
	  $this->navigation = $this->loadNavigationItems();
    }

    /**
     * Load the navigation items
     * @return mixed
     */
    public function loadNavigationItems()
    {
        $items = $this->pageRepository->getByCriteria(new IsActive(), new IsRoot());
        return $items;
    }

    /**
     * Display the home page
     * @return View
     */
    public function home()
    {
		        // $data['referenties'] = $this->referenties->getAll();

		// echo"<pre>";print_r($this->productRepository->paginate(3)->where('active',1)->toArray());die;
        return view('home.home', [
					'settings' 		=> $this->settings,
					'navigation' 	=> $this->navigation,
					'topmenu' 		=> $this->blockRepository->findWhereIn('id',[1,2,3])->where('active',1),
					'products'		=> $this->productRepository->paginate(3)->where('active',1),
					'subPages' 		=> $this->pageRepository->findWhereIn('id',[5,6,7])->where('active',1),
					'referenties'	=> $this->referenties->getAll(),
					'funda' 		=> $this->pageRepository->findByField('id',28)->where('active',1),
					'banner'		=> $this->bannerRepository->findByField('active',1),
					'submenuAabond'	=> $this->submenuAabond ,
					'submenuAdvies'	=> $this->submenuAdvies ,
					'blocks'		=> $this->blocks ,
					'page' 			=> $this->pageRepository->findBySlug('home')
				]);
    }

	/**
     * Display the aanbod page
     * @return View
     */
    public function aanbod()
    {
		$paramProduct['limit'] = 6;
        return view('aanbod.aanbod', [
					'settings' 		=> $this->settings,
					'navigation' 	=> $this->navigation,
					'topmenu' 		=> $this->blockRepository->findWhereIn('id',[1,2,3])->where('active',1),
					'product'		=> $this->productRepository->paginate($paramProduct['limit'])->where('active',1),
					'submenuAabond'	=> $this->submenuAabond ,
					'submenuAdvies'	=> $this->submenuAdvies ,
					'blocks'		=> $this->blocks ,
					'page' 			=> $this->pageRepository->findBySlug('aanbod')
        ]);
    }

	/**
     * Display the diensten page
     * @return View
     */
    public function diensten()
    {
		
        return view('diensten.diensten', [
					'settings' 		=> $this->settings,
					'navigation' 	=> $this->navigation,
					'topmenu' 		=> $this->blockRepository->findWhereIn('id',[1,2,3])->where('active',1),
					'subPages'		=> $this->pageRepository->findByField('parent_id',14)->where('active',1),
					'submenuAabond'	=> $this->submenuAabond ,
					'submenuAdvies'	=> $this->submenuAdvies ,
					'blocks'		=> $this->blocks ,
					'page' 			=> $this->pageRepository->findBySlug('diensten')
        ]);
    }
	
	/**
     * Display the optima page
     * @return View
     */
    public function optima()
    {
		// echo"aaa";die;
		return view('optima.optima', [
					'settings' 		=> $this->settings,
					'navigation' 	=> $this->navigation,
					'topmenu' 		=> $this->blockRepository->findWhereIn('id',[1,2,3])->where('active',1),
					'subPages'		=> $this->pageRepository->findByField('parent_id',8)->where('active',1),
					'submenuAabond'	=> $this->submenuAabond ,
					'submenuAdvies'	=> $this->submenuAdvies ,
					'blocks'		=> $this->blocks ,
					'page' 			=> $this->pageRepository->findBySlug('over-optima')
        ]);
    }
	
	/**
     * Display the objectshow page
     * @return View
     */
    public function objectshow($slug)
    {
		// echo"aaa";die;
		$productSlug = $this->product->getBySlug($slug);
		if($productSlug=="")
		{
			return redirect('/aanbod');
		}
		$product = $this->productRepository->find($productSlug->id);
		$product->images = json_decode($product->images()->orderBy('sequence')->get()->toJson());
		return view('object.object', [
					'settings' 		=> $this->settings,
					'navigation' 	=> $this->navigation,
					'topmenu' 		=> $this->blockRepository->findWhereIn('id',[1,2,3])->where('active',1),
					'submenuAabond'	=> $this->submenuAabond ,
					'submenuAdvies'	=> $this->submenuAdvies ,
					'blocks'		=> $this->blocks ,
					'product'		=> $product
        ]);
    }
	
	
    /**
     * Display a default page
     * @param $slug
     * @return View
     */
    public function defaultPage($slug)
    {
		 return view('pages.default', [
					'settings' 		=> $this->settings,
					'navigation' 	=> $this->navigation,
					'topmenu' 		=> $this->blockRepository->findWhereIn('id',[1,2,3])->where('active',1),
					'products'		=> $this->productRepository->paginate(3)->where('active',1),
					'subPages' 		=> $this->pageRepository->findWhereIn('id',[5,6,7])->where('active',1),
					'banner' 		=> $this->bannerRepository->all(),
					'submenuAabond'	=> $this->submenuAabond ,
					'submenuAdvies'	=> $this->submenuAdvies ,
					'blocks'		=> $this->blocks ,
					'page' 			=> $this->pageRepository->findBySlug($slug)
        ]);
    }

    /**
     * Display the contact page
     * @return View
     */
    public function contact()
    {
        return view('contact.contact', [
					'settings' 		=> $this->settings,
					'navigation' 	=> $this->navigation,
					'subPages'		=> $this->pageRepository->findByField('parent_id',19)->where('active',1),
					'topmenu' 		=> $this->blockRepository->paginate(3)->where('active',1),
					'submenuAabond'	=> $this->submenuAabond ,
					'submenuAdvies'	=> $this->submenuAdvies ,
					'blocks'		=> $this->blocks ,
					'page' 			=> $this->pageRepository->findBySlug('contact')
        ]);
    }

    /**
     * Send the contact form
     * @param Requests\SendContactRequest $request
     * @return Response
     */
    public function sendContact(Request $request)
    {
		$validator = Validator::make(Input::all(), 
			array(
				'email'             => 'required|max:50|email',
				'name'         		=> 'required',
				'onderwerp'          => 'required',
				'phone'   			 => 'required|regex:/[0-9]/',
				
			)
		);


		if($validator->fails()){
			// echo "<pre>";print_r($validator);die;
			return redirect()->back()->withErrors($validator)->withInput(Input::all());;
		} else {

			$config = $this->settings;
			
			$data = [
				'name' => $request->input('name'),
				'email' => $request->input('email'),
				'phone' => $request->input('phone'),
				'onderwerp'=>$request->input('onderwerp'),
				'msg' => $request->input('bericht'),
				'config' => $config
			];
			// echo"<pre>";print_r( $data);die;
			Mail::send('emails.contact', $data, function($message) use ($config) {
				$message->from($config['email']['value'], $config['company']['value']);
				$message->to($config['email']['value'], $config['company']['value']);
				$message->subject('Contactformulier');
			});

			return redirect()->back()->with('success', 'Uw bericht is verstuurd.');
		}
    }
}
