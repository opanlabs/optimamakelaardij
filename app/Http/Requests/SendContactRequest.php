<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class SendContactRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required|email',
            'message' => 'required',
            'g-recaptcha-response' => 'required|recaptcha'
        ];
    }

    /**
     * Error messages
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'Vul je naam in.',
            'email.required' => 'Vul je e-mailadres in.',
            'email.email' => 'Vul een geldig e-mailadres in.',
            'message.required' => 'Vul een vraag of opmerking in.',
            'g-recaptcha-response.required' => 'De reCaptcha is verplicht.',
            'g-recaptcha-response.recaptcha' => 'Verifieer dat u geen robot bent.'
        ];
    }
}
