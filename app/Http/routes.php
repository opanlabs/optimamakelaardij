<?php

/*Route::get('/', 'PagesController@home');
Route::get('/home', 'PagesController@home');

Route::get('/contact', 'PagesController@contact');
Route::post('/contact', 'PagesController@sendContact');

Route::any('{slug}', 'PagesController@defaultPage');
*/
Route::get('/', 'PagesController@home');
Route::get('/aanbod', 'PagesController@aanbod');
Route::get('/diensten', 'PagesController@diensten');
Route::get('/contact', 'PagesController@contact');
Route::get('/contact/send', 'PagesController@sendContacttest');
Route::post('/contact/send', 'PagesController@sendContact');
Route::get('/optima', 'PagesController@optima');
Route::get('/object/show/{slug}', 'PagesController@objectshow');
// Route::get('/overzicht', 'OverzichtController@index');

Route::any('{slug}', 'PagesController@defaultPage');
