<?php

namespace App\Providers;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Language;

class ConfigServiceProvider extends ServiceProvider
{
    public function boot(Schema $schema)
    {
        if ($schema::hasTable('languages')) {
            Config::set('laravellocalization.supportedLocales', Language::getSupportedLocales());
        } else {
            Log::error('Table languages doesn\'t exist! Make sure you run the migrations first.');
        }
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
